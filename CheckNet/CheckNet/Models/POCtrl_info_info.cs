﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{

    public class OrdernumberPocRequest
    {
        public long ProductionOrderId { get; set; }
        public organization_info OrganizationDetail { get; set; }
    }



    //public class BillingAddress
    //{
    //    public int AddressType { get; set; }
    //    public int PoCtrlNo { get; set; }
    //    public string IntegrateErpCode { get; set; }
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string Contact { get; set; }
    //    public string Street1 { get; set; }
    //    public string Street2 { get; set; }
    //    public string Street3 { get; set; }
    //    public string Street4 { get; set; }
    //    public string ZipCode { get; set; }
    //    public string City { get; set; }
    //    public string County { get; set; }
    //    public string StateProv { get; set; }
    //    public string CountryName { get; set; }
    //    public string Iso3166 { get; set; }
    //    public string Phone { get; set; }
    //    public string Fax { get; set; }
    //    public string Email { get; set; }
    //    public string VAT { get; set; }
    //    public string ERPRefCode { get; set; }
    //    public string StoreNumber { get; set; }
    //    public string TaxIdNo { get; set; }
    //    public string POBox { get; set; }
    //    public int CheckSum { get; set; }
    //    public string Error { get; set; }
    //}

    //public class DeliveryAddress
    //{
    //    public int AddressType { get; set; }
    //    public int PoCtrlNo { get; set; }
    //    public string IntegrateErpCode { get; set; }
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string Contact { get; set; }
    //    public string Street1 { get; set; }
    //    public string Street2 { get; set; }
    //    public string Street3 { get; set; }
    //    public string Street4 { get; set; }
    //    public string ZipCode { get; set; }
    //    public string City { get; set; }
    //    public string County { get; set; }
    //    public string StateProv { get; set; }
    //    public string CountryName { get; set; }
    //    public string Iso3166 { get; set; }
    //    public string Phone { get; set; }
    //    public string Fax { get; set; }
    //    public string Email { get; set; }
    //    public string VAT { get; set; }
    //    public string ERPRefCode { get; set; }
    //    public string StoreNumber { get; set; }
    //    public string TaxIdNo { get; set; }
    //    public string POBox { get; set; }
    //    public int CheckSum { get; set; }
    //    public string Error { get; set; }
    //}

    public class OrderTrackingNumberList
    {
        public int ProductionOrderId { get; set; }
        public string CourierName { get; set; }
        public string TrackingUrl { get; set; }
        public string AwbNumber { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

    public class MobileOrderDetail
    {
        public string OrderNumber { get; set; }
        public string ShoppingCartId { get; set; }
        public string OrganizationName { get; set; }
        public string SalesOrderNumber { get; set; }
        public string VendorRef { get; set; }
        public int Status { get; set; }
        public string StatusDisplay { get; set; }
        public DateTime? DateInSystem { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? EstimatedShipDate { get; set; }
        public DateTime? DueDate { get; set; }
        public int POCtrlNo { get; set; }
        public string OrderedBy { get; set; }
        public string EMailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Price { get; set; }
        public string Comments { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public DeliveryAddress DeliveryAddress { get; set; }
        public List<OrderTrackingNumberList> OrderTrackingNumberList { get; set; }
        public bool IsStagedOrder { get; set; }
        public bool BlockAddressInfo { get; set; }
        public bool HideCheckOutPrice { get; set; }
        public bool DoNotShowFreight { get; set; }
        public string SearchKeyValue { get; set; }
        public string SearchKeyValue2 { get; set; }
        public string SearchKeyValue3 { get; set; }
        public string CutNumber { get; set; }
        public DocumentCurrency DocumentCurrency { get; set; }
        public string RushComment { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

    public class Uom
    {
        public string Name { get; set; }
        public string ShortText { get; set; }
        public int UOMCode { get; set; }
        public string SapCode { get; set; }
        public int TypeId { get; set; }
        public string IntegrateCode { get; set; }
        public string UomDisplay { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

    public class Quantity
    {
        public int Value { get; set; }
        public Uom Uom { get; set; }
    }

    public class GrossCurrency
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

    public class PriceUom
    {
        public string Name { get; set; }
        public string ShortText { get; set; }
        public int UOMCode { get; set; }
        public string SapCode { get; set; }
        public int TypeId { get; set; }
        public string IntegrateCode { get; set; }
        public string UomDisplay { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

    public class MobileOrderlineDetailList
    {
        public string ItemDescription { get; set; }
        public string ItemCode { get; set; }
        public string PrintshopName { get; set; }
        public Quantity Quantity { get; set; }
        public GrossCurrency GrossCurrency { get; set; }
        public decimal GrossPrice { get; set; }
        public PriceUom PriceUom { get; set; }
        public int PriceUnit { get; set; }
        public decimal Surcharge { get; set; }
        public decimal ExpeditedServiceCharge { get; set; }
        public decimal GrossValue { get; set; }
        public decimal DiscountValue { get; set; }
        public decimal Vat { get; set; }
        public int OrderLineId { get; set; }
        public string UnitPrice { get; set; }
        public decimal HandlingFee { get; set; }
        public decimal Amount { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string SignedBy { get; set; }
        public string ThumbNailFile { get; set; }
        public DateTime? DateShipped { get; set; }
        public string ImagePath { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

    //public class OrgContext
    //{
    //    public string Name { get; set; }
    //    public int GroupType { get; set; }
    //    public int RetailerId { get; set; }
    //    public int VendorId { get; set; }
    //    public int BillingCenterId { get; set; }
    //    public int ErpSystemId { get; set; }
    //    public int OwnedByCkpOrgId { get; set; }
    //    public int LobId { get; set; }
    //    public bool HasCatalogFilters { get; set; }
    //    public bool ShowPriceCurrency { get; set; }
    //    public bool HasSpendingLimit { get; set; }
    //    public int SpendingLimitAmount { get; set; }
    //    public string SpendingLimitCurrency { get; set; }
    //    public int SpendingLimitDuration { get; set; }
    //    public int MultiAcctVendorId { get; set; }
    //    public string LogoPath { get; set; }
    //    public string RetailerLogoPath { get; set; }
    //    public bool TopDownHeirarcyOrdering { get; set; }
    //    public bool AllowRequestedShipDate { get; set; }
    //    public bool UpdateAutoReleaseDate { get; set; }
    //    public bool HasThirdPartyShipper { get; set; }
    //    public string ThirdPartyCourierName { get; set; }
    //    public string ThirdPartyCourierAcctNo { get; set; }
    //    public bool EnableByPassprocess { get; set; }
    //    public bool DoNotShowFreight { get; set; }
    //    public bool DoNotAllowShipToChange { get; set; }
    //    public int BillingType { get; set; }
    //    public bool EnableQuantityOverageOnDetail { get; set; }
    //    public bool EnableQuantityOverageOnOrder { get; set; }
    //    public string SpecialOrderInstructions { get; set; }
    //    public string SpecialInvoiceInstructions { get; set; }
    //    public bool HideCheckoutPrice { get; set; }
    //    public bool DoNotAllowVendorRefChange { get; set; }
    //    public int CustomerPONumberFormat { get; set; }
    //    public bool AllowRushShipping { get; set; }
    //    public int MinimumOrderAmount { get; set; }
    //    public string MinimumOrderCurrency { get; set; }
    //    public bool HasVolumeDiscount { get; set; }
    //    public string MandatoryFreightLineRefCode { get; set; }
    //    public bool DiscountByTable { get; set; }
    //    public bool AssortmentEnabled { get; set; }
    //    public string SearchKeyName { get; set; }
    //    public string SearchKeyName2 { get; set; }
    //    public string SearchKeyName3 { get; set; }
    //    public int SearchKeyVisibility { get; set; }
    //    public bool EnableVariableDataSearch { get; set; }
    //    public bool SplitAddressBySku { get; set; }
    //    public bool EnableOrderDataValidation { get; set; }
    //    public bool Outbound { get; set; }
    //    public bool HasTwoLevelOfApproval { get; set; }
    //    public bool ShowSpecialLevelInstructions { get; set; }
    //    public string ErpRefCode { get; set; }
    //    public bool SplitOrderByAddress { get; set; }
    //    public int PaymentTypeCode { get; set; }
    //    public bool BlockAddressInfo { get; set; }
    //    public bool HasCatalog { get; set; }
    //    public string CutNumberLabel { get; set; }
    //    public bool ShowProductionReport { get; set; }
    //    public bool IsNoLeadZero { get; set; }
    //    public bool EnableSplitStagedOrder { get; set; }
    //    public bool ShowConsolidatedInvoice { get; set; }
    //    public int HelpCenterId { get; set; }
    //    public bool EnableCutNumber { get; set; }
    //    public bool ShowPdfSingleInvoice { get; set; }
    //    public bool IsTaxExempt { get; set; }
    //    public bool ShowReOrderApproval { get; set; }
    //    public bool ShowInvoice { get; set; }
    //    public bool AllowCheckoutWhenLimitReached { get; set; }
    //    public string DateFormatCultureName { get; set; }
    //    public bool DoNotAllowDuplicateOrderNumber { get; set; }
    //    public bool WildCardSearch { get; set; }
    //    public bool EnableDynamicSortMethod { get; set; }
    //    public bool AllowClearShoppingCart { get; set; }
    //    public string CustServiceRepEmail { get; set; }
    //    public string VendorRefCode { get; set; }
    //    public bool EnableShipmentDeliveryInfo { get; set; }
    //    public bool SendConfirmationEmailForCustomerService { get; set; }
    //    public bool UpdateStagedOrderDeliveryAddress { get; set; }
    //    public bool ApplyVerticalVariableDataToSelectedLineItem { get; set; }
    //    public bool ShowReleaseStageOrder { get; set; }
    //    public string MultipleProductionLabel { get; set; }
    //    public bool AllowServiceLevel { get; set; }
    //    public bool AllowShippingType { get; set; }
    //    public bool CheckStagedOrderReleaseQuantity { get; set; }
    //    public bool ResetStagedOrderReleaseQuantity { get; set; }
    //    public bool AllowShippingChargeType { get; set; }
    //    public bool HideOrderConfirmationOption { get; set; }
    //    public bool MandatoryCustPONumber { get; set; }
    //    public bool BlockCustomer { get; set; }
    //    public string PoLabel { get; set; }
    //    public bool ReleaseStatusException { get; set; }
    //    public bool AllowResellerSimulate { get; set; }
    //    public bool MandatoryDeliveryDateSelection { get; set; }
    //    public bool OrderTicketPreview { get; set; }
    //    public bool MultiRetailerSearch { get; set; }
    //    public bool ShowAnalyticsReport { get; set; }
    //    public bool UsePODetailVDArrangement { get; set; }
    //    public bool AllowRushOrderApproval { get; set; }
    //    public bool AllowVendorRefForCatalogOrders { get; set; }
    //    public string VendorRefLabel { get; set; }
    //    public int ShowShipmentWeight { get; set; }
    //    public bool RoutingPerVendor { get; set; }
    //    public int RoutingMethod { get; set; }
    //    public bool PreserveCombi { get; set; }
    //    public bool RoutingProcess { get; set; }
    //    public bool EnableProductAlias { get; set; }
    //    public bool AllowOrderNumberBySku { get; set; }
    //    public bool MandatoryVendorRefCode { get; set; }
    //    public bool ItemSettingByPrintshopAndVendor { get; set; }
    //    public bool AllowSampleOrder { get; set; }
    //    public int DefaultServiceLevel { get; set; }
    //    public bool EnableQtyOverageByPcs { get; set; }
    //    public bool IsSupplierPickupDefault { get; set; }
    //    public bool IsSupplierPickupOnly { get; set; }
    //    public int DefaultCatalogSort { get; set; }
    //    public int StagedOrderLimit { get; set; }
    //    public string CustomSkuDisplay { get; set; }
    //    public bool EnableHorizontalPreview { get; set; }
    //    public bool MandatoryEmailApproval { get; set; }
    //    public bool EnableSkuConfirmation { get; set; }
    //    public bool RushShippingOnly { get; set; }
    //    public string NewItemTypeLabel { get; set; }
    //    public bool Split904ByItem { get; set; }
    //    public bool CreateSalesOrderTicketPreview { get; set; }
    //    public int StagedOrderLevelOrdering { get; set; }
    //    public bool EnableAllEmailAlerts { get; set; }
    //    public bool IncludeShipToEmail { get; set; }
    //    public bool ApprovalByItem { get; set; }
    //    public bool SendApproveOrdersToCustomerService { get; set; }
    //    public int DefaultStagedOrderSort { get; set; }
    //    public bool PercentageAndWeight { get; set; }
    //    public bool AssignItemTypeByOrderLine { get; set; }
    //    public int Id { get; set; }
    //    public string Error { get; set; }
    //}

        public class Orderlinelist
    {
        public string ItemDescription { get; set; }
        public string Value { get; set; }
        public string ErrorMessage { get; set; }
        public string ItemCode { get; set; }
        public string PrintshopName { get; set; }
        public string OrderLineId { get; set; }
        

    }

    public class OrdernumberPocResponse
    {
        public MobileOrderDetail MobileOrderDetail { get; set; }
        public List<MobileOrderlineDetailList> MobileOrderlineDetailList { get; set; }
        public string ErrorMessage { get; set; }
        public bool HasNotifyMe { get; set; }
        public OrgContext OrgContext { get; set; }
    }

    public class TrackListRequest
    {
        public string CourierName { get; set; }
    }











        public class DocumentCurrency
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

   

    





}
