﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
    public partial class DeviceInfo
    {
        public bool Active { get; set; }
        public string DeviceName { get; set; }
        public string DeviceId { get; set; }
        public string AppVersion { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DevicePlatform { get; set; }
    }
}
