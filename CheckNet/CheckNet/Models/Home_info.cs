﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
  public class Home_info
    {
        public string Name{get;set;}
        public string Stagename { get; set; }
        public long NewOrderCount { get; set; }
        public long HasReleasedOrder { get; set; }
        public long ReleasedOrderCount { get; set; }
        public long HasApproval { get; set; }
        public long ApprovalCount { get; set; }

    }
}
