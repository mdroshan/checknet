﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
    public class Notification
    {
        public string HolidayDescription { get; set; }
        public string Name { get; set; }
        public DateTime EndDate { get; set; }
    }
    public class Notification_setting
    {
            public int SubscriptionType { get; set; }
            public bool IsEnabled { get; set; }
            public string SubscriptinDescription { get; set; }
            public string ErrorDescription { get; set; }
            public int UserId { get; set; }
            public string IconClass { get; set; }
            public string POCtrlNo { get; set; }
            public string OrderStatus { get; set; }
        

    }
    public class NotificationsettingList
    {
        public string Icon { get; set; }
        public string Name { get; set; }
        public string IsEnabled { get; set; }
        public string Switch { get; set; }
        public string Detail { get; set; }
        public int Status { get; set; }
        public string UserId { get; set; }

    }

    public class GetMessagesResponse
    {
        public int PushNotificationMessageQueueId { get; set; }
        public string Message { get; set; }
        public int SubscriptionType { get; set; }
        public int Status { get; set; }
        public DateTime DateProcessed { get; set; }
       
        public int Id { get; set; }
        public string Error { get; set; }
    }
    public class GetMessagesRequest
    {
        public DateTime DateProcessed { get; set; }
        public string string_DateProcessed { get; set; }
        public string Message { get; set; }
        public string SubscriptionType { get; set; }
        public long PageSize { get; set; }
        public long PageNumber { get; set; }
        public string UserId { get; set; }
    }
    public class UpdateNotificationRequest
    {
        public string Status { get; set; }
        public string UserId { get; set; }
        
    }



    
}
