﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
    public class OrganizationDetail_Info 
    {

        public string UserName { get; set; }
        public string DeviceUUId { get; set; }
        public string DeviceModel { get; set; }
        public string DevicePlatform { get; set; }
        public string DeviceVersion { get; set; }

        public bool IsActive { get; set; }
    }
    
    public class Address
    {
        public bool IsDefault { get; set; }
        public int OrganizationId { get; set; }
        public int AddressType { get; set; }
        public string AddressLabel { get; set; }
        public string DefaultAddressLabel { get; set; }
        public string IntegrateErpCode { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Street3 { get; set; }
        public string Street4 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string StateProv { get; set; }
        public string CountryName { get; set; }
        public string Iso3166 { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string VAT { get; set; }
        public string ERPRefCode { get; set; }
        public string StoreNumber { get; set; }
        public string TaxIdNo { get; set; }
        public string POBox { get; set; }
        public int CheckSum { get; set; }
        public string Error { get; set; }
    }

    public class HelpDesk
    {
        public bool HasHelpCenter { get; set; }
        public string Region { get; set; }
        public string HelpCenterMessage { get; set; }
    }

    public class OrgContext
    {
        public string Name { get; set; }
        public int GroupType { get; set; }
        public int RetailerId { get; set; }
        public int VendorId { get; set; }
        public int BillingCenterId { get; set; }
        public int ErpSystemId { get; set; }
        public int OwnedByCkpOrgId { get; set; }
        public int LobId { get; set; }
        public bool HasCatalogFilters { get; set; }
        public bool ShowPriceCurrency { get; set; }
        public bool HasSpendingLimit { get; set; }
        public string SpendingLimitAmount { get; set; }
        public string SpendingLimitCurrency { get; set; }
        public string SpendingLimitDuration { get; set; }
        public int MultiAcctVendorId { get; set; }
        public string LogoPath { get; set; }
        public string RetailerLogoPath { get; set; }
        public bool TopDownHeirarcyOrdering { get; set; }
        public bool AllowRequestedShipDate { get; set; }
        public bool UpdateAutoReleaseDate { get; set; }
        public bool HasThirdPartyShipper { get; set; }
        public string ThirdPartyCourierName { get; set; }
        public string ThirdPartyCourierAcctNo { get; set; }
        public bool EnableByPassprocess { get; set; }
        public bool DoNotShowFreight { get; set; }
        public bool DoNotAllowShipToChange { get; set; }
        public int BillingType { get; set; }
        public bool EnableQuantityOverageOnDetail { get; set; }
        public bool EnableQuantityOverageOnOrder { get; set; }
        public string SpecialOrderInstructions { get; set; }
        public string SpecialInvoiceInstructions { get; set; }
        public bool HideCheckoutPrice { get; set; }
        public bool DoNotAllowVendorRefChange { get; set; }
        public int CustomerPONumberFormat { get; set; }
        public bool AllowRushShipping { get; set; }
        public string MinimumOrderAmount { get; set; }
        public string MinimumOrderCurrency { get; set; }
        public bool HasVolumeDiscount { get; set; }
        public string MandatoryFreightLineRefCode { get; set; }
        public bool DiscountByTable { get; set; }
        public bool AssortmentEnabled { get; set; }
        public string SearchKeyName { get; set; }
        public string SearchKeyName2 { get; set; }
        public string SearchKeyName3 { get; set; }
        public string SearchKeyVisibility { get; set; }
        public bool EnableVariableDataSearch { get; set; }
        public bool SplitAddressBySku { get; set; }
        public bool EnableOrderDataValidation { get; set; }
        public bool Outbound { get; set; }
        public bool HasTwoLevelOfApproval { get; set; }
        public bool ShowSpecialLevelInstructions { get; set; }
        public string ErpRefCode { get; set; }
        public bool SplitOrderByAddress { get; set; }
        public int PaymentTypeCode { get; set; }
        public bool BlockAddressInfo { get; set; }
        public bool HasCatalog { get; set; }
        public string CutNumberLabel { get; set; }
        public bool ShowProductionReport { get; set; }
        public bool IsNoLeadZero { get; set; }
        public bool EnableSplitStagedOrder { get; set; }
        public bool ShowConsolidatedInvoice { get; set; }
        public int HelpCenterId { get; set; }
        public bool EnableCutNumber { get; set; }
        public bool ShowPdfSingleInvoice { get; set; }
        public bool IsTaxExempt { get; set; }
        public bool ShowReOrderApproval { get; set; }
        public bool ShowInvoice { get; set; }
        public bool AllowCheckoutWhenLimitReached { get; set; }
        public string DateFormatCultureName { get; set; }
        public bool DoNotAllowDuplicateOrderNumber { get; set; }
        public bool WildCardSearch { get; set; }
        public bool EnableDynamicSortMethod { get; set; }
        public bool AllowClearShoppingCart { get; set; }
        public string CustServiceRepEmail { get; set; }
        public string VendorRefCode { get; set; }
        public bool EnableShipmentDeliveryInfo { get; set; }
        public bool SendConfirmationEmailForCustomerService { get; set; }
        public bool UpdateStagedOrderDeliveryAddress { get; set; }
        public bool ApplyVerticalVariableDataToSelectedLineItem { get; set; }
        public bool ShowReleaseStageOrder { get; set; }
        public string MultipleProductionLabel { get; set; }
        public bool AllowServiceLevel { get; set; }
        public bool AllowShippingType { get; set; }
        public bool CheckStagedOrderReleaseQuantity { get; set; }
        public bool ResetStagedOrderReleaseQuantity { get; set; }
        public bool AllowShippingChargeType { get; set; }
        public bool HideOrderConfirmationOption { get; set; }
        public bool MandatoryCustPONumber { get; set; }
        public bool BlockCustomer { get; set; }
        public string PoLabel { get; set; }
        public bool ReleaseStatusException { get; set; }
        public bool AllowResellerSimulate { get; set; }
        public bool MandatoryDeliveryDateSelection { get; set; }
        public bool OrderTicketPreview { get; set; }
        public bool MultiRetailerSearch { get; set; }
        public bool ShowAnalyticsReport { get; set; }
        public bool UsePODetailVDArrangement { get; set; }
        public bool AllowRushOrderApproval { get; set; }
        public bool AllowVendorRefForCatalogOrders { get; set; }
        public string VendorRefLabel { get; set; }
        public string ShowShipmentWeight { get; set; }
        public bool RoutingPerVendor { get; set; }
        public string RoutingMethod { get; set; }
        public bool PreserveCombi { get; set; }
        public bool RoutingProcess { get; set; }
        public bool EnableProductAlias { get; set; }
        public bool AllowOrderNumberBySku { get; set; }
        public bool MandatoryVendorRefCode { get; set; }
        public bool ItemSettingByPrintshopAndVendor { get; set; }
        public bool AllowSampleOrder { get; set; }
        public string DefaultServiceLevel { get; set; }
        public bool EnableQtyOverageByPcs { get; set; }
        public bool IsSupplierPickupDefault { get; set; }
        public bool IsSupplierPickupOnly { get; set; }
        public string DefaultCatalogSort { get; set; }
        public string StagedOrderLimit { get; set; }
        public string CustomSkuDisplay { get; set; }
        public bool EnableHorizontalPreview { get; set; }
        public bool MandatoryEmailApproval { get; set; }
        public bool EnableSkuConfirmation { get; set; }
        public bool RushShippingOnly { get; set; }
        public string NewItemTypeLabel { get; set; }
        public bool Split904ByItem { get; set; }
        public bool CreateSalesOrderTicketPreview { get; set; }
        public string StagedOrderLevelOrdering { get; set; }
        public bool EnableAllEmailAlerts { get; set; }
        public bool IncludeShipToEmail { get; set; }
        public bool ApprovalByItem { get; set; }
        public bool SendApproveOrdersToCustomerService { get; set; }
        public string DefaultStagedOrderSort { get; set; }
        public bool PercentageAndWeight { get; set; }
        public bool AssignItemTypeByOrderLine { get; set; }
        public string Id { get; set; }
        public string Error { get; set; }
    }

    public class organization_info
    {
        public string CreditStatus { get; set; }
        public Address Address { get; set; }
        public string UserOrganizationList { get; set; }
        public string RetailerName { get; set; }
        public HelpDesk HelpDesk { get; set; }
        public OrgContext OrgContext { get; set; }
        public string Logo { get; set; }
        public string DeviceId { get; set; }
        public bool HasUpdatedDeviceInfo { get; set; }
        public string ErrorMessage { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int GroupType { get; set; }
        public string GroupTypeDescription { get; set; }
        public bool HasNewPassword { get; set; }
        public bool HasStagedOrder { get; set; }
        public bool HasOrderApproval { get; set; }
    }
    
    
}
