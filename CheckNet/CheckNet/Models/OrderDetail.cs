﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
    public class OrderDetaildata
    {
        public string Title { get; set; }
        public string Id { get; set; }
        public bool Isvisible { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Street1 { get; set; }
        public string City { get; set; }
        public string ERPRefCode { get; set; }
        public string StateProv { get; set; }
        public string ZipCode { get; set; }
        public string Iso3166 { get; set; }
        
            
            
            
    }
}
