﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
   public class OrderLineRequest
    {
        public long OrderLineId { get; set; }
        public organization_info OrganizationDetail { get; set; }
    }

    public class OrderLineList
    {
        public string Position { get; set; }
        public string Id { get; set; }
        public string LineNumber { get; set; }
        public string Quantity { get; set; }
        public bool Isvisible { get; set; }
        public string __productname { get; set; }
        public string Icon { get; set; }
        public long OriginalQuantity { get; set; }
        public string ArcitemCode { get; set; }
        public string SizeCode { get; set; }
        public string Size1 { get; set; }
        public string Size2 { get; set; }
        public string Size3 { get; set; }
        public string Currency1 { get; set; }
        public string Price1 { get; set; }
        public string Currency2 { get; set; }
        public string Price2 { get; set; }
        public string Currency3 { get; set; }
        public string Price3 { get; set; }
        public string Color { get; set; }
        public string EANCode { get; set; }
        public string CutNumber { get; set; }
        public string TgFlexCat1 { get; set; }
        public string Fam { get; set; }
        public string Category { get; set; }
        public string CategoryName { get; set; }
        public string ShoeName { get; set; }
        public string Description { get; set; }



    }


    public class OrderLine
    {
        public string __productarticlenumber { get; set; }
        public string __productname { get; set; }
        public int __originalquantity { get; set; }
        public int __adjustedquantity { get; set; }
        public int __position { get; set; }
        public int __orderlineid { get; set; }
        public int __productionorderid { get; set; }
        public double __quantityoverage { get; set; }
        public string _arcitemcode { get; set; }
        public string _sizecode { get; set; }
        public string _size1 { get; set; }
        public string _size2 { get; set; }
        public string _size3 { get; set; }
        public string _scurr1 { get; set; }
        public string _sprice1 { get; set; }
        public string _scurr2 { get; set; }
        public string _sprice2 { get; set; }
        public string _scurrtag2 { get; set; }
        public string _spricetag2 { get; set; }
        public string _colour { get; set; }
        public string _eancode { get; set; }
        public string _linenumber { get; set; }
        public string _tgflexcat1 { get; set; }
        public string _fam { get; set; }
        public string _category { get; set; }
        public string _categoryname { get; set; }
        public string _tagflexcat3desc { get; set; }
        public string _tagflexcat2desc { get; set; }
    }

    public class Column
    {
        public string Field { get; set; }
        public string Title { get; set; }
    }

    public class OrderLineResponse
    {
        public List<OrderLine> OrderLine { get; set; }
        public List<Column> Columns { get; set; }
        public string CustomSkuColumn { get; set; }
        public string ErrorMessage { get; set; }
    }
}
