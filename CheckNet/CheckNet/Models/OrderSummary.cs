﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
    public class OrderSummaryResponse
    {
        public string OrderNumber { get; set; }
        public string ShoppingCartId { get; set; }
        public string OrganizationName { get; set; }
        public string SalesOrderNumber { get; set; }
        public string VendorRef { get; set; }
        public string Status { get; set; }
        public string StatusDisplay { get; set; }
        public DateTime? DateInSystem { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? EstimatedShipDate { get; set; }
        public DateTime? DueDate { get; set; }
        public int POCtrlNo { get; set; }
        public string OrderedBy { get; set; }
        public string EMailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Price { get; set; }
        public string Comments { get; set; }
        public string BillingAddress { get; set; }
        public string DeliveryAddress { get; set; }
        public List<object> OrderTrackingNumberList { get; set; }
        public bool IsStagedOrder { get; set; }
        public bool BlockAddressInfo { get; set; }
        public bool HideCheckOutPrice { get; set; }
        public bool DoNotShowFreight { get; set; }
        public string SearchKeyValue { get; set; }
        public string SearchKeyValue2 { get; set; }
        public string SearchKeyValue3 { get; set; }
        public string CutNumber { get; set; }
        public string DocumentCurrency { get; set; }
        public string RushComment { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }

        public string Display1 { get; set; }
        public string Display2 { get; set; }
        public string Display3 { get; set; }
        public string Display4 { get; set; }

        public string Pagenumber { get; set; }
    }
    public class OrderSummaryRequest
    {
        public long PageSize { get; set; }
        public long PageNumber { get; set; }
        public long RetailerId { get; set; }
        public string OrderNumber { get; set; }
        public string ShoppingCartId { get; set; }
        public string SalesOrderNumber { get; set; }
        public string VendorRef { get; set; }
        public string SearchType { get; set; }
        public long OrderType { get; set; }
        public long ProductionOrderId { get; set; }
        public long OrderLineId { get; set; }

        public string[] SearchList { get; set; }
        public string UserId { get; set; }

    }

}
