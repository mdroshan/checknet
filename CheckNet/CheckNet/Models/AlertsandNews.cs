﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
    public class PartnerHolidayList
    {
        public long PartnerId { get; set; }
        public string PrintShopId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string HolidayDescription { get; set; }
        public long OkamiEventId { get; set; }
        public string Name { get; set; }
        public long Id { get; set; }
        public string Error { get; set; }
    }

    public class GetMessageListTaskAsync_Response
    {
        public List<PartnerHolidayList> PartnerHolidayList { get; set; }
        public List<object> AnnouncementList { get; set; }
    }
}
