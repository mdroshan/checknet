﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
    public class principal_info
    {
       
        public string Name { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

    public class Permission
    {
        public int PermissionId { get; set; }
        public int CheckpointSubId { get; set; }
        public int ObjectId { get; set; }
        public int RetailerId { get; set; }
        public int PartnerId { get; set; }
        public int SalesSubId { get; set; }
        public int GroupId { get; set; }
        public int GroupType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public principal_info Application { get; set; }
        public string ApplicationName { get; set; }
        public int ApplicationId { get; set; }
        public int PermissionGroupId { get; set; }
        public int SortOrder { get; set; }
        public bool CanImpersonate { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

    public class Principaldata
    {
        public List<Permission> Permissions { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public int GroupType { get; set; }
        public int OrganizationId { get; set; }
        public object Apps { get; set; }
        public bool HasUnacceptedTerms { get; set; }
    }
}
