﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
   public class OrderLine_info
    {
        public organization_info OrganizationDetail { get; set; }
        public long OrderLineId { get; set; }
    }
    public class ConfirmationHtml_info
    {
        public  OrgContext orgContext { get; set; }
        public  long ShoppingCartId { get; set; }
    }
}
