﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CheckNet.Models
{
    public partial class Auth_Prop
    {

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public long ExpiresIn { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("as:client_id")]
        public string AsClientId { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty(".issued")]
        public string Issued { get; set; }

        [JsonProperty(".expires")]
        public string Expires { get; set; }


        public bool UseRefreshToken { get; set; }

        public bool isAuth { get; set; }
        public string password { get; set; }

        public bool _loggedIn = false;
        public bool LoggedIn
        {
            get
            {
                return this._loggedIn;
            }
            set
            {
                this._loggedIn = value;
            }
        }




        public bool _status = true;
        public bool status
        {
            get; set;
        }

        private string _error;
        public string error
        {
            get
            {
                return this._error;
            }
            set
            {
                this._error = value;
                if (string.IsNullOrEmpty(value))
                {
                    this.status = true;
                }
                else
                {
                    this.status = false;
                }
            }
        }
        public string email { get; set; }
        //public string userName { get; set; }
        public string error_description { get; set; }
        public bool hasForceRefresh { get; set; }


        
        
    }

    public partial class UserProfile_Prop
    {
        public int OrganizationId { get; set; }
        public string UserName { get; set; }
        public long UserId { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public int GroupType { get; set; }
        //public string Apps { get; set; }
        public bool HasUnacceptedTerms { get; set; }
       
    }


   
    public class Write_Email
    {
        public string Name { get; set; }
        public string WEmail { get; set; }
        public string Organization { get; set; }
        public string OrderNumber { get; set; }
        public string Message { get; set; }
    }
   
   
}
