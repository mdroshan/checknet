﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
   public class List_info
    {
        public string OrderNumber { get; set; }
        public string Organization { get; set; }
        public string RecentOrders { get; set; }
        public string Icon { get; set; }
        public string OrderNumberinfo { get; set; }
        public string Organizationinfo { get; set; }
        public string RecentOrdersinfo { get; set; }
        public string Vendor_ref{ get; set; }
        public string Updated_On { get; set; }
        


    }
}
