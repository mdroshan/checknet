﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.MenuItem
{
    //public class MasterPageItem
    //{
    //    public string Title { get; set; }

    //    public string IconSource { get; set; }

    //    public Type TargetType { get; set; }
    //}

    public class MenuList
    {
        public string MenuId
        {
            get;
            set;
        }
        public string MenuName
        {
            get;
            set;
        }
        public string Icon
        {
            get;
            set;
        }
        public string ArrowIcon
        {
            get;
            set;
        }
        public string Text
        {
            get;
            set;
        }
        public string FAQ_Content
        {
            get;
            set;
        }
        public string TermsCondition
        {
            get;
            set;
        }
        public string Id
        {
            get;
            set;
        }
        public string Contactus_Title
        {
            get;
            set;
        }
        public string FAQ_Title
        {
            get;
            set;
        }
        public string Terms_Title
        {
            get;
            set;
        }
        public string Policies_title
        {
            get;
            set;
        }


        public string Policies_Contents { get; set; }
        //public string Policies_title { get; set; }
        public string Menu_ContactUs { get; set; }
        //public string Menu_FAQ { get; set; }
        //public string Menu_TermsConditions { get; set; }
        //public string Menu_Policies { get; set; }
        //public string Menu_Help { get; set; }
        //public string Contactus_writeemail { get; set; }

    }
}
