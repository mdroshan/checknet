﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
  public  class OrderconfirmationRequest
    {

        public string ShoppingCartId { get; set; }
        public OrgContext OrgContext { get; set; }
    }
}
