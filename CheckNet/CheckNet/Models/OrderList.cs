﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
   public class OrderListRequest
    {
        public long RetailerId { get; set; }
        public string OrderNumber { get; set; }
        public string ShoppingCartId { get; set; }
        public string SalesOrderNumber { get; set; }
        public string VendorRef { get; set; }
        public string[] SearchList { get; set; }
        public string UserId { get; set; }
    }

   




        public class BillingAddress
    {
        public int AddressType { get; set; }
        public int PoCtrlNo { get; set; }
        public string IntegrateErpCode { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Street3 { get; set; }
        public string Street4 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string StateProv { get; set; }
        public string CountryName { get; set; }
        public string Iso3166 { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string VAT { get; set; }
        public string ERPRefCode { get; set; }
        public string StoreNumber { get; set; }
        public string TaxIdNo { get; set; }
        public string POBox { get; set; }
        public int CheckSum { get; set; }
        public string Error { get; set; }
    }

    public class DeliveryAddress
    {
        public int AddressType { get; set; }
        public int PoCtrlNo { get; set; }
        public string IntegrateErpCode { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Street3 { get; set; }
        public string Street4 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string StateProv { get; set; }
        public string CountryName { get; set; }
        public string Iso3166 { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string VAT { get; set; }
        public string ERPRefCode { get; set; }
        public string StoreNumber { get; set; }
        public string TaxIdNo { get; set; }
        public string POBox { get; set; }
        public int CheckSum { get; set; }
        public string Error { get; set; }
    }

    public class OrderListResponse
    {
        public string OrderNumber { get; set; }
        public string ShoppingCartId { get; set; }
        public string OrganizationName { get; set; }
        public string SalesOrderNumber { get; set; }
        public string VendorRef { get; set; }
        public int Status { get; set; }
        public string StatusDisplay { get; set; }

        DateTime dateInSystem;
        public DateTime DateInSystem
        {
            get
            {

                if (dateInSystem.Equals(DateTime.MinValue))
                {

                    dateInSystem = Convert.ToDateTime("01/01/1900");

                }
                return dateInSystem;

            }

            set
            {
                dateInSystem = value;
            }
        }

        DateTime orderDate;
        public DateTime OrderDate
        {
            get
            {

                if (orderDate.Equals(DateTime.MinValue))
                {

                    orderDate = Convert.ToDateTime("01/01/1900");

                }
                return orderDate;

            }

            set
            {
                orderDate = value;
            }
        }


        // public DateTime DateInSystem { get; set; }
        // public DateTime OrderDate { get; set; }
        public string EstimatedShipDate { get; set; }
        public string DueDate { get; set; }
        public int POCtrlNo { get; set; }
        public string OrderedBy { get; set; }
        public string EMailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Price { get; set; }
        public string Comments { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public DeliveryAddress DeliveryAddress { get; set; }
        public List<string> OrderTrackingNumberList { get; set; }
        public bool IsStagedOrder { get; set; }
        public bool BlockAddressInfo { get; set; }
        public bool HideCheckOutPrice { get; set; }
        public bool DoNotShowFreight { get; set; }
        public string SearchKeyValue { get; set; }
        public string SearchKeyValue2 { get; set; }
        public string SearchKeyValue3 { get; set; }
        public string CutNumber { get; set; }
        public string DocumentCurrency { get; set; }
        public string RushComment { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }

}
