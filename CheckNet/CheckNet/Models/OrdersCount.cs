﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{


   
        public class MobileOrderCountList
    {
        public int RetailerId { get; set; }
        public string RetailerName { get; set; }
        public bool HasApproval { get; set; }
        public int ApprovalCount { get; set; }
        public bool HasStagedOrder { get; set; }
        public int NewOrderCount { get; set; }
        public bool HasReleasedOrder { get; set; }
        public int ReleasedOrderCount { get; set; }
        public int Id { get; set; }
        public string Error { get; set; }
    }


    public class OrdercountRequest
    {
        public string OrderNumber { get; set; }
        public string ShoppingCartId { get; set; }
        public string SalesOrderNumber { get; set; }
        public string VendorRef { get; set; }
        public organization_info OrganizationDetail { get; set; }
        public string SearchType { get; set; }
        public List<MobileOrderCountList> MobileOrderCountList { get; set; }
        public string Error { get; set; }
    }

    public class OrdersCountResponse
    {
        public string OrderNumber { get; set; }
        public string ShoppingCartId { get; set; }
        public string SalesOrderNumber { get; set; }
        public string VendorRef { get; set; }
        public organization_info OrganizationDetail { get; set; }
        public string SearchType { get; set; }


        public List<MobileOrderCountList> MobileOrderCountList { get; set; }
        public string Error { get; set; }
    }
   

   
}
