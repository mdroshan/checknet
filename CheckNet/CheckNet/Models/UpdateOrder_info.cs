﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckNet.Models
{
    public class updateorder_info
    {
        public string UpdateStatus { get; set; }
        public organization_info OrganizationDetail { get; set; }
        public long ApproveOrdersListData { get; set; }
    }
}
