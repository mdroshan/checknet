﻿using CheckNet.MenuItem;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using System.IO;
using Xamarin.Forms.StyleSheets;
using Xamarin.Forms.Xaml;
using System.Diagnostics;
using CheckNet.View;
using CheckNet.Models;

namespace CheckNet
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
       
        CheckNetWebService data = new CheckNetWebService();
        //private NavigationPage _mainlayout;
        MainPageViewModel MpV = new MainPageViewModel();
        private IEnumerable<MenuList> mylist;

        public MainPage()
        {

            InitializeComponent();
            BindingContext = new MainPageViewModel
            {
                CurrentPage = this
            };
           
            if (IsCurrentlyLogin())
            {
                lst.ItemsSource = MenulanguageforLoginUser();

                lst.SeparatorColor = Color.Black;
                //organization_info organizationdata = (organization_info)Application.Current.Properties["organizationdetails"];
                Detail = new NavigationPage(new View.Home())
                {
                    BarBackgroundColor = Color.FromHex("#ed103c"),
                    BarTextColor = Color.FromHex("#ffffff"),
                };
                IsPresented = false;

                Icon_Logout.IsVisible = true;
                Icon_First.Source = "Home.png";
                AddressDetails.IsVisible = true;

                organization_info org = (organization_info)Application.Current.Properties["organizationdetails"];

                AddressOne.Text = org.Address.Name;
                AddressTwo.Text = org.Address.AddressType + org.Address.Street1 ;
                AddressThree.Text = org.Address.City + "," +org.Address.StateProv + ","+ org.Address.ZipCode+org.Address.Iso3166;
                Phone.Text = org.Address.Phone;
                email.Text = org.Address.Email;
            }
            else
            {
                lst.ItemsSource = Menulanguage();
                lst.SeparatorColor = Color.Black;
                Detail = new NavigationPage(new View.Login())
                {
                    BarBackgroundColor = Color.FromHex("#ed103c"),
                    BarTextColor = Color.FromHex("#ffffff"),
                };
                IsPresented = false;

                Icon_Logout.IsVisible = false;
                Icon_First.Source = "Exit.png";
                AddressDetails.IsVisible = false;
               
            }
           

        }

        public bool IsCurrentlyLogin()
        {
            try
            {
                if (Application.Current.Properties.ContainsKey("authorizationdata"))
                {
                    Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                    if (authprop.LoggedIn)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            catch(Exception ex)
            {
                return false;
            }            
        }

        public IEnumerable<MenuList> Menulanguage()
        {


            List<MenuList> mylist = new List<MenuList>() {

            new MenuItem.MenuList() {

               MenuName=MpV.Menu_ContactUs ,
               MenuId="1",Icon="Contactus.png",ArrowIcon="ArrowList.png",
               Contactus_Title=MpV.Contactus_Title
            },
            new MenuItem.MenuList() {
                MenuName =MpV.Menu_FAQ,MenuId="2",Icon="Faq.png",ArrowIcon="ArrowList.png",
                FAQ_Title=MpV.FAQ_Title,

            },
            new MenuItem.MenuList() {
                MenuName =MpV.Menu_TermsConditions,MenuId="3",Icon="Termscondi.png",ArrowIcon="ArrowList.png",
            },
            new MenuItem.MenuList() {
                MenuName =MpV.Menu_Policies,Icon="Policies.png",MenuId="4",ArrowIcon="ArrowList.png",
            },
            new MenuItem.MenuList() {
                MenuName =MpV.Menu_Help,Icon="Help.png",MenuId="5",ArrowIcon="ArrowList.png",
            },



        };

            return mylist;

        }

        public IEnumerable<MenuList> MenulanguageforLoginUser()
        {


            List<MenuList> mylist = new List<MenuList>() {

            new MenuItem.MenuList() {
                MenuName =MpV.Menu_Notification,Icon="Notification_inbox.png",MenuId="6",ArrowIcon="ArrowList.png",Contactus_Title=MpV.Contactus_Title,
            },
            new MenuItem.MenuList() {

               MenuName=MpV.Menu_ContactUs ,
               MenuId="1",Icon="Contactus.png",ArrowIcon="ArrowList.png",
            },
            new MenuItem.MenuList() {
                MenuName =MpV.Menu_FAQ,MenuId="2",Icon="Faq.png",ArrowIcon="ArrowList.png",

            },
            new MenuItem.MenuList() {
                MenuName =MpV.Menu_TermsConditions,MenuId="3",Icon="Termscondi.png",ArrowIcon="ArrowList.png",
            },
            new MenuItem.MenuList() {
                MenuName =MpV.Menu_Policies,Icon="Policies.png",MenuId="4",ArrowIcon="ArrowList.png",
            },
            new MenuItem.MenuList() {
                MenuName =MpV.Menu_Help,Icon="Help.png",MenuId="5",ArrowIcon="ArrowList.png",
            },

             new MenuItem.MenuList() {
                MenuName =MpV.Menu_Notificationone,Icon="Notification_Setting.png",MenuId="7",ArrowIcon="ArrowList.png",
            },
             new MenuItem.MenuList() {
                MenuName =MpV.Menu_Feedback,Icon="Feedback.png",MenuId="8",ArrowIcon="ArrowList.png",
            },

        };
            
            return mylist;
        }

        public void MainListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var Selected = e.Item as MenuList;

            switch (Selected.MenuId)
            {

                case "1":
                    Detail = new NavigationPage(new View.ContactUs())
                    {
                        BarBackgroundColor = Color.FromHex("#d52b1e"),
                        BarTextColor = Color.White,
                        
                    };

                    IsPresented = false;
                    break;
                case "2":
                    Detail = new NavigationPage(new View.FAQ())
                    {
                        BarBackgroundColor = Color.FromHex("#d52b1e"),
                        BarTextColor = Color.White,
                        

                    };
                    IsPresented = false;

                    break;
                case "3":

                    Detail = new NavigationPage(new View.Terms_And_Condition())
                    {
                        BarBackgroundColor = Color.FromHex("#d52b1e"),
                        BarTextColor = Color.White,
                    };
                    IsPresented = false;
                    break;
                case "4":


                    Detail = new NavigationPage(new View.Policies())
                    {
                        BarBackgroundColor = Color.FromHex("#b21313"),
                        BarTextColor = Color.White,

                    };
                    IsPresented = false;
                    break;
                case "5":

                    Detail = new NavigationPage(new View.Help())
                    {
                        BarBackgroundColor = Color.FromHex("#d52b1e"),
                        BarTextColor = Color.White,
                    };
                    IsPresented = false;
                    break;
                case "6":

                    Detail = new NavigationPage(new View.Notification_Inbox())
                    {
                        BarBackgroundColor = Color.FromHex("#d52b1e"),
                        BarTextColor = Color.White,
                    };
                    IsPresented = false;
                    break;
                case "7":

                    Detail = new NavigationPage(new View.Notification_Setting())
                    {
                        BarBackgroundColor = Color.FromHex("#d52b1e"),
                        BarTextColor = Color.White,
                    };
                    IsPresented = false;
                    break;
                case "8":

                    Detail = new NavigationPage(new View.Feedback())
                    {
                        BarBackgroundColor = Color.FromHex("#d52b1e"),
                        BarTextColor = Color.White,
                    };
                    IsPresented = false;
                    break;
            }

           ((ListView)sender).SelectedItem = "";
        }

        void OnTapGestureRecognizerTapped(object sender, EventArgs args)
        {
            if (IsCurrentlyLogin())
            {
                lst.ItemsSource = MenulanguageforLoginUser();
                Detail = new NavigationPage(new View.Home())
                {
                    BarBackgroundColor = Color.FromHex("#d52b1e"),
                    BarTextColor = Color.FromHex("#ffffff"),
                };
                IsPresented = false;

                Icon_Logout.IsVisible = true;
                Icon_First.Source = "Home.png";
            }
            else
            {
                Detail = new NavigationPage(new View.Login())
                {
                    BarBackgroundColor = Color.FromHex("#d52b1e"),
                    BarTextColor = Color.FromHex("#ffffff"),
                };
                IsPresented = false;

                Icon_Logout.IsVisible = false;
                Icon_First.Source = "Exit.png";
            }

        }

        void OnLogoutButtonTapped(object sender, EventArgs args)
        {
            if (IsCurrentlyLogin())
            { //data.Lo
                if(data.Logout())
                {
                    lst.ItemsSource = Menulanguage();
                    lst.SeparatorColor = Color.Black;
                    Detail = new NavigationPage(new View.Login())
                    {
                        BarBackgroundColor = Color.FromHex("#ed103c"),
                        BarTextColor = Color.FromHex("#ffffff"),
                    };
                    IsPresented = false;

                    Icon_Logout.IsVisible = false;
                    Icon_First.Source = "Exit.png";
                    AddressDetails.IsVisible = false;
                }
            }
            
        }

    }

}
