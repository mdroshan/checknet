﻿using I18NPortable;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using CheckNet.MenuItem;
using CheckNet.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace CheckNet.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
        //Dclass MenuName = new Dclass();
        public II18N Strings => I18N.Current;
        //Login List
        public string Login_RememberMe => "Login_RememberMe".Translate(Device.RuntimePlatform);
        public string Login_Username => "Login_Username".Translate(Device.RuntimePlatform);
        public string Login_Password => "Login_Password".Translate(Device.RuntimePlatform);
        public string Login_PasswordHint => "Login_PasswordHint".Translate(Device.RuntimePlatform);
        public string Login_ResetPassword => "Login_ResetPassword".Translate(Device.RuntimePlatform);
        
        public string Login_Login => "Login_Login".Translate(Device.RuntimePlatform);
        public string Login_Contentone => "Login_Contentone".Translate(Device.RuntimePlatform);
        public string Login_Contenttwo => "Login_Contenttwo".Translate(Device.RuntimePlatform);
        public string Login_help => "Login_help".Translate(Device.RuntimePlatform);
        public string Login_Icon => "Login_Icon".Translate(Device.RuntimePlatform);

        public string Reset_Password => "Reset_Password".Translate(Device.RuntimePlatform);
        public string Reset_Password_content => "Reset_Password_content".Translate(Device.RuntimePlatform);
        public string Reset_Password_email => "Reset_Password_email".Translate(Device.RuntimePlatform);
        public string Reset_Password_button => "Reset_Password_button".Translate(Device.RuntimePlatform);
        //Validation Messages
        public string Validation_Alert => "Validation_Alert".Translate(Device.RuntimePlatform);
        public string Validation_User => "Validation_User".Translate(Device.RuntimePlatform);
        public string Validation_Pass => "Validation_Pass".Translate(Device.RuntimePlatform);
        public string Validation_Ok => "Validation_Ok".Translate(Device.RuntimePlatform);
        public string Validation_Cancel => "Validation_Cancel".Translate(Device.RuntimePlatform);
        public string Validation_PasswordHint => "Validation_PasswordHint".Translate(Device.RuntimePlatform);



        //Menu List
        public string Menu_ContactUs => "Menu_ContactUs".Translate(Device.RuntimePlatform);
        public string Menu_FAQ => "Menu_FAQ".Translate(Device.RuntimePlatform);
        public string Menu_TermsConditions => "Menu_TermsConditions".Translate(Device.RuntimePlatform);
        public string Menu_Policies => "Menu_Policies".Translate(Device.RuntimePlatform);
        public string Menu_Help => "Menu_Help".Translate(Device.RuntimePlatform);
        public string Menu_Notification => "Menu_Notification".Translate(Device.RuntimePlatform);
        public string Menu_Feedback => "Menu_Feedback".Translate(Device.RuntimePlatform);
        public string Menu_Notificationone => "Menu_Notificationone".Translate(Device.RuntimePlatform);
        


        //ContactUs List
        public string Contactus_Title => "Contactus_Title".Translate(Device.RuntimePlatform);
        public string Contactus_USA => "Contactus_USA".Translate(Device.RuntimePlatform);
        public string Contactus_Europe => "Contactus_Europe".Translate(Device.RuntimePlatform);
        public string Contactus_Asia => "Contactus_Asia".Translate(Device.RuntimePlatform);
        public string Contactus_Austraila => "Contactus_Austraila".Translate(Device.RuntimePlatform);
        public string Contactus_writeemail => "Contactus_writeemail".Translate(Device.RuntimePlatform);

        public string Contactus_Usa_qustone => "Contactus_Usa_qustone".Translate(Device.RuntimePlatform);
        public string Contactus_Usa_qusttwo => "Contactus_Usa_qusttwo".Translate(Device.RuntimePlatform);
        public string Contactus_Usa_qustthree => "Contactus_Usa_qustthree".Translate(Device.RuntimePlatform);

        public string Contactus_Usa_Answone => "Contactus_Usa_Answone".Translate(Device.RuntimePlatform);
        public string Contactus_Usa_Anstwo => "Contactus_Usa_Anstwo".Translate(Device.RuntimePlatform);
        public string Contactus_Usa_Ansthree => "Contactus_Usa_Ansthree".Translate(Device.RuntimePlatform);

        public string Contactus_Europe_qustone => "Contactus_Europe_qustone".Translate(Device.RuntimePlatform);
        public string Contactus_Europe_qusttwo => "Contactus_Europe_qusttwo".Translate(Device.RuntimePlatform);
        public string Contactus_Europe_qustthree => "Contactus_Europe_qustthree".Translate(Device.RuntimePlatform);
        public string Contactus_Europe_qustfour => "Contactus_Europe_qustfour".Translate(Device.RuntimePlatform);
        public string Contactus_Europe_Anstone => "Contactus_Europe_Anstone".Translate(Device.RuntimePlatform);
        public string Contactus_Europe_Answtwo => "Contactus_Europe_Answtwo".Translate(Device.RuntimePlatform);
        public string Contactus_Europe_Answthree => "Contactus_Europe_Answthree".Translate(Device.RuntimePlatform);
        public string Contactus_Europe_Answfour => "Contactus_Europe_Answfour".Translate(Device.RuntimePlatform);

        public string Write_Email_name => "Write_Email_name".Translate(Device.RuntimePlatform);
        public string Write_Email_email => "Write_Email_email".Translate(Device.RuntimePlatform);
        public string Write_Email_orng => "Write_Email_orng".Translate(Device.RuntimePlatform);
        public string Write_Email_odrno => "Write_Email_odrno".Translate(Device.RuntimePlatform);
        public string Write_Email_message => "Write_Email_message".Translate(Device.RuntimePlatform);
        public string Write_Email_send => "Write_Email_send".Translate(Device.RuntimePlatform);
        public string Write_Email => "Write_Email".Translate(Device.RuntimePlatform);



        public string Contactus_Asia_qustone => "Contactus_Asia_qustone".Translate(Device.RuntimePlatform);
        public string Contactus_Asia_qusttwo => "Contactus_Asia_qusttwo".Translate(Device.RuntimePlatform);
        public string Contactus_Asia_Answone => "Contactus_Asia_Answone".Translate(Device.RuntimePlatform);
        public string Contactus_Asia_Answtwo => "Contactus_Asia_Answtwo".Translate(Device.RuntimePlatform);

        public string Contactus_Austraila_qustone => "Contactus_Austraila_qustone".Translate(Device.RuntimePlatform);
        public string Contactus_Austraila_Answone => "Contactus_Austraila_Answone".Translate(Device.RuntimePlatform);



        //Forgot List

        public string FAQ_Title => "FAQ_Title".Translate(Device.RuntimePlatform);
        public string Forgot_Password => "Forgot_Password".Translate(Device.RuntimePlatform);
        public string Forgot_info => "Forgot_info".Translate(Device.RuntimePlatform);
        public string Forgot_Email => "Forgot_Email".Translate(Device.RuntimePlatform);
        public string Forgot_pass => "Forgot_pass".Translate(Device.RuntimePlatform);
        public string FAQ_Content => "FAQ_Content".Translate(Device.RuntimePlatform);

        //FAQ List
        public string FAQ_HeaderContent => "FAQ_HeaderContent".Translate(Device.RuntimePlatform);
        public string FAQ_QustOne => "FAQ_QustOne".Translate(Device.RuntimePlatform);
        public string FAQ_QuestTwo => "FAQ_QuestTwo".Translate(Device.RuntimePlatform);
        public string FAQ_QustThree => "FAQ_QustThree".Translate(Device.RuntimePlatform);
        public string FAQ_QustFour => "FAQ_QustFour".Translate(Device.RuntimePlatform);
        public string FAQ_QustFive => "FAQ_QustFive".Translate(Device.RuntimePlatform);
        public string FAQ_QustSix => "FAQ_QustSix".Translate(Device.RuntimePlatform);
        public string FAQ_QustSeven => "FAQ_QustSeven".Translate(Device.RuntimePlatform);
        public string FAQ_QustEight => "FAQ_QustEight".Translate(Device.RuntimePlatform);
        public string FAQ_QustNine => "FAQ_QustNine".Translate(Device.RuntimePlatform);

        public string FAQ_AnswrOne => "FAQ_AnswrOne".Translate(Device.RuntimePlatform);
        public string FAQ_AnswrTwo => "FAQ_AnswrTwo".Translate(Device.RuntimePlatform);
        public string FAQ_AnswrThree => "FAQ_AnswrThree".Translate(Device.RuntimePlatform);
        public string FAQ_AnswrFour => "FAQ_AnswrFour".Translate(Device.RuntimePlatform);
        public string FAQ_AnswrFive => "FAQ_AnswrFive".Translate(Device.RuntimePlatform);
        public string FAQ_AnswrSix => "FAQ_AnswrSix".Translate(Device.RuntimePlatform);
        public string FAQ_AnswrSeven => "FAQ_AnswrSeven".Translate(Device.RuntimePlatform);
        public string FAQ_AnswrEight => "FAQ_AnswrEight".Translate(Device.RuntimePlatform);
        public string FAQ_AnswrNine => "FAQ_AnswrNine".Translate(Device.RuntimePlatform);

        //Terms and Condition

        public string Terms_condition => "Terms_condition".Translate(Device.RuntimePlatform);
        public string Terms_condition_Title => "Terms_condition_Title".Translate(Device.RuntimePlatform);

        //Policies List
        public string Policies_title => "Policies_title".Translate(Device.RuntimePlatform);
        public string Policies_Contents => "Policies_Contents".Translate(Device.RuntimePlatform);
        public string Policies_Privacy => "Policies_Privacy".Translate(Device.RuntimePlatform);
        public string Policies_Privacy_Cont => "Policies_Privacy_Cont".Translate(Device.RuntimePlatform);
        public string Policies_Return => "Policies_Return".Translate(Device.RuntimePlatform);
        public string Policies_Return_cont => "Policies_Return_cont".Translate(Device.RuntimePlatform);


        public string Alert_News => "Alert_News".Translate(Device.RuntimePlatform);
        public string Alert_Shop => "Alert_Shop".Translate(Device.RuntimePlatform);
        //Help List
        public string Help_Title => "Help_Title".Translate(Device.RuntimePlatform);

        public string Home => "Home".Translate(Device.RuntimePlatform);


        //Alerts and News:
        public string AlertsandNew => "AlertsandNew".Translate(Device.RuntimePlatform);
        public string Alerts_PrintshopHoliday => "Alerts_PrintshopHoliday".Translate(Device.RuntimePlatform);
        //Notification Inbox
        public string Notification_title => "Help_Title".Translate(Device.RuntimePlatform);
        public string NotificationList_headingone => "NotificationList_headingone".Translate(Device.RuntimePlatform);
        public string NotificationList_headingtwo => "NotificationList_headingtwo".Translate(Device.RuntimePlatform);
        //Notification Setting
        public string Notification_SettingTitle => "Notification_SettingTitle".Translate(Device.RuntimePlatform);
        public string Clear_badge_Count => "Clear_badge_Count".Translate(Device.RuntimePlatform);
        public string New_Orders_received => "New_Orders_received".Translate(Device.RuntimePlatform);
        public string Shipment_Confirmation => "Shipment_Confirmation".Translate(Device.RuntimePlatform);
        public string Production_Facility_Holiday => "Production_Facility_Holiday".Translate(Device.RuntimePlatform);
        public string Net_Message => "Net_Message".Translate(Device.RuntimePlatform);

        //Pickerone
        public string FeedBack => "FeedBack".Translate(Device.RuntimePlatform);
        public string Feedback_Entry => "Feedback_Entry".Translate(Device.RuntimePlatform);
        public string Feedback_button => "Feedback_button".Translate(Device.RuntimePlatform);

        //Picker
        public string Pickerone => "Pickerone".Translate(Device.RuntimePlatform);
        public string Pickertwo => "Pickertwo".Translate(Device.RuntimePlatform);
        public string Pickerthree => "Pickerthree".Translate(Device.RuntimePlatform);
        public string PickerFour => "PickerFour".Translate(Device.RuntimePlatform);

        //Order COunt
        public string Approvals => "Approvals".Translate(Device.RuntimePlatform);
        public string Staged_orders => "Staged_orders".Translate(Device.RuntimePlatform);
        public string Recent_Orders => "Recent_Orders".Translate(Device.RuntimePlatform);
        public string Search => "Search".Translate(Device.RuntimePlatform);

        //Order summary
        public string OrderSummary_Next => "OrderSummary_Next".Translate(Device.RuntimePlatform);
        public string OrderSummary_VendorRef => "OrderSummary_VendorRef".Translate(Device.RuntimePlatform);
        public string OrderSummary_OrderNumber => "OrderSummary_OrderNumber".Translate(Device.RuntimePlatform);
        public string OrderSummary_ReceiveDate => "OrderSummary_ReceiveDate".Translate(Device.RuntimePlatform);
        public string OrderSummary_Update => "OrderSummary_Update".Translate(Device.RuntimePlatform);
        public string OrderSummary_Organizaiton => "OrderSummary_Organizaiton".Translate(Device.RuntimePlatform);
        public string Ordersummary_Status => "Ordersummary_Status".Translate(Device.RuntimePlatform);
        public string OrderSummary_SalesOrder => "OrderSummary_SalesOrder".Translate(Device.RuntimePlatform);
        public string Ordersummary_ShoppingCart => "Ordersummary_ShoppingCart".Translate(Device.RuntimePlatform);
        public string Ordersummary => "Ordersummary".Translate(Device.RuntimePlatform);
        public string OrderSummary_Orderdate => "OrderSummary_Orderdate".Translate(Device.RuntimePlatform);

        //Order details
        public string Orderdetail => "Orderdetail".Translate(Device.RuntimePlatform);
        public string OrderDetail_ReceiveDate => "OrderDetail_ReceiveDate".Translate(Device.RuntimePlatform);
        public string OrderDetail_ShoppingCart => "OrderDetail_ShoppingCart".Translate(Device.RuntimePlatform);
        public string OrderDetail_OrderNumer => "OrderDetail_OrderNumer".Translate(Device.RuntimePlatform);
        public string OrderDetail_SalesOrder => "OrderDetail_SalesOrder".Translate(Device.RuntimePlatform);
        public string OrderDetail_VendorRef => "OrderDetail_VendorRef".Translate(Device.RuntimePlatform);
        public string OrderDetail_Esatimated => "OrderDetail_Esatimated".Translate(Device.RuntimePlatform);
        public string OrderDetail_ShipDate => "OrderDetail_ShipDate".Translate(Device.RuntimePlatform);
        public string OrderDetail_Cutmunber => "OrderDetail_Cutmunber".Translate(Device.RuntimePlatform);
        public string OrderDetail_NotifyMe => "OrderDetail_NotifyMe".Translate(Device.RuntimePlatform);
        public string Orderdetail_Orderconfirmation => "Orderdetail_Orderconfirmation".Translate(Device.RuntimePlatform);
        public string OrderDetail_OrderStatus => "OrderDetail_OrderStatus".Translate(Device.RuntimePlatform);
        public string OrderDetail_bill => "OrderDetail_bill".Translate(Device.RuntimePlatform);
        public string OrderDetail_ship => "OrderDetail_ship".Translate(Device.RuntimePlatform);
        public string OrderDetail_inquiry => "OrderDetail_inquiry".Translate(Device.RuntimePlatform);
        public string OrderDetail_send => "OrderDetail_send".Translate(Device.RuntimePlatform);
        public string OrderDetail_detail => "OrderDetail_detail".Translate(Device.RuntimePlatform);
        public string OrderDetail_item => "OrderDetail_item".Translate(Device.RuntimePlatform);
        public string OrderDetail_Quantity => "OrderDetail_Quantity".Translate(Device.RuntimePlatform);
        public string Orderdetail_released => "Orderdetail_released".Translate(Device.RuntimePlatform);
        public string Orderdetail_Email => "Orderdetail_Email".Translate(Device.RuntimePlatform);
        public string Orderdetail_Phone => "Orderdetail_Phone".Translate(Device.RuntimePlatform);
        public string Orderdetail_Status => "Orderdetail_Status".Translate(Device.RuntimePlatform);
        //OrderLinedetail
        public string OrderLinedetail => "OrderLinedetail".Translate(Device.RuntimePlatform);
        public string OrderLinedetail_line => "OrderLinedetail_line".Translate(Device.RuntimePlatform);
        public string OrderLinedetail_Quantity => "OrderLinedetail_Quantity".Translate(Device.RuntimePlatform);
        public string OrderLine => "OrderLine".Translate(Device.RuntimePlatform);
        public string OrderLine_itemcode => "OrderLine_itemcode".Translate(Device.RuntimePlatform);
        public string OrderLine_TotalQuantity => "OrderLine_TotalQuantity".Translate(Device.RuntimePlatform);

        //OrderList
        public string OrderList_Ordernumber => "OrderList_Ordernumber".Translate(Device.RuntimePlatform);
        public string OrderList_SalesOrder => "OrderList_SalesOrder".Translate(Device.RuntimePlatform);
        public string OrderList_Shoppingcart => "OrderList_Shoppingcart".Translate(Device.RuntimePlatform);
        public string OrderList_VendorRef => "OrderList_VendorRef".Translate(Device.RuntimePlatform);
        public string OrderList_OrderbOrder => "OrderList_OrderbOrder".Translate(Device.RuntimePlatform);
        public string OrderList_Orderdate => "OrderList_Orderdate".Translate(Device.RuntimePlatform);
        public string OrderList_Orderconfirmation => "OrderList_Orderconfirmation".Translate(Device.RuntimePlatform);
        public string Orderlist => "Orderlist".Translate(Device.RuntimePlatform);

        //Tracking List
        public string trackingList => "trackingList".Translate(Device.RuntimePlatform);



        public IEnumerable<PortableLanguage> LanguagesToSelect
            => I18N.Current.Languages
                .Where(x => x.Locale != I18N.Current.Locale);

        public IEnumerable<MenuList> MenuToSelect
          => LoadLanguages();

        public IEnumerable<MenuList> LoadLanguages()
        {

            //Dclass MenuName = new Dclass();
            List<MenuList> mylist = new List<MenuList>() {

            new MenuItem.MenuList() {

                Text =Menu_ContactUs ,MenuId="1",Icon="Contactus.png",ArrowIcon="Arrow1.png",
            },
            new MenuItem.MenuList() {
                Text =Menu_FAQ,MenuId="2",Icon="Faq.png",ArrowIcon="Arrow1.png",

            },
            new MenuItem.MenuList() {
                Text =Menu_TermsConditions,MenuId="3",Icon="Termscondi.png",ArrowIcon="Arrow1.png",
            },
            new MenuItem.MenuList() {
                Text =Menu_Policies,Icon="Policies.png",MenuId="4",ArrowIcon="Arrow1.png",
            },
            new MenuItem.MenuList() {
                Text =Menu_Help,Icon="Help.png",MenuId="5",ArrowIcon="Arrow1.png",
            },


        };

            return mylist;

        }

        public ObservableCollection<OrderListResponse> OrderList
        {
            get;
            set;
        }
         //=> LoadOrderList();

        //public async Task<IEnumerable<OrderListResponse>> LoadOrderList()
        //{
        //    OrganizationWebService api = new OrganizationWebService();
        //    OrderList order = new OrderList();
        //    order.PageSize = 10;
        //    order.PageNumber = 1;
        //    order.RetailerId = 9638;
        //    order.OrderNumber = "";
        //    order.SalesOrderNumber = "";
        //    order.ShoppingCartId = "";
        //    order.VendorRef = "VendorRef";
        //    string[] b = new string[0];
        //    order.SearchList = b;
        //    order.UserId = "181321";
            
        //    return await api.GetOrderList(order);
        //}

       

        public void LoadLocale(string locale)
        {
            I18N.Current.Locale = locale;
            //Login List
            OnPropertyChanged(nameof(Login_RememberMe));
            OnPropertyChanged(nameof(Login_Username));
            OnPropertyChanged(nameof(Login_Password));
            OnPropertyChanged(nameof(Login_PasswordHint));
            OnPropertyChanged(nameof(Login_Login));
            OnPropertyChanged(nameof(Login_Contentone));
            OnPropertyChanged(nameof(Login_Contenttwo));
            OnPropertyChanged(nameof(Login_help));
            OnPropertyChanged(nameof(Login_Icon));
            OnPropertyChanged(nameof(Login_ResetPassword));
            


            //ResetPassword List
            OnPropertyChanged(nameof(Reset_Password));
            OnPropertyChanged(nameof(Reset_Password_content));
            OnPropertyChanged(nameof(Reset_Password_email));
            OnPropertyChanged(nameof(Reset_Password_button));


            //Validation List
            OnPropertyChanged(nameof(Validation_Alert));
            OnPropertyChanged(nameof(Validation_User));
            OnPropertyChanged(nameof(Validation_Pass));
            OnPropertyChanged(nameof(Validation_Ok));
            OnPropertyChanged(nameof(Validation_Cancel));
            OnPropertyChanged(nameof(Validation_PasswordHint));

            //Menus List
            OnPropertyChanged(nameof(Menu_ContactUs));
            OnPropertyChanged(nameof(Menu_FAQ));
            OnPropertyChanged(nameof(Menu_TermsConditions));
            OnPropertyChanged(nameof(Menu_Policies));
            OnPropertyChanged(nameof(Menu_Help));
            OnPropertyChanged(nameof(Menu_Notification));
            OnPropertyChanged(nameof(Menu_Feedback));
            OnPropertyChanged(nameof(Menu_Notificationone));


            //Forgot List
            OnPropertyChanged(nameof(Forgot_Password));
            OnPropertyChanged(nameof(Forgot_info));
            OnPropertyChanged(nameof(Menu_ContactUs));
            OnPropertyChanged(nameof(Forgot_pass));
            OnPropertyChanged(nameof(FAQ_Content));

            //ContactUs List
            OnPropertyChanged(nameof(Contactus_Title));
            OnPropertyChanged(nameof(Contactus_USA));
            OnPropertyChanged(nameof(Contactus_Europe));
            OnPropertyChanged(nameof(Contactus_Asia));
            OnPropertyChanged(nameof(Contactus_Austraila));
            OnPropertyChanged(nameof(Contactus_writeemail));

            //Write Email

            OnPropertyChanged(nameof(Write_Email));
            OnPropertyChanged(nameof(Write_Email_name));
            OnPropertyChanged(nameof(Write_Email_email));
            OnPropertyChanged(nameof(Write_Email_orng));
            OnPropertyChanged(nameof(Write_Email_odrno));
            OnPropertyChanged(nameof(Write_Email_message));
            OnPropertyChanged(nameof(Write_Email_send));

            //USA
            OnPropertyChanged(nameof(Contactus_Usa_qustone));
            OnPropertyChanged(nameof(Contactus_Usa_qusttwo));
            OnPropertyChanged(nameof(Contactus_Usa_qustthree));

            OnPropertyChanged(nameof(Contactus_Usa_Answone));
            OnPropertyChanged(nameof(Contactus_Usa_Anstwo));
            OnPropertyChanged(nameof(Contactus_Usa_Ansthree));

            //Europe
            OnPropertyChanged(nameof(Contactus_Europe_qustone));
            OnPropertyChanged(nameof(Contactus_Europe_qusttwo));
            OnPropertyChanged(nameof(Contactus_Europe_qustthree));
            OnPropertyChanged(nameof(Contactus_Europe_qustfour));
            OnPropertyChanged(nameof(Contactus_Europe_Anstone));
            OnPropertyChanged(nameof(Contactus_Europe_Answtwo));
            OnPropertyChanged(nameof(Contactus_Europe_Answthree));
            OnPropertyChanged(nameof(Contactus_Europe_Answfour));
            //Asia
            OnPropertyChanged(nameof(Contactus_Asia_qustone));
            OnPropertyChanged(nameof(Contactus_Asia_qusttwo));
            OnPropertyChanged(nameof(Contactus_Asia_Answone));
            OnPropertyChanged(nameof(Contactus_Asia_Answtwo));
            //Austraila
            OnPropertyChanged(nameof(Contactus_Austraila_qustone));
            OnPropertyChanged(nameof(Contactus_Austraila_Answone));



            //FAQ List
            OnPropertyChanged(nameof(FAQ_Title));
            OnPropertyChanged(nameof(FAQ_HeaderContent));
            OnPropertyChanged(nameof(FAQ_QustOne));
            OnPropertyChanged(nameof(FAQ_QuestTwo));
            OnPropertyChanged(nameof(FAQ_QustThree));
            OnPropertyChanged(nameof(FAQ_QustFour));
            OnPropertyChanged(nameof(FAQ_QustFive));
            OnPropertyChanged(nameof(FAQ_QustSix));
            OnPropertyChanged(nameof(FAQ_QustSeven));
            OnPropertyChanged(nameof(FAQ_QustEight));
            OnPropertyChanged(nameof(FAQ_QustNine));

            OnPropertyChanged(nameof(FAQ_AnswrOne));
            OnPropertyChanged(nameof(FAQ_AnswrTwo));
            OnPropertyChanged(nameof(FAQ_AnswrThree));
            OnPropertyChanged(nameof(FAQ_AnswrFour));
            OnPropertyChanged(nameof(FAQ_AnswrFive));
            OnPropertyChanged(nameof(FAQ_AnswrSix));
            OnPropertyChanged(nameof(FAQ_AnswrSeven));
            OnPropertyChanged(nameof(FAQ_AnswrEight));
            OnPropertyChanged(nameof(FAQ_AnswrNine));

            //Terms And condition

            OnPropertyChanged(nameof(Terms_condition_Title));
            OnPropertyChanged(nameof(Terms_condition));

            //Policies List
            OnPropertyChanged(nameof(Policies_title));
            OnPropertyChanged(nameof(Policies_Privacy));
            OnPropertyChanged(nameof(Policies_Privacy_Cont));
            OnPropertyChanged(nameof(Policies_Privacy_Cont));
            OnPropertyChanged(nameof(Policies_Return));
            OnPropertyChanged(nameof(Policies_Contents));

            OnPropertyChanged(nameof(Alert_Shop));
            OnPropertyChanged(nameof(Alert_News));


            //Help List
            OnPropertyChanged(nameof(Help_Title));


            //News and alerts
            OnPropertyChanged(nameof(AlertsandNew));
            OnPropertyChanged(nameof(Alerts_PrintshopHoliday));
            


            //Notification setting
            OnPropertyChanged(nameof(Notification_SettingTitle));
            OnPropertyChanged(nameof(Clear_badge_Count));
            OnPropertyChanged(nameof(New_Orders_received));
            OnPropertyChanged(nameof(Shipment_Confirmation));
            OnPropertyChanged(nameof(Production_Facility_Holiday));
            OnPropertyChanged(nameof(Net_Message));


            //notification inbox
            OnPropertyChanged(nameof(Notification_title));
            OnPropertyChanged(nameof(NotificationList_headingone));
            OnPropertyChanged(nameof(NotificationList_headingtwo));

            //feedback
            OnPropertyChanged(nameof(FeedBack));
            OnPropertyChanged(nameof(Feedback_Entry));
            OnPropertyChanged(nameof(Feedback_button));

            //Picker
            OnPropertyChanged(nameof(Pickerone));
            OnPropertyChanged(nameof(Pickertwo));
            OnPropertyChanged(nameof(Pickerthree));
            OnPropertyChanged(nameof(PickerFour));

            //Ordercounts
            OnPropertyChanged(nameof(Approvals));
            OnPropertyChanged(nameof(Staged_orders));
            OnPropertyChanged(nameof(Recent_Orders));
            OnPropertyChanged(nameof(Search));

            //Order summary
            OnPropertyChanged(nameof(OrderSummary_Next));
            OnPropertyChanged(nameof(Ordersummary));
            OnPropertyChanged(nameof(OrderSummary_Orderdate));
            OnPropertyChanged(nameof(OrderSummary_VendorRef));
            OnPropertyChanged(nameof(OrderSummary_OrderNumber));
            OnPropertyChanged(nameof(OrderSummary_ReceiveDate));
            OnPropertyChanged(nameof(OrderSummary_Update));
            OnPropertyChanged(nameof(OrderSummary_Organizaiton));
            OnPropertyChanged(nameof(Ordersummary_Status));
            OnPropertyChanged(nameof(OrderSummary_SalesOrder));
            OnPropertyChanged(nameof(Ordersummary_ShoppingCart));
            
            

            //order detail
            OnPropertyChanged(nameof(Orderdetail));
            OnPropertyChanged(nameof(OrderDetail_ReceiveDate));
            OnPropertyChanged(nameof(OrderDetail_ShoppingCart));
            OnPropertyChanged(nameof(OrderDetail_OrderNumer));
            OnPropertyChanged(nameof(OrderDetail_SalesOrder));
            OnPropertyChanged(nameof(OrderDetail_VendorRef));
            OnPropertyChanged(nameof(OrderDetail_Esatimated));
            OnPropertyChanged(nameof(OrderDetail_ShipDate));
            OnPropertyChanged(nameof(OrderDetail_Cutmunber));
            OnPropertyChanged(nameof(OrderDetail_Cutmunber));
            OnPropertyChanged(nameof(OrderDetail_NotifyMe));
            OnPropertyChanged(nameof(Orderdetail_Orderconfirmation));
            OnPropertyChanged(nameof(OrderDetail_OrderStatus));
            OnPropertyChanged(nameof(OrderDetail_bill));
            OnPropertyChanged(nameof(OrderDetail_ship));
            OnPropertyChanged(nameof(OrderDetail_inquiry));
            OnPropertyChanged(nameof(OrderDetail_send));
            OnPropertyChanged(nameof(OrderDetail_detail));
            OnPropertyChanged(nameof(OrderDetail_item));
            OnPropertyChanged(nameof(OrderDetail_Quantity));
            OnPropertyChanged(nameof(Orderdetail_released));
            OnPropertyChanged(nameof(Orderdetail_Email));
            OnPropertyChanged(nameof(Orderdetail_Phone));
            OnPropertyChanged(nameof(Orderdetail_Status));
            //OrderLinedetail
            OnPropertyChanged(nameof(OrderLinedetail));
            OnPropertyChanged(nameof(OrderLinedetail_line));
            OnPropertyChanged(nameof(OrderLinedetail_Quantity));
            OnPropertyChanged(nameof(OrderLine));
            OnPropertyChanged(nameof(OrderLine_itemcode));
            OnPropertyChanged(nameof(OrderLine_TotalQuantity));

            //OrderList
            OnPropertyChanged(nameof(Orderlist));
            OnPropertyChanged(nameof(OrderList_Ordernumber));
            OnPropertyChanged(nameof(OrderList_SalesOrder));
            OnPropertyChanged(nameof(OrderList_Shoppingcart));
            OnPropertyChanged(nameof(OrderList_VendorRef));
            OnPropertyChanged(nameof(OrderList_OrderbOrder));
            OnPropertyChanged(nameof(OrderList_Orderdate));
            OnPropertyChanged(nameof(OrderList_Orderconfirmation));

            //trackinglist
            OnPropertyChanged(nameof(trackingList));   







        }
    }
    public class Dclass : BaseViewModel
    {

    }
}
