﻿using CheckNet.MenuItem;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CheckNet.ViewModels
{
    enum Location
    {
        USA = 1,
        ASIA = 2,
        AUSTRALIA = 3,
        EUROPE = 4
    }
    public class ContactUs1ViewModel
    {
        MainPageViewModel MpV = new MainPageViewModel();
        private Products _oldProduct;
        public string Contactus_Title { get; set; }
        public string Contactus_USA { get; set; }
        public string Contactus_Europe { get; set; }
        public string Contactus_Asia { get; set; }
        public string Contactus_Austraila { get; set; }
        public string Contactus_writeemail { get; set; }
        public ObservableCollection<Products> Products { get; set; }
        public ContactUs1ViewModel()
        {

        }
       

        public ContactUs1ViewModel(int loc)
        {

            switch (loc)
            {
                case (int)Location.USA:
                    #region USA
                    var Contactus_Usa_qustone = MpV.Contactus_Usa_qustone;
                    var Contactus_Usa_qusttwo = MpV.Contactus_Usa_qusttwo;
                    var Contactus_Usa_qustthree = MpV.Contactus_Usa_qustthree;
                    Contactus_Europe = MpV.Contactus_Europe;
                    Contactus_Asia = MpV.Contactus_Asia;
                    Contactus_Title = MpV.Contactus_Title;
                    Contactus_USA = MpV.Contactus_USA;
                    Contactus_Austraila = MpV.Contactus_Austraila;
                    Contactus_writeemail = MpV.Contactus_writeemail;

                    Products = new ObservableCollection<Products>
            {

                new Products
                {

                    Title=Contactus_Usa_qustone,
                   
                    Id="1",
                    Isvisible=false,
                    Icon="add.png",
                    USA="usa",
                    BGColor="Gray",
                    FtColor="White",

        },
                new Products
                {
                    Title=Contactus_Usa_qusttwo,
                     Id="2",
                    Isvisible =false,
                    Icon="add.png",
                    BGColor="Gray",
                    USA="usa",
                    FtColor="White",

                },
                new Products
                {
                    Title=Contactus_Usa_qustthree,
                    Id="3",
                    Isvisible =false,
                    Icon="add.png",
                    USA="usa",
                    BGColor="Gray",
                    FtColor="White",

                },

            };
                    #endregion
                    break;
                case (int)Location.ASIA:
                    #region Asia
                    var Asiaeone = MpV.Contactus_Asia_qustone;
                    var Asiaetwo = MpV.Contactus_Asia_qusttwo;
                    Contactus_Europe = MpV.Contactus_Europe;
                    Contactus_Europe = MpV.Contactus_Europe;
                    Contactus_USA = MpV.Contactus_USA;
                    Contactus_Title = MpV.Contactus_Title;
                    Contactus_Asia = MpV.Contactus_Asia;
                    Contactus_Austraila = MpV.Contactus_Austraila;
                    Contactus_writeemail = MpV.Contactus_writeemail;
                    Products = new ObservableCollection<Products>
            {

                new Products
                {

                    Title=Asiaeone,
                    Id="1",
                   Asia="Asia",
                    Isvisible=false,
                    Icon="add.png",
                    BGColor="Gray",
                    FtColor="White",
                },
                new Products
                {
                    Title=Asiaetwo,
                     Id="2",
                     Asia="Asia",
                    Isvisible =false,
                    Icon="add.png",
                    BGColor="Gray",
                    FtColor="White",
                },

            };
                    #endregion
                    break;
                case (int)Location.EUROPE:
                    #region Europe
                    var Europeone = MpV.Contactus_Europe_qustone;
                    var Europetwo = MpV.Contactus_Europe_qusttwo;
                    var Europethree = MpV.Contactus_Europe_qustthree;
                    var Europefour = MpV.Contactus_Europe_qustfour;
                    Contactus_Europe = MpV.Contactus_Europe;
                    Contactus_Asia = MpV.Contactus_Asia;
                    Contactus_Title = MpV.Contactus_Title;
                    Contactus_USA = MpV.Contactus_USA;
                    Contactus_Austraila = MpV.Contactus_Austraila;
                    Contactus_writeemail = MpV.Contactus_writeemail;
                    Products = new ObservableCollection<Products>
            {

                new Products
                {

                    Title=Europeone,
                    Id="1",
                    Isvisible=false,
                    Europe="Europe",
                    Icon="add.png",
                    BGColor="Gray",
                    FtColor="White",
                },
                new Products
                {
                    Title=Europetwo,
                     Id="2",
                    Isvisible =false,
                    Icon="add.png",
                    BGColor="Gray",
                    Europe="Europe",
                    FtColor="White",
                },
                 new Products
                {
                    Title=Europethree,
                     Id="3",
                    Isvisible =false,
                    Icon="add.png",
                    BGColor="Gray",
                    Europe="Europe",
                    FtColor="White",
                },
                  new Products
                {
                    Title=Europefour,
                     Id="4",
                    Isvisible =false,
                    Icon="add.png",
                    BGColor="Gray",
                    Europe="Europe",
                    FtColor="White",
                },

            };
                    #endregion
                    break;
                case (int)Location.AUSTRALIA:
                    #region Australia
                    var Austeone = MpV.Contactus_Austraila_qustone;
                    Contactus_Austraila = MpV.Contactus_Austraila;
                    Contactus_Europe = MpV.Contactus_Europe;
                    Contactus_Title = MpV.Contactus_Title;
                    Contactus_Asia = MpV.Contactus_Asia;
                    Contactus_USA = MpV.Contactus_USA;
                    Contactus_writeemail = MpV.Contactus_writeemail;
                    Products = new ObservableCollection<Products>
            {

                new Products
                {

                    Title=Austeone,
                    Id="1",
                    Austraila="Australia",
                    Isvisible=false,
                    Icon="add.png",
                    BGColor="Gray",
                    FtColor="White",
                },

            };
                    #endregion
                    break;
            }


        }

        internal void ShoworHiddenProducts(Products product)
        {
            if (_oldProduct == product)
            {
                product.Isvisible = !product.Isvisible;
                if (!product.Isvisible)
                    product.Icon = "add.png";
                else
                    product.Icon = "minus.png";
                UpDateProducts(product);
            }
            else
            {
                if (_oldProduct != null)
                {
                    _oldProduct.Isvisible = false;
                    _oldProduct.Icon = "add.png";
                    UpDateProducts(_oldProduct);

                }
                product.Isvisible = true;
                UpDateProducts(product);
            }
            _oldProduct = product;
        }

        private void UpDateProducts(Products product)
        {
            var Index = Products.IndexOf(product);
            Products.Remove(product);
            Products.Insert(Index, product);
        }



    }
}
