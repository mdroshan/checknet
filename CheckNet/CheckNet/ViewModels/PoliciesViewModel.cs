﻿using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CheckNet.MenuItem
{
    public class PoliciesViewModel
    {
        MainPageViewModel MpV = new MainPageViewModel();
        private Products _oldProduct;
        public ObservableCollection<Products> Products { get; set; }
        public string Policy_title { get; set; }
        public string Policies_Contents { get; set; }
        public PoliciesViewModel()
        {
            var privacy = MpV.Policies_Privacy;
            var Return = MpV.Policies_Return;
          

            Policy_title = MpV.Policies_title;
            Policies_Contents = MpV.Policies_Contents;
            
           

            Products = new ObservableCollection<Products>
            {
               
            //Policies_title = MpV.Policies_title,
            //Policies_Contents = MpV.Policies_Contents,
            new Products
                {

                    Policies_title=MpV.Policies_title,
                    Title=privacy,
                    Id="1",
                    Isvisible=false,
                    Icon="add.png",
                    BGColor="Gray",
                    FtColor="White",

                },
                new Products
                {
                    Policies_title=MpV.Policies_title,
                    Title=Return,
                     Id="2",
                    Isvisible =false,
                    Icon="add.png",
                    BGColor="Gray",
                    FtColor="White",

                },

            };
        }

        internal void ShoworHiddenProducts(Products product)
        {
            if (_oldProduct == product)
            {
                product.Isvisible = !product.Isvisible;
                if (!product.Isvisible)
                    product.Icon = "add.png";
                else
                    product.Icon = "minus.png";
                UpDateProducts(product);
            }
            else
            {
                if (_oldProduct != null)
                {
                    _oldProduct.Isvisible = false;
                    _oldProduct.Icon = "add.png";
                    UpDateProducts(_oldProduct);

                }
                product.Isvisible = true;
                UpDateProducts(product);
            }
            _oldProduct = product;
        }

        private void UpDateProducts(Products product)
        {
            var Index = Products.IndexOf(product);
            Products.Remove(product);
            Products.Insert(Index, product);
        }


    }
}


