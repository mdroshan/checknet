﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CheckNet.ViewModels
{
   public class SplashPage:ContentPage
    {
        Image SplashImage;

        public SplashPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
                var sub = new AbsoluteLayout();
            SplashImage = new Image
            {
                Source = "ChecknetLogo.png"
            };
            //AbsoluteLayout.SetLayoutFlags(
            AbsoluteLayout.SetLayoutFlags(SplashImage, AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(SplashImage, new Rectangle(0.5, 0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
            sub.Children.Add(SplashImage);
            this.Padding = 20;
            this.BackgroundColor = Color.FromHex("#ffffff");
            this.Content = sub;
        }
         protected override async void OnAppearing()
        {
            base.OnAppearing();
            await SplashImage.ScaleTo(1, 2000);
            //await SplashImage.ScaleTo(0.9, 1500,Easing.Linear);
            //await SplashImage.ScaleTo(150, 1200, Easing.Linear);

            Application.Current.MainPage = new CheckNet.MainPage();

            //Application.Current.MainPage = new NavigationPage(new View.Login());
        }

    }
}
