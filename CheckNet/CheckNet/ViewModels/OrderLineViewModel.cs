﻿using CheckNet.MenuItem;
using CheckNet.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace CheckNet.ViewModels
{
    
  public  class OrderLineViewModel
    {
        OrderLineResponse orderline = (OrderLineResponse)Application.Current.Properties["OrderLine"];
        private OrderLineList _oldProduct;
        MainPageViewModel MpV = new MainPageViewModel();
        public string FAQ_HeaderContent { get; set; }
        public ObservableCollection<OrderLineList> OrderLineList { get; set; }
        public OrderLineViewModel()
        {
            OrderLineList = new ObservableCollection<OrderLineList>();
            
            for (int i = 0; i < orderline.OrderLine.Count; i++)
            {


                OrderLineList.Add(  new OrderLineList
                {

                    Id = "1",
                    Position = MpV.OrderLinedetail_line + orderline.OrderLine[i].__position.ToString(),
                    Quantity = MpV.OrderLinedetail_Quantity + orderline.OrderLine[i].__adjustedquantity.ToString(),
                    Icon = "add.png"
                });
            };
            }

        

        internal void ShoworHiddenProducts(OrderLineList OrderLineList)
        {
            if (_oldProduct == OrderLineList)
            {
                OrderLineList.Isvisible = !OrderLineList.Isvisible;
                if (!OrderLineList.Isvisible)
                    OrderLineList.Icon = "add.png";
                else
                    OrderLineList.Icon = "minus.png";
                UpDateProducts(OrderLineList);
            }
            else
            {
                if (_oldProduct != null)
                {
                    _oldProduct.Isvisible = false;
                    _oldProduct.Icon = "add.png";
                    UpDateProducts(_oldProduct);

                }
                OrderLineList.Isvisible = true;
                UpDateProducts(OrderLineList);
            }
            _oldProduct = OrderLineList;
        }

        private void UpDateProducts(OrderLineList product)
        {
            var Index = OrderLineList.IndexOf(product);
            OrderLineList.Remove(product);

            OrderLineList.Insert(Index, product);
        }


    }
}
