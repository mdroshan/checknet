﻿using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CheckNet.MenuItem
{
    public class MainViewModel
    {
        MainPageViewModel MpV = new MainPageViewModel();
        private Products _oldProduct;
        public string FAQ_Title { get; set; }
        public string FAQ_HeaderContent { get; set; }
        public ObservableCollection<Products> Products { get; set; }
        public MainViewModel()
        {
            var Qa1 = MpV.FAQ_QustOne;
            var Qa2 = MpV.FAQ_QuestTwo;
            var Qa3 = MpV.FAQ_QustThree;
            var Qa4 = MpV.FAQ_QustFour;
            var Qa5 = MpV.FAQ_QustFive;
            var Qa6 = MpV.FAQ_QustSix;
            var Qa7 = MpV.FAQ_QustSeven;
            var Qa8 = MpV.FAQ_QustEight;
            var Qa9 = MpV.FAQ_QustNine;
            FAQ_Title = MpV.FAQ_Title;
            FAQ_HeaderContent = MpV.FAQ_HeaderContent;
            Products = new ObservableCollection<Products>
            {
                new Products
                {
                    Title=Qa1,
                    Id="1",
                    Isvisible=false,
                  Icon="add.png"
                    
                },
                new Products
                {
                    Title=Qa2,
                    Id="2",
                    Isvisible =false,
                     Icon="add.png"
                },
                new Products
                {
                    Title=Qa3,
                     Id="3",
                    Isvisible =false,
                    Icon="add.png"

                },
                new Products
                {
                    Title=Qa4,
                     Id="4",
                    Isvisible =false,
                    Icon="add.png"
                },
                new Products
                {
                    Title=Qa5,
                     Id="5",
                    Isvisible =false,
                    Icon="add.png"
                },
                new Products
                {
                    Title=Qa6,
                     Id="6",
                    Isvisible =false,
                    Icon ="add.png"
                },
                new Products
                {
                    Title=Qa7,
                     Id="7",
                    Isvisible =false,
                    Icon ="add.png"
                },
                new Products
                {
                    Title=Qa8,
                     Id="8",
                    Isvisible =false,
                    Icon="add.png"
                },
                new Products
                {
                    Title=Qa9,
                     Id="9",
                    Isvisible =false,
                    Icon ="add.png"
                },
            };
        }

        internal void ShoworHiddenProducts(Products product)
        {
            if (_oldProduct == product)
            {
                product.Isvisible = !product.Isvisible;
                if (!product.Isvisible)
                    product.Icon = "add.png";
                else
                    product.Icon = "minus.png";
                UpDateProducts(product);
            }
            else
            {
                if (_oldProduct != null)
                {
                    _oldProduct.Isvisible = false;
                    _oldProduct.Icon = "add.png";
                    UpDateProducts(_oldProduct);

                }
                product.Isvisible = true;
                UpDateProducts(product);
            }
            _oldProduct = product;
        }

        private void UpDateProducts(Products product)
        {
            var Index = Products.IndexOf(product);
            Products.Remove(product);
           
            Products.Insert(Index, product);
        }


    }
}

