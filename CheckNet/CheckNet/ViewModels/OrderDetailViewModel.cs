﻿using CheckNet.MenuItem;
using CheckNet.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CheckNet.ViewModels
{
   public class OrderDetailViewModel
    {
       
        public ObservableCollection<OrderDetaildata> OrderDetail { get; set; }
        private OrderDetaildata _oldProduct;
        public OrderDetailViewModel()
        {
            OrderDetail = new ObservableCollection<OrderDetaildata>
            {
            new OrderDetaildata
                {
                   Title="Bill To Information",
                    Id="1",
                    
                    Icon="add.png",
               },
                new OrderDetaildata
                {
                   Title="Ship To Information",
                   Id="2",
                  
                   Icon="add.png",
                },
               

            };
        }
        internal void ShoworHiddenProducts(OrderDetaildata product)
        {
            if (_oldProduct == product)
            {
                product.Isvisible = !product.Isvisible;
                if (!product.Isvisible)
                    product.Icon = "add.png";
                else
                    product.Icon = "minus.png";
                UpDateProducts(product);
            }
            else
            {
                if (_oldProduct != null)
                {
                    _oldProduct.Isvisible = false;
                    _oldProduct.Icon = "add.png";
                    UpDateProducts(_oldProduct);

                }
                product.Isvisible = true;
                UpDateProducts(product);
            }
            _oldProduct = product;
        }

        private void UpDateProducts(OrderDetaildata product)
        {
            var Index = OrderDetail.IndexOf(product);
            OrderDetail.Remove(product);
            OrderDetail.Insert(Index, product);
        }


    }

}
