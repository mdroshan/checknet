﻿using CheckNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using Xamarin.Forms;
using I18NPortable;
using System.Diagnostics;
using CheckNet.ViewModels;
using CheckNet.View;
using CheckNet.Models;
using Plugin.DeviceInfo;
using System.Threading.Tasks;
using CheckNet.Service;

namespace CheckNet
{
    public partial class App : Application
    {

        public App()
        {

            InitializeComponent();

            var currentAssembly = GetType().GetTypeInfo().Assembly;



                
            I18N.Current
                    .SetLogger(text => Debug.WriteLine(text))
                    .SetNotFoundSymbol("⛔")
                    .SetFallbackLocale("en")
                    .Init(currentAssembly);

            DeviceInfo device_info = new DeviceInfo();
            device_info.DeviceName = CrossDeviceInfo.Current.DeviceName;
            device_info.DeviceId = CrossDeviceInfo.Current.Id;
            device_info.AppVersion = CrossDeviceInfo.Current.AppVersion;
            device_info.DeviceModel = CrossDeviceInfo.Current.Model;
            device_info.Active = true;
            device_info.DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer;
            device_info.DevicePlatform = CrossDeviceInfo.Current.Platform.ToString();

            Application.Current.Properties["deviceinfo"] = device_info;

            
            MainPage = new NavigationPage(new SplashPage());

            //#if debug
            //    //Api call for login
            //    //Api Call for Org

              // MainPage = new NavigationPage(new OrderCnfrmPage(""));
            //# end

            //MainPage = new NavigationPage(new CarsPage());



            //public Login RootMasterDetail => MainPage as Login;


        }

        async protected override void OnStart()
        {
            //var data = Application.Current.Properties["authorizationdata"];
            //if(data.==true)
            //{
            //    //await Navigation.PushAsync(new View.Home());
            //}
            MainPage mp = new MainPage();
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            //CheckNetWebService API = new CheckNetWebService();
            //API.RefreshToken();
        }
    }
}
