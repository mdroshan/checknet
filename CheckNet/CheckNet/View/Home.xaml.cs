﻿using CheckNet.Models;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    enum EnumFilter
    {
        OrderNumber = 0,
        SalesOrderNumber = 1,
        ShoppingCartId = 2,
        VendorRef = 3,        
    }


    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : ContentPage
    {
        MainPageViewModel MpV = new MainPageViewModel();
        organization_info Org_Detail;
        public ObservableCollection<OrderListResponse> orderlist { get; set; }
        OrganizationWebService api = new OrganizationWebService();
        Picker picker;
        SearchBar Searchcntrl;
        Dictionary<string, int> OrderFilters;
        Int32 CurrentPage = 1;
        int value = 0;
        public Home()
        {
            InitializeComponent();
            Org_Detail = (organization_info)Application.Current.Properties["organizationdetails"];
            picker = LoadPicker(0);
            picker.SelectedIndexChanged += myPickerSelectedIndexChanged;
            Title = MpV.Home;
            Searchcntrl = LoadSearchBar();
            Searchcntrl.SearchButtonPressed += SearchButtonClicked;

            CalculatorGridCode((int)EnumFilter.OrderNumber, "");


        }
        public SearchBar LoadSearchBar()
        {
            Searchcntrl = new SearchBar { Placeholder = MpV.Search, FontSize = 20 };
            Searchcntrl.Text = "";
            return Searchcntrl;
        }

        private void SearchButtonClicked(object sender, EventArgs e)
        {

            string selectedValue = picker.Items[picker.SelectedIndex];
            int value = OrderFilters[selectedValue];
            string searchdata = Searchcntrl.Text;
            CalculatorGridCode(value, searchdata);

        }

        string OrderType = "";
        int OrderCnt = 0;

        public Picker LoadPicker(int selectedvalue)
        {
            OrderFilters = new Dictionary<string, int>
            {
                //{ "OrderNumber", 0 },
                //{ "SalesOrderNumber", 3 },
                //{ "ShoppingCartId", 1 },
                //{ "VendorRef ", 2 },
                { MpV.Pickerone, 0 },
                { MpV.Pickertwo, 1 },
                { MpV.Pickerthree, 2 },
                { MpV.PickerFour, 3 },
            };
            picker = new Picker
            {
                VerticalOptions = LayoutOptions.StartAndExpand,
                BackgroundColor = Color.White,
            };
            foreach (string Filter in OrderFilters.Keys)
            {
                picker.Items.Add(Filter);
            }

            picker.SelectedIndex = selectedvalue;
            return picker;
        }

        public async void CalculatorGridCode(int EnumFilterValue, string SearchValue)
        {
            OrdersCountResponse oc = new OrdersCountResponse
            {
                OrderNumber = "",
                ShoppingCartId = "",
                SalesOrderNumber = "",
                VendorRef = "",
                OrganizationDetail = new organization_info()
            };
            if (SearchValue != "")
            {
                if (EnumFilterValue == 0)
                {
                    oc.OrderNumber = SearchValue;
                }
                else if (EnumFilterValue == 1)
                {
                    oc.ShoppingCartId = SearchValue;
                }
                else if (EnumFilterValue == 2)
                {
                    oc.VendorRef = SearchValue;
                }
                else if (EnumFilterValue == 3)
                {
                    oc.SalesOrderNumber = SearchValue;
                }
            }
            oc.OrganizationDetail.UserId = Org_Detail.UserId;
            EnumFilter val = (EnumFilter)EnumFilterValue;
            string stringValue = val.ToString();
            oc.SearchType = stringValue;
            OrdersCountResponse lsts = await api.GetOrderCounts(oc);
            Application.Current.Properties["Order counts"] = lsts;
            #region Dev
            //OrdersCount oc = new OrdersCount();
            //MobileOrderCountList obj1 = new MobileOrderCountList { RetailerId = 1, RetailerName = "Urban Outfitters", HasApproval = true, ApprovalCount = 1, HasStagedOrder = false, NewOrderCount = 2, HasReleasedOrder = true, ReleasedOrderCount = 3 };
            //MobileOrderCountList obj2 = new MobileOrderCountList { RetailerId = 2, RetailerName = "Urban ", HasApproval = false, ApprovalCount = 0, HasStagedOrder = true, NewOrderCount = 4, HasReleasedOrder = true, ReleasedOrderCount = 5 };
            //MobileOrderCountList obj3 = new MobileOrderCountList { RetailerId = 3, RetailerName = "Outfitters ", HasApproval = true, ApprovalCount = 0, HasStagedOrder = true, NewOrderCount = 0, HasReleasedOrder = true, ReleasedOrderCount = 6 };
            //MobileOrderCountList obj4 = new MobileOrderCountList { RetailerId = 3, RetailerName = "Test ", HasApproval = false, ApprovalCount = 0, HasStagedOrder = true, NewOrderCount = 0, HasReleasedOrder = false, ReleasedOrderCount = 6 };
            //List<MobileOrderCountList> lsts = new List<MobileOrderCountList>();
            //lsts.Add(obj1);
            //lsts.Add(obj2);
            //lsts.Add(obj3); lsts.Add(obj4);
            #endregion
            oc.MobileOrderCountList = lsts.MobileOrderCountList;
            Title = "Home";
            BackgroundColor = Color.FromHex("#fff");

            #region Styles
            var DataButton = new Style(typeof(Button))
            {
                Setters = {
      new Setter { Property = Button.BackgroundColorProperty, Value = Color.FromHex ("#b0b0b0") },
      new Setter { Property = Button.TextColorProperty, Value = Color.Black },
      new Setter { Property = Button.BorderRadiusProperty, Value = 0 },
      new Setter { Property = Button.FontSizeProperty, Value = 10 },
      new Setter {  Property = Button.BorderColorProperty, Value= Color.White}
    }
            };
            var Headerlabel = new Style(typeof(Label))
            {
                Setters = {
      new Setter { Property = Label.BackgroundColorProperty, Value = Color.FromHex ("#5e6a71") },
      new Setter { Property = Label.TextColorProperty, Value = Color.White },
      new Setter { Property = Label.FontSizeProperty, Value = 10 }
    }
            };

            #endregion

            #region CreateGridPage
            var controlGrid = new Grid { RowSpacing = 1, ColumnSpacing = 1 };
            controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });
            controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });


            #region DDL and Search
            //DDL and Search

            controlGrid.Children.Add(picker, 0, 0);
            Grid.SetColumnSpan(picker, 3);


            controlGrid.Children.Add(Searchcntrl, 3, 0);
            Grid.SetColumnSpan(Searchcntrl, 3);
            #endregion


            #region DynamicRow
            //Header
            int Headercnt = 1;
            int Datacnt = 2;
            for (int i = 0; i <= oc.MobileOrderCountList.Count - 1; i++)
            {
                int cnt = Convert.ToInt32(oc.MobileOrderCountList[i].HasApproval) + Convert.ToInt32(oc.MobileOrderCountList[i].HasStagedOrder) + Convert.ToInt32(oc.MobileOrderCountList[i].HasReleasedOrder);

                controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(45) });
                controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(60) });


                var label = new Label
                {
                    Text = oc.MobileOrderCountList[i].RetailerName,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center,
                    TextColor = Color.White,
                    Style = Headerlabel,
                    FontSize = 20
                };
                controlGrid.Children.Add(label, 0, Headercnt);
                Grid.SetColumnSpan(label, 6);

                if (cnt == 1)
                {

                    GetorderType_Count(out OrderType, out OrderCnt, cnt, oc.MobileOrderCountList[i], 1);
                    var Btn1 = new Button { Text = OrderType + OrderCnt, Style = DataButton };
                    controlGrid.Children.Add(Btn1, 0, Datacnt);
                    Grid.SetColumnSpan(Btn1, 6);
                    Btn1.Clicked += new EventHandler(Orders_Clicked);
                }
                else if (cnt == 2)
                {
                    //Button
                    GetorderType_Count(out OrderType, out OrderCnt, cnt, oc.MobileOrderCountList[i], 1);
                    var Btn1 = new Button { Text = OrderType + OrderCnt, Style = DataButton };
                    controlGrid.Children.Add(Btn1, 0, Datacnt);
                    Grid.SetColumnSpan(Btn1, 3);
                    Btn1.Clicked += new EventHandler(Orders_Clicked);


                    GetorderType_Count(out OrderType, out OrderCnt, cnt, oc.MobileOrderCountList[i], 2);
                    var Btn2 = new Button { Text = OrderType + OrderCnt, Style = DataButton };
                    controlGrid.Children.Add(Btn2, 3, Datacnt);
                    Grid.SetColumnSpan(Btn2, 3);
                    Btn2.Clicked += new EventHandler(Orders_Clicked);
                }
                else if (cnt == 3)
                {
                    //Button
                    GetorderType_Count(out OrderType, out OrderCnt, cnt, oc.MobileOrderCountList[i], 1);
                    var Btn1 = new Button { Text = OrderType + OrderCnt, Style = DataButton };
                    controlGrid.Children.Add(Btn1, 0, Datacnt);
                    Grid.SetColumnSpan(Btn1, 2);
                    Btn1.Clicked += new EventHandler(Orders_Clicked);

                    GetorderType_Count(out OrderType, out OrderCnt, cnt, oc.MobileOrderCountList[i], 2);
                    var Btn2 = new Button { Text = OrderType + OrderCnt, Style = DataButton };
                    controlGrid.Children.Add(Btn2, 2, Datacnt);
                    Grid.SetColumnSpan(Btn2, 2);
                    Btn1.Clicked += new EventHandler(Orders_Clicked);


                    GetorderType_Count(out OrderType, out OrderCnt, cnt, oc.MobileOrderCountList[i], 3);
                    var Btn3 = new Button { Text = OrderType + OrderCnt, Style = DataButton };
                    controlGrid.Children.Add(Btn3, 4, Datacnt);
                    Btn1.Clicked += new EventHandler(Orders_Clicked);
                    Grid.SetColumnSpan(Btn3, 2);
                }

                Headercnt = Datacnt + 1;
                Datacnt = Datacnt + 2;

            }
            #endregion

            controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Content = controlGrid;

            #endregion
        }

        private void GetorderType_Count(out string OrderType, out int OrderCnt, int cnt, MobileOrderCountList mc, int ocnt)
        {
            OrderType = "";
            OrderCnt = 0;
            switch (cnt)
            {
                case 1:
                    if (mc.HasApproval)
                    {
                        OrderType = "Approval :";
                        OrderCnt = mc.ApprovalCount;


                    }
                    else if (mc.HasStagedOrder)
                    {
                        OrderType = "Staged :";
                        OrderCnt = mc.NewOrderCount;
                    }
                    else if (mc.HasReleasedOrder)
                    {
                        OrderType = "Released :";
                        OrderCnt = mc.ReleasedOrderCount;
                    }
                    break;
                case 2:
                    if (ocnt == 1)
                    {
                        if (mc.HasApproval)
                        {
                            OrderType = "Approval :";
                            OrderCnt = mc.ApprovalCount;
                        }
                        else if (mc.HasStagedOrder)
                        {
                            OrderType = "Staged :";
                            OrderCnt = mc.NewOrderCount;
                        }
                    }
                    else if (ocnt == 2)
                    {

                        if (mc.HasReleasedOrder)
                        {
                            OrderType = "Released :";
                            OrderCnt = mc.ReleasedOrderCount;
                        }
                        else if (mc.HasStagedOrder)
                        {
                            OrderType = "Staged :";
                            OrderCnt = mc.NewOrderCount;
                        }
                    }
                    else if (ocnt == 3)
                    {
                        OrderType = "Released :";
                        OrderCnt = mc.ReleasedOrderCount;
                    }

                    break;
                case 3:
                    if (ocnt == 1)
                    {
                        if (mc.HasApproval)
                        {
                            OrderType = "Approval :";
                            OrderCnt = mc.ApprovalCount;
                        }
                    }
                    else if (ocnt == 2)
                    {
                        if (mc.HasStagedOrder)
                        {
                            OrderType = "Staged Orders:";
                            OrderCnt = mc.NewOrderCount;
                        }
                    }
                    else if (ocnt == 3)
                    {
                        if (mc.HasReleasedOrder)
                        {
                            OrderType = "Recent Orders :";
                            OrderCnt = mc.ReleasedOrderCount;
                        }
                    }
                    break;
            }

        }

        public void myPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            //Method call every time when picker selection changed.
            string selectedValue = picker.Items[picker.SelectedIndex];
            int value = OrderFilters[selectedValue];
            string SearchValue = Searchcntrl.Text;
            CalculatorGridCode(value, SearchValue);
        }


        //public void SearchButtonClicked(object sender, EventArgs e)
        //{            

        //}

        async void Notification_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new View.Alerts_and_News());
        }
        public async void Orders_Clicked(Object sender, EventArgs e)
        {
            string Ordertype = "0";
            Button slctbtn = sender as Button;
            string[] splitvalue = slctbtn.Text.Split(':');

            if (splitvalue[0].ToString().Trim() == "Approvals")
            {
                Ordertype = "1";
            }

            if (splitvalue[0].ToString().Trim() == "Staged")
            {
                Ordertype = "2";
            }
            if (splitvalue[0].ToString().Trim() == "Released")
            {
                Ordertype = "3";
            }
            //Order Summary api call
            OrganizationWebService org = new OrganizationWebService();
            OrderSummaryRequest orderlist = new OrderSummaryRequest();
            string selectedValue = ""; // picker.Items[picker.SelectedIndex];
            if (picker.SelectedIndex == 0)
            {
                selectedValue = "OrderNumber";
            }
            else if (picker.SelectedIndex == 1)
            {
                selectedValue = "SalesOrderNumber";
            }
            else if (picker.SelectedIndex == 2)
            {
                selectedValue = "ShoppingCartId";
            }
            else if (picker.SelectedIndex == 3)
            {
                selectedValue = "VendorRef";
            }
            string selectedValue1 = picker.Items[picker.SelectedIndex];
            value = OrderFilters[selectedValue1];
            EnumFilter val = new EnumFilter();
            string stringValue = value.ToString();

            orderlist.PageSize = 5;
            orderlist.PageNumber = CurrentPage;
            orderlist.OrderNumber = "";
            orderlist.SalesOrderNumber = "";
            orderlist.ShoppingCartId = "";
            orderlist.VendorRef = "";
            orderlist.UserId = Org_Detail.UserId;
            orderlist.RetailerId = Org_Detail.OrgContext.RetailerId;
            orderlist.OrderType = Convert.ToInt32(Ordertype);
            string[] b = { Searchcntrl.Text };



            orderlist.SearchList = b;
            orderlist.SearchType = selectedValue.ToString();
            ObservableCollection<OrderSummaryResponse> getresponse = await org.GetOrderSummary(orderlist);
            Application.Current.Properties["Order Summary"] = getresponse;
            //CurrentPage = CurrentPage + 1;
            await Navigation.PushAsync(new View.Listpage(Convert.ToInt32(orderlist.OrderType), stringValue.ToString(),1));
        }





    }
}

