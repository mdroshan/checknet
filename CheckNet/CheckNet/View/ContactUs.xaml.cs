﻿using CheckNet.MenuItem;
using CheckNet.Models;
using CheckNet.Service;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    enum Location
    {
        USA = 1,
        ASIA = 2,
        AUSTRALIA = 3,
        EUROPE = 4
    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactUs : ContentPage
    {

        MainPageViewModel MpV = new MainPageViewModel();
        public ContactUs()
        {
            

            //BindingContext = new MainPageViewModel
            //{
            //    CurrentPage = this
            //};
            InitializeComponent();
            ContactUs1ViewModel data = new ContactUs1ViewModel((int)Location.USA);
            BindingContext = data;

            //BindingContext = MpV.Contactus_Title;


        }
       
        async void Email_clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new View.Email());
            BackgroundColor = Color.White;

        }


        private void ContactUs1_Tabbed(object sender, ItemTappedEventArgs e)
        {
            var product = e.Item as Products;
            if (product.USA == "usa")
            {
                #region USA
                if (product.Id != "0")
                {
                     product.Contactus_Title =MpV.Contactus_Title;
                    var Contactus_Usa_Answone = MpV.Contactus_Usa_Answone;
                    if (product.Id == "1")
                    {
                        product.ContactUs1_Content = Contactus_Usa_Answone;
                        product.Icon = "minus.png";
                    }
                    else if (product.Id == "2")
                    {
                        var Contactus_Usa_Anstwo = MpV.Contactus_Usa_Anstwo;
                        product.ContactUs1_Content = Contactus_Usa_Anstwo;
                        product.Icon = "minus.png";
                    }
                    else
                    {
                        var Contactus_Usa_Ansthree = MpV.Contactus_Usa_Ansthree;
                        product.ContactUs1_Content = Contactus_Usa_Ansthree;
                        product.Icon = "minus.png";
                    }
                    var vm1 = BindingContext as ContactUs1ViewModel;
                    vm1?.ShoworHiddenProducts(product);
                }
            }
            #endregion
            if (product.Asia == "Asia")
            {
                #region asia
                var asiaone = MpV.Contactus_Asia_Answone;
                var asiatwo = MpV.Contactus_Asia_Answtwo;
                if (product.Id == "1")
                {
                    product.ContactUs1_Content = asiaone;
                    product.Icon = "minus.png";
                }
                else if (product.Id == "2")
                {
                    product.ContactUs1_Content = asiatwo;
                    product.Icon = "minus.png";
                }


                var vm2 = BindingContext as ContactUs1ViewModel;
                vm2?.ShoworHiddenProducts(product);
                #endregion
            }
            if (product.Europe == "Europe")
            {
                #region europe
                var eurpone = MpV.Contactus_Europe_Anstone;
                var eurptwo = MpV.Contactus_Europe_Answtwo;
                var eurpthree = MpV.Contactus_Europe_Answthree;
                var eurpfour = MpV.Contactus_Europe_Answfour;
                if (product.Id == "1")
                {
                    product.ContactUs1_Content = eurpone;
                    product.Icon = "minus.png";
                }
                else if (product.Id == "2")
                {
                    product.ContactUs1_Content = eurptwo;
                    product.Icon = "minus.png";
                }
                else if (product.Id == "3")
                {
                    product.ContactUs1_Content = eurpthree;
                    product.Icon = "minus.png";
                }
                else if (product.Id == "4")
                {
                    product.ContactUs1_Content = eurpfour;
                    product.Icon = "minus.png";
                }


                var vm4 = BindingContext as ContactUs1ViewModel;
                vm4?.ShoworHiddenProducts(product);
                #endregion
            }
            #region australia
            if (product.Austraila == "Australia") {
            var austone = MpV.Contactus_Austraila_Answone;
            if (product.Id == "1")
            {
                product.ContactUs1_Content = austone;
                product.Icon = "minus.png";
            }


            var vm3 = BindingContext as ContactUs1ViewModel;
            vm3?.ShoworHiddenProducts(product);
            #endregion
        }
        }
        public void ResetBtnColor()
        {
            btn_USA.BackgroundColor = Color.FromHex("#ffffff");
            btn_Europe.BackgroundColor = Color.FromHex("#ffffff");
            btn_Asia.BackgroundColor = Color.FromHex("#ffffff");
            btn_Australia.BackgroundColor = Color.FromHex("#ffffff");

            btn_USA.TextColor = Color.FromHex("#d52b1e");
            btn_Europe.TextColor = Color.FromHex("#d52b1e");
            btn_Asia.TextColor = Color.FromHex("#d52b1e");
            btn_Australia.TextColor = Color.FromHex("#d52b1e");
        }

        private void USAClicked(object sender, ItemTappedEventArgs e)
        {
            ResetBtnColor();
            btn_USA.BackgroundColor = Color.FromHex("#d52b1e");
            btn_USA.TextColor = Color.FromHex("#ffffff");

            ContactUs1ViewModel data = new ContactUs1ViewModel((int)Location.USA);
            BindingContext = data;
            

        }
        private void EuropeClicked(object sender, ItemTappedEventArgs e)
        {
            ResetBtnColor();
            btn_Europe.BackgroundColor = Color.FromHex("#d52b1e");
            btn_Europe.TextColor = Color.FromHex("#ffffff");

            ContactUs1ViewModel data = new ContactUs1ViewModel((int)Location.EUROPE);
            BindingContext = data;
        }
        private void AsiaClicked(object sender, ItemTappedEventArgs e)
        {
            ResetBtnColor();
            btn_Asia.BackgroundColor = Color.FromHex("#d52b1e");
            btn_Asia.TextColor = Color.FromHex("#ffffff");
            ContactUs1ViewModel data = new ContactUs1ViewModel((int)Location.ASIA);
            BindingContext = data;
        }

        private void AustraliaClicked(object sender, ItemTappedEventArgs e)
        {
            ResetBtnColor();
            btn_Australia.BackgroundColor = Color.FromHex("#d52b1e");
            btn_Australia.TextColor = Color.FromHex("#ffffff");

            ContactUs1ViewModel data = new ContactUs1ViewModel((int)Location.AUSTRALIA);
            BindingContext = data;

        }
        //private void Email_clicked(object sender, ItemTappedEventArgs e)
        //{
            
        //}

        //private void ListViewItem_Tabbed(object sender, ItemTappedEventArgs e)
        //{
        //    var product = e.Item as Products;
        //    var usaaOne = MpV.Contactus_Usa_Answone;
        //    var usatwo = MpV.Contactus_Usa_Anstwo;
        //    var usathree = MpV.Contactus_Usa_Ansthree;
        //    if (product.Id == "1")
        //    {
        //        product.UsaUs_Content = usaaOne;
        //    }
        //    else if (product.Id == "2")
        //    {
        //        product.UsaUs_Content = usatwo;

        //    }
        //    else if (product.Id == "3")
        //    {
        //        product.UsaUs_Content = usathree;

        //    }

        //    var vm = BindingContext as SampleViewModel;
        //    //var vm = BindingContext as SampleViewModel;
        //    vm?.ShoworHiddenProducts(product);
        //}


        //private void ListViewItem_Tabbed(object sender, ItemTappedEventArgs e)
        //{
        //    var privacypolicy = MpV.Contactus_Usa_Answone;
        //    var product = e.Item as Products;

        //    if (product.Id == "1")
        //    {
        //        product.FAQ_Content = privacypolicy;
        //    }
        //    else if (product.Id == "2")
        //    {

        //    }
        //    else if (product.Id == "3")
        //    {

        //    }
        //    var vm = BindingContext as ContactUsViewModelUSA;
        //    vm?.ShoworHiddenProducts(product);
        //}
        //private void EuropeItem_Tabbed(object sender, ItemTappedEventArgs e)
        //{
        //    var BindingContext = new ContactUsEuropeViewModel();
        //    var faq = e.Item as Products;
        //    var EurpOne = MpV.Contactus_Europe_Anstone;
        //    var Eurptwo = MpV.Contactus_Europe_Answtwo;
        //    var Eurpthree = MpV.Contactus_Europe_Answthree;
        //    var Eurpfour = MpV.Contactus_Europe_Answfour;
        //    if (faq.Id == "1")
        //    {
        //        faq.Europe_Content = EurpOne;
        //    }
        //    else if (faq.Id == "2")
        //    {
        //        faq.Europe_Content = Eurptwo;
        //    }
        //    else if (faq.Id == "3")
        //    {
        //        faq.Europe_Content = Eurpthree;
        //    }
        //    else if (faq.Id == "4")
        //    {
        //        faq.Europe_Content = Eurpfour;
        //    }


        //    var vm = BindingContext as ContactUsEuropeViewModel;
        //    vm?.ShoworHiddenProducts(faq);
        //}
        //private void AsiaItem_Tabbed(object sender, ItemTappedEventArgs e)
        //{
        //    var BindingContext = new ContactUsAsiaViewModel();
        //    var faq = e.Item as Products;
        //    var AsiaOne = MpV.Contactus_Asia_Answone;
        //    var Asiatwo = MpV.Contactus_Asia_Answtwo;
        //    if (faq.Id == "1")
        //    {
        //        faq.Asia_Content = AsiaOne;
        //    }
        //    else if (faq.Id == "2")
        //    {
        //        faq.Asia_Content = Asiatwo;
        //    }


        //    var vm = BindingContext as ContactUsAsiaViewModel;
        //    vm?.ShoworHiddenProducts(faq);
        //}

        //private void AustrailaItem_Tabbed(object sender, ItemTappedEventArgs e)
        //{
        //    var BindingContext = new ContactusAustrailaViewModel();
        //    var faq = e.Item as Products;
        //    var Austone = MpV.Contactus_Austraila_Answone;
        //    if (faq.Id == "1")
        //    {
        //        faq.Asia_Content = Austone;
        //    }


        //    var vm = BindingContext as ContactusAustrailaViewModel;
        //    vm?.ShoworHiddenProducts(faq);
        //}
    }
}
