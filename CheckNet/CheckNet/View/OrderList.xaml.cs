﻿using CheckNet.Models;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderList : ContentPage
    {
        MainPageViewModel MpV = new MainPageViewModel();
        organization_info Org_Detail = (organization_info)Application.Current.Properties["organizationdetails"];
        OrganizationWebService org = new OrganizationWebService();
        List<OrderListResponse> mylist = new List<OrderListResponse>();
        ObservableCollection<OrderListResponse> orderlistresponse = (ObservableCollection<OrderListResponse>)Application.Current.Properties["OrderList"];
        public OrderList()
        {
            InitializeComponent();
           
        }
        string Currentsearchtype;

       


        public OrderList(string currentsearch)
        {
            var OrderList_Orderconfirmation = MpV.OrderList_Orderconfirmation;
            var OrderList_Ordernumber = MpV.OrderList_Ordernumber;
            var Orderlist = MpV.Orderlist;
            InitializeComponent();
            Currentsearchtype = currentsearch;
            orderlistResponse(currentsearch);
        }
        public void orderlistResponse(string currentsearch)
        {
            if (currentsearch == "3")
            {

                SalesOrderNumber.Text = MpV.OrderList_SalesOrder + orderlistresponse[0].SalesOrderNumber;
                OrganizationName.Text = orderlistresponse[0].OrganizationName;
                OrderedBy.Text = "Order By :" + orderlistresponse[0].OrderedBy;
                OrderDate.Text = MpV.OrderList_Orderdate + orderlistresponse[0].OrderDate.ToString();
                for (int i = 0; i < orderlistresponse.Count; i++)
                {
                    mylist.Add(new OrderListResponse { OrderNumber = orderlistresponse[i].OrderNumber, POCtrlNo = orderlistresponse[i].POCtrlNo, });
                }
                
            }
            else if(currentsearch== "1")
            {

                SalesOrderNumber.Text = MpV.OrderList_Shoppingcart + orderlistresponse[0].ShoppingCartId;
                OrganizationName.Text = orderlistresponse[0].OrganizationName;
                OrderedBy.Text = "Order By :" + orderlistresponse[0].OrderedBy;
                OrderDate.Text = MpV.OrderList_Orderdate + orderlistresponse[0].OrderDate.ToString();
                for (int i = 0; i < orderlistresponse.Count; i++)
                {
                    mylist.Add(new OrderListResponse { OrderNumber = orderlistresponse[i].OrderNumber, POCtrlNo = orderlistresponse[i].POCtrlNo, });
                }
                
            }
            else if (currentsearch == "2")
            {

                SalesOrderNumber.Text = MpV.OrderList_VendorRef + orderlistresponse[0].VendorRef;
                OrganizationName.Text = orderlistresponse[0].OrganizationName;
                OrderedBy.Text = "Order By :" + orderlistresponse[0].OrderedBy;
                OrderDate.Text = MpV.OrderList_Orderdate + orderlistresponse[0].OrderDate.ToString(" dd MMMM yyyy");
                for (int i = 0; i < orderlistresponse.Count; i++)
                {
                    mylist.Add(new OrderListResponse { OrderNumber = orderlistresponse[i].OrderNumber, POCtrlNo = orderlistresponse[i].POCtrlNo, });
                }
                
            }
            list.ItemsSource = mylist;
        }
        async void Order_Details(object sender, ItemTappedEventArgs e)
        {
            var product = e.Item as OrderListResponse;
            string POCID = product.POCtrlNo.ToString();
           

            OrdernumberPocRequest request = new OrdernumberPocRequest();
            request.ProductionOrderId = Convert.ToInt64(POCID);
            request.OrganizationDetail = Org_Detail;
            OrdernumberPocResponse responsepoc = await org.GetOrderDetailByPOCtrlNo(request);
            Application.Current.Properties["OrderDetailByPOCtrlNo"] = responsepoc;
            await Navigation.PushAsync(new View.OrderdetailPage());

        }


        //async void orderconfirmation_Clicked(object sender, EventArgs e)
        //{
        //    OrdernumberPocResponse OrderDetailByPOCtrlNo = (OrdernumberPocResponse)Application.Current.Properties["OrderDetailByPOCtrlNo"];
        //    OrderconfirmationRequest requet = new OrderconfirmationRequest();
        //    requet.ShoppingCartId = OrderDetailByPOCtrlNo.MobileOrderDetail.ShoppingCartId;
        //    requet.OrgContext = Org_Detail.OrgContext;
        //    string Result= await org.GetConfirmationHtml(requet);
        //    await Navigation.PushAsync(new View.OrderCnfrmPage(Result));
        //}

    }
}