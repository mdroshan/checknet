﻿using CheckNet.MenuItem;
using CheckNet.Models;
using CheckNet.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Notification_Setting : ContentPage
    {
        List<Notification_setting> res = new List<Notification_setting>();
        NotificationWebService api = new NotificationWebService();
        public Notification_Setting()
        {
            InitializeComponent();
            NotificationSetting();
        }

        public async void NotificationSetting()
        {
            var response = await api.Notification_Setting();
            
            for (int i = 0; i < response.Count; i++)
            {
                if(i==0)
                { res.Add(new Notification_setting { IconClass = "ck_clear.png", SubscriptinDescription = "Clear Badge Count", IsEnabled = false }); }
                
                if (response[i].SubscriptionType != 108)
                {
                    if(response[i].IconClass == "ck-shipment")
                    {
                        res.Add(new Notification_setting { IconClass ="ck_shipment.png", SubscriptinDescription = response[i].SubscriptinDescription, IsEnabled = response[i].IsEnabled });
                    }
                    else if (response[i].IconClass == "ck-printshop")
                    {
                        res.Add(new Notification_setting { IconClass = "ck_printshop.png", SubscriptinDescription = response[i].SubscriptinDescription, IsEnabled = response[i].IsEnabled });
                    }
                    else if (response[i].IconClass == "ck-maintance")
                    {
                        res.Add(new Notification_setting { IconClass = "ck_message.png", SubscriptinDescription = response[i].SubscriptinDescription, IsEnabled = response[i].IsEnabled });
                    }
                    else if (response[i].IconClass == "ck-order")
                    {
                        res.Add(new Notification_setting { IconClass = "ck_printshop.png", SubscriptinDescription = response[i].SubscriptinDescription, IsEnabled = response[i].IsEnabled });
                    }
                    else if (response[i].IconClass == "ck-recieved")
                    {
                        res.Add(new Notification_setting { IconClass = "ck_received.png", SubscriptinDescription = response[i].SubscriptinDescription, IsEnabled = response[i].IsEnabled });
                    }
                    else if (response[i].IconClass == "ck-message")
                    {
                        res.Add(new Notification_setting { IconClass = "ck_message.png", SubscriptinDescription = response[i].SubscriptinDescription, IsEnabled = response[i].IsEnabled });
                    }
                    else if (response[i].IconClass == "ck-clear")
                    {
                        res.Add(new Notification_setting { IconClass = "ck_clear.png", SubscriptinDescription = response[i].SubscriptinDescription, IsEnabled = response[i].IsEnabled });
                    }
                }              
            }
            list.ItemsSource = res;
        }

    }

}