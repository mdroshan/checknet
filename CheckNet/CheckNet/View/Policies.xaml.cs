﻿using CheckNet.MenuItem;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Policies : ContentPage
    {
        MainPageViewModel MpV = new MainPageViewModel();
        
        public Policies()
        {
            BindingContext = new MainPageViewModel
            {
                CurrentPage = this
            };
            InitializeComponent();
           
            
        }
        private void ListViewItem_Tabbed(object sender, ItemTappedEventArgs e)
        {


            var product = e.Item as Products;
            if (product.Id != "0")
            {
               
                var privacypolicy = MpV.Policies_Privacy_Cont;
                if (product.Id == "1")
                {

                    product.FAQ_Content = privacypolicy;
                    product.Icon = "minus.png";
                }
                else
                {
                    var ReturnPolicy = MpV.Policies_Return_cont;
                    product.FAQ_Content = ReturnPolicy;
                    product.Icon = "minus.png"; 
                }
                var vm = BindingContext as PoliciesViewModel;
                vm?.ShoworHiddenProducts(product);
            }
        }




    }
}