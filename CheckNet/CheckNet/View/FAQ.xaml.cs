﻿using CheckNet.MenuItem;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FAQ : ContentPage
	{
        MainPageViewModel MpV = new MainPageViewModel();
        
        public FAQ ()
		{
            BindingContext = new MainPageViewModel
            {
                CurrentPage = this
            };
            //MenuList faq = new MenuList();
            //faq.FAQ_Content = "This Following list contains a serios of frequently Asked Questions(FAQ)and their repective answers.the list is provided as the first source of information for users when there is an operational question regarding login procedure or the credit application process.Users should refer to this list first and prior to containing their Checkpoint systems Customer Service Respresentative.(CSR)";
            //BindingContext = faq;
            //string a =MpV.FAQ_Title;            
            InitializeComponent ();
            //Products product = new Products();
            //product.FAQ_Title = MpV.FAQ_Title;
           
            
        }
   
        
        private void ListViewItem_Tabbed(object sender, ItemTappedEventArgs e)
        {
           
            var faq = e.Item as Products;
            var Aone = MpV.FAQ_AnswrOne;
            var logo = e.Item;

 
            if (faq.Id == "1")
            {
                faq.Policies_Content = Aone;
                faq.Icon = "minus.png";
            }
            else if(faq.Id == "2")
            {
                var Atwo = MpV.FAQ_AnswrTwo;
                faq.Policies_Content = Atwo;
                faq.Icon = "minus.png";
            }
            else if (faq.Id == "3")
            {
                var Athree = MpV.FAQ_AnswrTwo;
                faq.Policies_Content = Athree;
                faq.Icon = "minus.png";
            }
            else if (faq.Id == "4")
            {
                var Afour = MpV.FAQ_AnswrFour;
                faq.Policies_Content = Afour;
                faq.Icon = "minus.png";
            }
            else if (faq.Id == "5")
            {
                var Afive = MpV.FAQ_AnswrFive;
                faq.Policies_Content = Afive;
                faq.Icon = "minus.png";
            }
            else if (faq.Id == "6")
            {
                var Asix = MpV.FAQ_AnswrSix;
                faq.Policies_Content = Asix;
                faq.Icon = "minus.png";
            }
            else if (faq.Id == "7")
            {
                var Aseven = MpV.FAQ_AnswrSeven;
                faq.Policies_Content = Aseven;
                faq.Icon = "minus.png";
            }
            else if (faq.Id == "8")
            {
                var Aeight = MpV.FAQ_AnswrEight;
                faq.Policies_Content = Aeight;
                faq.Icon = "minus.png";
            }
            else if (faq.Id == "9")
            {
                var Anine = MpV.FAQ_AnswrNine;
                faq.Policies_Content = Anine;
                faq.Icon = "minus.png";
            }
            var vm = BindingContext as MainViewModel;
            vm?.ShoworHiddenProducts(faq);
        }
    }
}