﻿using CheckNet.MenuItem;
using CheckNet.Models;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderDetail : ContentPage
    {
        organization_info Org_Detail = (organization_info)Application.Current.Properties["organizationdetails"];
        OrganizationWebService org = new OrganizationWebService();
        OrdernumberPocResponse OrderDetailByPOCtrlNo = (OrdernumberPocResponse)Application.Current.Properties["OrderDetailByPOCtrlNo"];
        List<Orderlinelist> mylist = new List<Orderlinelist>();

        public OrderDetail()
        {
            InitializeComponent();


            //OrderdetailResponse();
        }

        //public void OrderdetailResponse()
        //{

        //    Address.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.OrganizationName;
        //    OrderDate.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.OrderDate.ToString();
        //    ShoppingCartNo.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.ShoppingCartId;
        //    OrderNumber.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.OrderNumber;
        //    salesOrderNumber.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.SalesOrderNumber;
        //    vendorrefNumber.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.VendorRef;
        //    EstimatedShipDate.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.EstimatedShipDate.ToString();
        //    shipdate.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.DueDate.ToString();
        //    Cutnumber.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.CutNumber;

        //    for (int i = 0; i < OrderDetailByPOCtrlNo.MobileOrderlineDetailList.Count; i++)
        //    {
        //        mylist.Add(new Orderlinelist { ItemDescription = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].ItemDescription, Value = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].Quantity.Value.ToString(), ItemCode = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].ItemCode, PrintshopName = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].PrintshopName, OrderLineId = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].OrderLineId.ToString() });
        //    }
        //    //list.ItemsSource = mylist;

        //}
        async void Track_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new View.TrackList());
        }
        //async void OrderLine_Clicked(object sender, ItemTappedEventArgs e)
        //{
        //    var product = e.Item as Orderlinelist;
        //    string Orderlineid = product.OrderLineId;
        //    OrderLineRequest request = new OrderLineRequest();
        //    request.OrderLineId = Convert.ToInt64(Orderlineid);
        //    request.OrganizationDetail = Org_Detail;
        //    OrderLineResponse Orderline = await org.GetOrderLine(request);
        //    await Navigation.PushAsync(new View.OrderLine());
        //}

        //private void ListViewItem_Tabbed(object sender, ItemTappedEventArgs e)
        //{

        //    var orderdata = e.Item as OrderDetaildata;
        //    string id = orderdata.Id.ToString();
        //    if (id == "1")
        //    {
        //        orderdata.Name = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Name;
        //        orderdata.Contact = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Contact;
        //        orderdata.Street1 = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Street1 + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.City;
        //        orderdata.City = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.ERPRefCode + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.StateProv + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.ZipCode + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Iso3166;
        //    }
        //    else if (id == "2")
        //    {
        //        orderdata.Name = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Name;
        //        orderdata.Contact = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Contact;
        //        orderdata.Street1 = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Street1 + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.City;
        //        orderdata.City = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.ERPRefCode + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.StateProv + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.ZipCode + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Iso3166;

        //    }

        //    var vm = BindingContext as OrderDetailViewModel;
        //    vm?.ShoworHiddenProducts(orderdata);
        //}







    }
    }

