﻿using CheckNet.Models;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TrackList : ContentPage
	{
        MainPageViewModel MpV = new MainPageViewModel();
        OrdernumberPocResponse OrderDetailByPOCtrlNo = (OrdernumberPocResponse)Application.Current.Properties["OrderDetailByPOCtrlNo"];
        List<TrackListRequest> res = new List<TrackListRequest>();
        public TrackList ()
		{
			InitializeComponent ();
            trackList();
           var trackingList= MpV.trackingList;
        }

        public  void trackList()
        {
            
            for (int i = 0; i < OrderDetailByPOCtrlNo.MobileOrderDetail.OrderTrackingNumberList.Count; i++)
            {
                
               
                    res.Add(new TrackListRequest { CourierName = OrderDetailByPOCtrlNo.MobileOrderDetail.OrderTrackingNumberList[i].CourierName+"-" + OrderDetailByPOCtrlNo.MobileOrderDetail.OrderTrackingNumberList[i].AwbNumber});
               
            }
            list.ItemsSource = res;
        }


    }

}