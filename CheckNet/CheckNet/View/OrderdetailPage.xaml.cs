﻿using CheckNet.Models;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderdetailPage : ContentPage
    {
        MainPageViewModel MpV = new MainPageViewModel();
        organization_info Org_Detail = (organization_info)Application.Current.Properties["organizationdetails"];
        OrganizationWebService org = new OrganizationWebService();
        OrdernumberPocResponse OrderDetailByPOCtrlNo = (OrdernumberPocResponse)Application.Current.Properties["OrderDetailByPOCtrlNo"];
        List<Orderlinelist> mylist = new List<Orderlinelist>();
        public OrderdetailPage()
        {
            InitializeComponent();
            if (OrderDetailByPOCtrlNo.MobileOrderDetail.OrderTrackingNumberList[0].ProductionOrderId==0)
            {
                ToolbarItems.RemoveAt(0);
                
            }
            OrderdetailResponse();
            var Orderdetail = MpV.Orderdetail;
            var Orderdetail_Status = MpV.Orderdetail_Status;
            var OrderList_Orderdate = MpV.OrderList_Orderdate;
            var OrderDetail_ShoppingCart = MpV.OrderDetail_ShoppingCart;
            var OrderDetail_OrderNumer = MpV.OrderDetail_OrderNumer;
            var OrderDetail_NotifyMe = MpV.OrderDetail_NotifyMe;
            var OrderDetail_SalesOrder = MpV.OrderDetail_SalesOrder;
            var OrderDetail_VendorRef = MpV.OrderDetail_VendorRef;
            var OrderDetail_Esatimated = MpV.OrderDetail_Esatimated;
            var Orderdetail_Orderconfirmation = MpV.Orderdetail_Orderconfirmation;
            var OrderDetail_ShipDate = MpV.OrderDetail_ShipDate;
            var OrderDetail_Cutmunber = MpV.OrderDetail_Cutmunber;
            var OrderDetail_bill = MpV.OrderDetail_bill;
            var OrderDetail_ship = MpV.OrderDetail_ship;
            var OrderDetail_inquiry = MpV.OrderDetail_inquiry;
            var OrderDetail_send = MpV.OrderDetail_send;
            var OrderDetail_detail = MpV.OrderDetail_detail;
            var OrderDetail_item = MpV.OrderDetail_item;
            var OrderDetail_Quantity = MpV.OrderDetail_Quantity;
            var Orderdetail_released = MpV.Orderdetail_released;
            var Orderdetail_Email = MpV.Orderdetail_Email;
            var Orderdetail_Phone = MpV.Orderdetail_Phone;


        }
        public void OrderdetailResponse()
        {
           

            Address.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.OrganizationName;
            OrderDate.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.OrderDate.ToString();
            ShoppingCartNo.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.ShoppingCartId;
            OrderNumber.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.OrderNumber;
            salesOrderNumber.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.SalesOrderNumber;
            vendorrefNumber.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.VendorRef;
            EstimatedShipDate.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.EstimatedShipDate.ToString();
            shipdate.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.DueDate.ToString();
            Cutnumber.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.CutNumber;
            Name.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Name;
            Contact.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Contact;
            Street1.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Street1;
            City.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.City + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.StateProv + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.ZipCode + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Iso3166;

            Nameship.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Name;
            Contactship.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Contact;
            Street1ship.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Street1;
            Cityship.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.City + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.StateProv + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.ZipCode + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Iso3166;

            //Released.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.VendorRef;
            //Email.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Email;
            //phone.Text = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Phone;


            for (int i = 0; i < OrderDetailByPOCtrlNo.MobileOrderlineDetailList.Count; i++)
            {
                mylist.Add(new Orderlinelist { ItemDescription = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].ItemDescription, Value = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].Quantity.Value.ToString(), ItemCode = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].ItemCode, PrintshopName = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].PrintshopName, OrderLineId = OrderDetailByPOCtrlNo.MobileOrderlineDetailList[i].OrderLineId.ToString() });
            }
            list.ItemsSource = mylist;

        }
        async void Track_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new View.TrackList());
        }

        async void OrderLine_Clicked(object sender, ItemTappedEventArgs e)
        {
            var product = e.Item as Orderlinelist;
            string Orderlineid = product.OrderLineId;
            OrderLineRequest request = new OrderLineRequest();
            request.OrderLineId = Convert.ToInt64(Orderlineid);
            request.OrganizationDetail = Org_Detail;
            OrderLineResponse Orderline = await org.GetOrderLine(request);
            await Navigation.PushAsync(new View.OrderLine());
        }
        
        async void orderconfirmation_Clicked(object sender, System.EventArgs e)
        {
            OrderconfirmationRequest requet = new OrderconfirmationRequest();
            requet.ShoppingCartId = OrderDetailByPOCtrlNo.MobileOrderDetail.ShoppingCartId;
            requet.OrgContext = Org_Detail.OrgContext;
            
            string Result = await org.GetConfirmationHtml(requet);
             await Navigation.PushAsync(new View.OrderCnfrmPage(Result));
            //OrderCnfrmPage oc = new OrderCnfrmPage(Result);
        }

        //private void ListViewItem_Tabbed(object sender, ItemTappedEventArgs e)
        //{

        //    var orderdata = e.Item as OrderDetaildata;
        //    string id = orderdata.Id.ToString();
        //    if (id == "1")
        //    {
        //        orderdata.Name = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Name;
        //        orderdata.Contact = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Contact;
        //        orderdata.Street1 = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Street1 + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.City;
        //        orderdata.City = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.ERPRefCode + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.StateProv + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.ZipCode + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Iso3166;
        //    }
        //    else if (id == "2")
        //    {
        //        orderdata.Name = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Name;
        //        orderdata.Contact = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Contact;
        //        orderdata.Street1 = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Street1 + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.City;
        //        orderdata.City = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.ERPRefCode + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.StateProv + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.ZipCode + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Iso3166;

        //    }

        //    var vm = BindingContext as OrderDetailViewModel;
        //    vm?.ShoworHiddenProducts(orderdata);
        //}
        //private void ListViewItem_Tabbed(object sender, ItemTappedEventArgs e)
        //{

        //    var orderlinedata = e.Item as OrderDetaildata;

        //    string id = orderlinedata.Id;

        //    if(id=="1")
        //    {
        //        orderlinedata.Name = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Name;
        //        orderlinedata.Contact =;
        //        orderlinedata.Street1 = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Street1;
        //        orderlinedata.City = OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.City + OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.StateProv+ OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.ZipCode+ OrderDetailByPOCtrlNo.MobileOrderDetail.BillingAddress.Iso3166;
        //    }
        //    else if(id=="2")
        //    {
        //        orderlinedata.Name = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Name;
        //        orderlinedata.Contact = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Contact;
        //        orderlinedata.Street1 = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Street1;
        //        orderlinedata.City = OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.City + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.StateProv + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.ZipCode + OrderDetailByPOCtrlNo.MobileOrderDetail.DeliveryAddress.Iso3166;

        //    }


        //    var vm = BindingContext as OrderDetailViewModel;
        //    vm?.ShoworHiddenProducts(orderlinedata);


        //}
    }
}