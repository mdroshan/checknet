﻿using CheckNet.Models;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPass : ContentPage
    {
        public ForgotPass()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel
            {
                CurrentPage = this
            };


        }
       
        async private void LoginButton_Clicked(object sender, EventArgs e)
        {

            
             if (Email.Text == string.Empty)
            {
                var answer = await DisplayAlert("Alert", "Email is Required", "Ok", "Cancel");
                Debug.WriteLine("Answer: " + answer);
                
            }
            else
            {
                var sv = new CheckNetWebService();
                Auth_Prop en = new Auth_Prop();
                 
                 en.email = Email.Text;
                
               
                en = await sv.ResetPassword(en);
                if (string.IsNullOrEmpty(en.error))
                {
                    await Navigation.PushAsync(new View.Login());
                }
                else
                {
                    await DisplayAlert(en.error, en.error_description, "OK");
                    Debug.WriteLine(en.error);
                }
            }
        }
    }
}
