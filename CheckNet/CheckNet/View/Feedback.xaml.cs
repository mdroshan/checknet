﻿using CheckNet.Service;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Feedback : ContentPage
    {
        MainPageViewModel MpV = new MainPageViewModel();
        public Feedback()
        {
            InitializeComponent();
            var Feedback_Entry=MpV.Feedback_Entry;
            var Feedback_button = MpV.Feedback_button;
        }
        async private void SendFeedback(object sender, EventArgs e)
        {
            try
            {
                if (Message.Text == string.Empty)
                {
                    var answer = await DisplayAlert(MpV.Validation_Alert, MpV.Validation_User, MpV.Validation_Ok, MpV.Validation_Cancel);
                    Debug.WriteLine("Answer: " + answer);
                    //await DisplayAlert("Alert", "User Name Required", "OK");
                }
                else
                {
                    var service = new Feedbackservices();
                    Feedback data = new Feedback();
                    //data.Message = Message.Text;
                    string res = await service.SendFeedback();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}