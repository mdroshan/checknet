﻿using CheckNet.Models;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    enum EnumOrderType
    {
        Approval = 1,
        StagedOrders = 2,
        RecenetOrders = 3
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Listpage : ContentPage
    {
        MainPageViewModel MpV = new MainPageViewModel();
        organization_info Org_Detail = (organization_info)Application.Current.Properties["organizationdetails"];
        OrdersCountResponse ordercounts = (OrdersCountResponse)Application.Current.Properties["Order counts"];
        List<OrderSummaryResponse> mylist = new List<OrderSummaryResponse>();
        OrganizationWebService org = new OrganizationWebService();
        ObservableCollection<OrderSummaryResponse> response = (ObservableCollection<OrderSummaryResponse>)Application.Current.Properties["Order Summary"];
        OrganizationWebService api = new OrganizationWebService();
        //string OrderNumber;
        //string SalesOrderNumber;
        //string ShoppingCartId;
        //string VendorRef;
        public Listpage()
        {
            InitializeComponent();
        }
        ObservableCollection<OrderSummaryResponse> getresponse;
        Int32 currentOrdertype;
        string Currentsearch;
       
        Int32 CurrentPage = 1;
        public Listpage(Int32 OrderType, string SearchType,Int32 pagenumber)
        {
            CurrentPage = pagenumber;
            Title = MpV.Ordersummary;
            InitializeComponent();
            currentOrdertype = OrderType;
            Currentsearch = SearchType;
            var OrderSummary_Next = MpV.OrderSummary_Next;
            LoadListBasedOnCatg(OrderType, SearchType);
        }

       

        private async Task<List<OrderSummaryResponse>> LoadListBasedOnCatg(Int32 OrderType, string SearchType)
        {
            if (SearchType ==Convert.ToString((int)EnumFilter.OrderNumber))
            {
                if (OrderType == (Int32)EnumOrderType.RecenetOrders)
                {
                    for (int i = 0; i < response.Count; i++)
                    {
                        DateTime? date= response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;

                        mylist.Add(new OrderSummaryResponse { Display1 = response[i].OrderNumber, Display2 = Convert.ToString(response[i].StatusDisplay), Display3 = date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                    }
                    list.ItemsSource = mylist;
                    Label1.Text = MpV.OrderSummary_OrderNumber;
                    Label2.Text = MpV.Ordersummary_Status;
                    Label3.Text = MpV.OrderSummary_Update;
                }
                if (OrderType == (Int32)EnumOrderType.StagedOrders)
                {
                    
                    if (ordercounts.MobileOrderCountList[0].NewOrderCount != 0)
                    {
                        for (int i = 0; i < response.Count; i++)
                        {
                            var date= response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;
                            mylist.Add(new OrderSummaryResponse { Display1 = response[i].OrderNumber, Display2 = response[i].OrganizationName.ToString(), Display3 = date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                        }
                        list.ItemsSource = mylist;
                        Label1.Text = MpV.OrderSummary_OrderNumber;
                        Label2.Text = MpV.OrderSummary_Organizaiton;
                        Label3.Text = MpV.OrderSummary_ReceiveDate;
                        
                    }
                    else
                    {
                        await Navigation.PushAsync(new View.StagedOrder());
                    }
                }
                if (OrderType == (Int32)EnumOrderType.Approval)
                {
                    for (int i = 0; i < response.Count; i++)
                    {
                        var date = response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;
                        mylist.Add(new OrderSummaryResponse { Display1 = response[i].OrderNumber, Display2 = response[i].SalesOrderNumber.ToString(), Display3 = date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                    }
                    list.ItemsSource = mylist;
                    Label1.Text = MpV.OrderSummary_OrderNumber;
                    Label2.Text = MpV.OrderSummary_SalesOrder;
                    Label3.Text = MpV.OrderSummary_Orderdate;
                }
            }
            else if (SearchType == Convert.ToString((int)EnumFilter.ShoppingCartId))
            {
                if (OrderType == (Int32)EnumOrderType.RecenetOrders)
                {
                    for (int i = 0; i < response.Count; i++)
                    {
                        var date = response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;

                        mylist.Add(new OrderSummaryResponse { Display1 = response[i].ShoppingCartId, Display2 = response[i].OrderNumber, Display3 = date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                    }
                    list.ItemsSource = mylist;
                    Label1.Text = MpV.Ordersummary_ShoppingCart;
                    Label2.Text = MpV.OrderSummary_OrderNumber;
                    Label3.Text = MpV.OrderSummary_Update;
                }
                if (OrderType == (Int32)EnumOrderType.StagedOrders)
                {
                    if (ordercounts.MobileOrderCountList[0].NewOrderCount != 0)
                    {
                        for (int i = 0; i < response.Count; i++)
                        {
                           var date = response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;
                            DateTime dt = DateTime.ParseExact(date.ToString(), "yyyy/MM/DD", CultureInfo.InvariantCulture);
                            mylist.Add(new OrderSummaryResponse { Display1 = response[i].OrderNumber, Display2 = response[i].OrganizationName.ToString(), Display3 = dt.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                        }
                        list.ItemsSource = mylist;
                        Label1.Text = MpV.OrderSummary_OrderNumber;
                        Label2.Text = MpV.OrderSummary_Organizaiton;
                        Label3.Text = MpV.OrderSummary_ReceiveDate;
                    }
                    else
                    {
                        await Navigation.PushAsync(new View.StagedOrder());
                    }

                }
                if (OrderType == (Int32)EnumOrderType.Approval)
                {
                    for (int i = 0; i < response.Count; i++)
                    {
                        var date = response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;
                        mylist.Add(new OrderSummaryResponse { Display1 = response[i].OrderNumber, Display2 = response[i].SalesOrderNumber.ToString(), Display3 = date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                    }
                    list.ItemsSource = mylist;
                    Label1.Text = MpV.OrderSummary_OrderNumber;
                    Label2.Text = MpV.OrderSummary_SalesOrder;
                    Label3.Text = MpV.OrderSummary_Orderdate;
                }
            }
            else if (SearchType == Convert.ToString((int) EnumFilter.SalesOrderNumber))
            {
                if (OrderType == (Int32)EnumOrderType.RecenetOrders)
                {
                    for (int i = 0; i < response.Count; i++)
                    {
                        var date = response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;
                        mylist.Add(new OrderSummaryResponse { Display1 = response[i].SalesOrderNumber, Display2 = response[i].OrderNumber, Display3 = date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                    }
                    list.ItemsSource = mylist;
                    Label1.Text = MpV.OrderSummary_SalesOrder;
                    Label2.Text = MpV.OrderSummary_OrderNumber;
                    Label3.Text = MpV.OrderSummary_Update;
                }
                if (OrderType == (Int32)EnumOrderType.StagedOrders)
                {
                    if (ordercounts.MobileOrderCountList[0].NewOrderCount != 0)
                    {
                        for (int i = 0; i < response.Count; i++)
                        {
                            var date = response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;
                            mylist.Add(new OrderSummaryResponse { Display1 = response[i].OrderNumber, Display2 = response[i].OrderNumber.ToString(), Display3 =date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                        }
                        list.ItemsSource = mylist;
                        Label1.Text = MpV.OrderSummary_SalesOrder;
                        Label2.Text = MpV.OrderSummary_OrderNumber;
                        Label3.Text = MpV.OrderSummary_ReceiveDate;
                    }
                    else
                    {
                        await Navigation.PushAsync(new View.StagedOrder());
                    }

                }
                if (OrderType == (Int32)EnumOrderType.Approval)
                {
                    for (int i = 0; i < response.Count; i++)
                    {
                        mylist.Add(new OrderSummaryResponse { Display1 = response[i].OrderNumber, Display2 = response[i].SalesOrderNumber.ToString(), Display3 = response[i].OrderDate.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                    }
                    list.ItemsSource = mylist;
                    Label1.Text = MpV.OrderSummary_OrderNumber;
                    Label2.Text =MpV.OrderSummary_SalesOrder;
                    Label3.Text = MpV.OrderSummary_Orderdate;
                }
            }
            else if (SearchType == Convert.ToString((int) EnumFilter.VendorRef))
            {
                if (OrderType == (Int32)EnumOrderType.RecenetOrders)
                {
                    for (int i = 0; i < response.Count; i++)
                    {
                        var date = response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;
                        mylist.Add(new OrderSummaryResponse { Display1 = response[i].VendorRef, Display2 = response[i].OrderNumber, Display3 = date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                    }
                    list.ItemsSource = mylist;
                    Label1.Text = MpV.OrderSummary_VendorRef;
                    Label2.Text =MpV. OrderSummary_OrderNumber;
                    Label3.Text =MpV.OrderSummary_Update;
                }
                if (OrderType == (Int32)EnumOrderType.StagedOrders)
                {
                    if (ordercounts.MobileOrderCountList[0].NewOrderCount != 0)
                    {
                        for (int i = 0; i < response.Count; i++)
                        {
                            var date = response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;
                            mylist.Add(new OrderSummaryResponse { Display1 = response[i].VendorRef, Display2 = response[i].OrderNumber.ToString(), Display3 = date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                        }
                        list.ItemsSource = mylist;
                        Label1.Text = MpV.OrderSummary_VendorRef;
                        Label2.Text = MpV.OrderSummary_OrderNumber;
                        Label3.Text = MpV.OrderSummary_ReceiveDate;
                    }
                    else
                    {
                        await Navigation.PushAsync(new View.StagedOrder());
                    }

                }
                if (OrderType == (Int32)EnumOrderType.Approval)
                {
                    for (int i = 0; i < response.Count; i++)
                    {
                        var date = response[i].OrderDate == null ? DateTime.Now : response[i].OrderDate;
                        mylist.Add(new OrderSummaryResponse { Display1 = response[i].OrderNumber, Display2 = response[i].SalesOrderNumber.ToString(), Display3 = date.ToString(), Display4 = response[i].POCtrlNo.ToString() });
                    }
                    list.ItemsSource = mylist;
                    Label1.Text = MpV.OrderSummary_OrderNumber;
                    Label2.Text =MpV.OrderSummary_SalesOrder;
                    Label3.Text = MpV.OrderSummary_Orderdate;
                }
            }


            return mylist;

        }

        async void Next_Clicked(object sender, System.EventArgs e)
        {
            
            CurrentPage = CurrentPage + 1;

            OrderSummaryRequest orderlist = new OrderSummaryRequest();

            EnumFilter val = new EnumFilter();
            string stringValue = val.ToString();

            orderlist.PageSize = 5;
            orderlist.PageNumber = CurrentPage;
            orderlist.OrderNumber = "";
            orderlist.SalesOrderNumber = "";
            orderlist.ShoppingCartId = "";
            orderlist.VendorRef = "";
            orderlist.UserId = Org_Detail.UserId;
            orderlist.RetailerId = Org_Detail.OrgContext.RetailerId;
            orderlist.OrderType = Convert.ToInt32(currentOrdertype);
            string[] b = new string[0];
            orderlist.SearchList = b;
            orderlist.SearchType = stringValue;
            response = await org.GetOrderSummary(orderlist);
            Application.Current.Properties["Order Summary"] = response;
            var data = response;
            if (Convert.ToInt16( data.Count) != 0) { 
            //list.ItemsSource = LoadListBasedOnCatg(Convert.ToInt32(orderlist.OrderType), Currentsearch.ToString()).Result;
            await Navigation.PushAsync(new View.Listpage(Convert.ToInt32(orderlist.OrderType), Currentsearch, CurrentPage));
            }
        }
        
        async void OrderList_Details(object sender, ItemTappedEventArgs e)
        {

            var product = e.Item as OrderSummaryResponse;
            string POCID = product.Display4;
            string id = product.Display1;
            getresponse = (ObservableCollection<OrderSummaryResponse>)Application.Current.Properties["Order Summary"];
            OrderListRequest order = new OrderListRequest();
            EnumFilter val = new EnumFilter();
            string stringValue = val.ToString();


            //GetOrderDetailByPOCtrlNo Api call
            //Ordernumber
           
            if (Currentsearch== "0")
            {
                OrdernumberPocRequest request = new OrdernumberPocRequest
                {
                    ProductionOrderId = Convert.ToInt64(POCID),
                    OrganizationDetail = Org_Detail
                };
                OrdernumberPocResponse responsepoc = await org.GetOrderDetailByPOCtrlNo(request);
                Application.Current.Properties["OrderDetailByPOCtrlNo"] = responsepoc;
                await Navigation.PushAsync(new View.OrderdetailPage());
            }
//GetOrderList Api Call
//SalesOrderNumber
            else if (Currentsearch== "1")
            {
                OrderListRequest orderlist = new OrderListRequest
                {
                    RetailerId = Org_Detail.OrgContext.RetailerId,
                    OrderNumber = (Currentsearch == "OrderNumber") ? Currentsearch : "",
                    SalesOrderNumber = (Currentsearch == "SalesOrderNumber") ? Currentsearch : "SalesOrderNumber",
                    ShoppingCartId = (Currentsearch == "ShoppingCartId") ? Currentsearch : "",
                    VendorRef = (Currentsearch == "VendorRef") ? Currentsearch : "",
                    UserId = Org_Detail.UserId
                };
                var SalesOrderNumber = id;
                orderlist.SearchList = new[] { SalesOrderNumber };
                ObservableCollection<OrderListResponse> orderresponse = await org.GetOrderList(orderlist);
                await Navigation.PushAsync(new View.OrderList(Currentsearch));
            }
 //Shopping Cart Id
            else if (Currentsearch== "2")
            {
                OrderListRequest orderlist = new OrderListRequest
                {
                    RetailerId = Org_Detail.OrgContext.RetailerId,
                    OrderNumber = (Currentsearch == "OrderNumber") ? Currentsearch : "",
                    SalesOrderNumber = (Currentsearch == "SalesOrderNumber") ? Currentsearch : "",
                    ShoppingCartId = (Currentsearch == "ShoppingCartId") ? Currentsearch : "ShoppingCartId",
                    VendorRef = (Currentsearch == "VendorRef") ? Currentsearch : "",
                    UserId = Org_Detail.UserId
                };
                var ShoppingCartId = id;
                orderlist.SearchList = new[] { ShoppingCartId };
                ObservableCollection<OrderListResponse> orderresponse = await org.GetOrderList(orderlist);
                await Navigation.PushAsync(new View.OrderList(Currentsearch));
            }
 //Vendor Ref
            else if (Currentsearch== "3")
            {
                OrderListRequest orderlist = new OrderListRequest
                {
                    RetailerId = Org_Detail.OrgContext.RetailerId,
                    OrderNumber = (Currentsearch == "OrderNumber") ? Currentsearch : "",
                    SalesOrderNumber = (Currentsearch == "SalesOrderNumber") ? Currentsearch : "",
                    ShoppingCartId = (Currentsearch == "ShoppingCartId") ? Currentsearch : "",
                    VendorRef = (Currentsearch == "VendorRef") ? Currentsearch : "VendorRef",
                    UserId = Org_Detail.UserId
                };
                var VendorRef = id;
                orderlist.SearchList = new[] { VendorRef };
                ObservableCollection<OrderListResponse> orderresponse = await org.GetOrderList(orderlist);
                await Navigation.PushAsync(new View.OrderList(Currentsearch));
            }
            //Approval Order
            else if (Currentsearch == "Approval")
            {
                OrderListRequest orderlist = new OrderListRequest
                {
                    RetailerId = Org_Detail.OrgContext.RetailerId,
                    OrderNumber = (Currentsearch == "OrderNumber") ? Currentsearch : "",
                    SalesOrderNumber = (Currentsearch == "SalesOrderNumber") ? Currentsearch : "",
                    ShoppingCartId = (Currentsearch == "ShoppingCartId") ? Currentsearch : "",
                    VendorRef = (Currentsearch == "VendorRef") ? Currentsearch : "",
                    UserId = Org_Detail.UserId
                };
                var Approval= id;
                orderlist.SearchList = new[] { Approval };
                ObservableCollection<OrderListResponse> orderresponse = await org.GetOrderList(orderlist);
                await Navigation.PushAsync(new View.Approval(Currentsearch));
            }


        }



    }

}

