﻿using Badge.Plugin;
using CheckNet.Models;
using CheckNet.Service;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Notification_Inbox : ContentPage
    {
        MainPageViewModel MpV = new MainPageViewModel();
        organization_info Org_Detail = (organization_info)Application.Current.Properties["organizationdetails"];
        NotificationWebService api = new NotificationWebService();
        List<GetMessagesRequest> Myres = new List<GetMessagesRequest>();
        NotificationWebService service = new NotificationWebService();
        List<GetMessagesResponse> response = null;
        int userid;
        string status;
        public Notification_Inbox()
        {
            InitializeComponent();
          
            Notification_inbox();

         }
      
       
        public async void Notification_inbox()
        {
            response = await api.GetMessagesForInboxByUserId();
            for (int i = 0; i < response.Count; i++)
            {
                if (response[i].SubscriptionType == 106) {
                    Myres.Add(new GetMessagesRequest { string_DateProcessed = response[i].DateProcessed.ToString("dd MMMM yyyy hh:mm tt"), Message = response[i].Message,SubscriptionType= MpV.NotificationList_headingtwo });
                }
                else
                {
                    Myres.Add(new GetMessagesRequest { string_DateProcessed = response[i].DateProcessed.ToString("dd MMMM yyyy hh:mm tt"), Message = response[i].Message, SubscriptionType = MpV.NotificationList_headingone });
                }
 }
            list.ItemsSource = Myres;
        }

        public async Task<string> MarkAll(object sender, EventArgs e)
        {

            var status = 104;
            var data = new NotificationsettingList
            {
                UserId = Org_Detail.UserId,
                Status = status,
            };
             await api.UpdateMessageStatusWithMarkAll(data);
             CrossBadge.Current.SetBadge(0);
            
            return null;
        }

        public async Task<string> DeleteAll(object sender, EventArgs e)
        {

            var status = 105;
            var data = new NotificationsettingList
            {
                UserId = Org_Detail.UserId,
                Status = status,
            };
            await api.UpdateMessageStatusWithMarkAll(data);
            CrossBadge.Current.SetBadge(0);

            return null;
        }


    }
}