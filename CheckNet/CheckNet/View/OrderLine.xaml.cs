﻿using CheckNet.MenuItem;
using CheckNet.Models;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OrderLine : ContentPage
	{
        OrdernumberPocResponse OrderDetailByPOCtrlNo = (OrdernumberPocResponse)Application.Current.Properties["OrderDetailByPOCtrlNo"];
        OrderLineResponse orderline = (OrderLineResponse)Application.Current.Properties["OrderLine"];
        MainPageViewModel MpV = new MainPageViewModel();
        public OrderLine ()
		{
			InitializeComponent ();
            if(orderline.OrderLine.Count==0)
            {
                Itemcode.Text = "Item code: ";
                Totalquantity.Text = "Total Quantity: " + OrderDetailByPOCtrlNo.MobileOrderlineDetailList[0].Quantity.Value;
            }
            
            else
            {
                __productname.Text = orderline.OrderLine[0].__productname;
                Itemcode.Text = "Item code: " + orderline.OrderLine[0].__productarticlenumber;
                Totalquantity.Text = "Total Quantity: " + OrderDetailByPOCtrlNo.MobileOrderlineDetailList[0].Quantity.Value;
            }
            var OrderLine = MpV.OrderLine;
        }

        private void ListViewItem_Tabbed(object sender, ItemTappedEventArgs e)
        {

            var orderlinedata = e.Item as OrderLineList;
           
            var Aone = MpV.FAQ_AnswrOne;
            var logo = e.Item;

            for (int i = 0; i < orderline.OrderLine.Count; i++)
            {
                
                if (orderlinedata.Position.Replace("Order Number:","") == orderline.OrderLine[i].__position.ToString())
                {
                    orderlinedata.OriginalQuantity = orderline.OrderLine[i].__originalquantity;
                    orderlinedata.ArcitemCode = orderline.OrderLine[i]._arcitemcode;
                    orderlinedata.SizeCode = orderline.OrderLine[i]._sizecode;
                    orderlinedata.Size1 = orderline.OrderLine[i]._size1;
                    orderlinedata.Size2 = orderline.OrderLine[i]._size2;
                    orderlinedata.Size3 = orderline.OrderLine[i]._size3;
                    orderlinedata.Currency1 = orderline.OrderLine[i]._scurr1;
                    orderlinedata.Price1 = orderline.OrderLine[i]._sprice1;
                    orderlinedata.Currency2 = orderline.OrderLine[i]._scurr2;
                    orderlinedata.Price2 = orderline.OrderLine[i]._sprice2;
                    orderlinedata.Currency3 = orderline.OrderLine[i]._scurrtag2;
                    orderlinedata.Price3 = orderline.OrderLine[i]._spricetag2;
                    orderlinedata.Color = orderline.OrderLine[i]._colour;
                    orderlinedata.EANCode = orderline.OrderLine[i]._eancode;
                    orderlinedata.CutNumber = orderline.OrderLine[i]._linenumber;
                    orderlinedata.TgFlexCat1 = orderline.OrderLine[i]._tgflexcat1;
                    orderlinedata.Fam = orderline.OrderLine[i]._fam;
                    orderlinedata.Category = orderline.OrderLine[i]._category;
                    orderlinedata.CategoryName = orderline.OrderLine[i]._categoryname;
                    orderlinedata.ShoeName = orderline.OrderLine[i]._tagflexcat3desc;
                    orderlinedata.Description = orderline.OrderLine[i]._tagflexcat2desc;
                    orderlinedata.Icon = "minus.png";
                }
            }
           
            var vm = BindingContext as OrderLineViewModel;
            vm?.ShoworHiddenProducts(orderlinedata);
        }
    }
}