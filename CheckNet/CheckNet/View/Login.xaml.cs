﻿using CheckNet.MenuItem;
using CheckNet.Models;
using CheckNet.Service;
using CheckNet.ViewModels;
using ProgressRingControl.Forms.Plugin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace CheckNet.View
{
   
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
       
        MainPageViewModel MpV = new MainPageViewModel();

        //private NavigationPage _mainlayout;
        public Login()
        {
            InitializeComponent();

            BindingContext = new MainPageViewModel
            {
                CurrentPage = this
            };
            var boxview = new Entry();
            boxview.SetBinding(BoxView.ColorProperty, new Binding(nameof(BackgroundColorProperty), source: this));
            
            Myindicator.IsRunning = false;

            Label lbl = new Label();
            lbl.TranslationX = 10;
            lbl.TranslationY = 10;
        }






        private void ToolbarClicked(object sender, EventArgs e)
        {
            //DisplayAlert("Alert", "You have been alerted", "OK");
        }

        async private void LoginButton_Clicked(object sender, EventArgs e)
        {
            
            try
            {
                Myindicator.IsRunning = true;

                if (Username.Text == string.Empty)
                {
                    var answer = await DisplayAlert(MpV.Validation_Alert, MpV.Validation_User, MpV.Validation_Ok, MpV.Validation_Cancel);
                    Debug.WriteLine("Answer: " + answer);
                    //await DisplayAlert("Alert", "User Name Required", "OK");
                }
                else if (Password.Text == string.Empty)
                {
                    var answer = await DisplayAlert(MpV.Validation_Alert, MpV.Validation_Pass, MpV.Validation_Ok,MpV.Validation_Cancel);
                    Debug.WriteLine("Answer: " + answer);
                }
                else
                {
                    var sv = new CheckNetWebService();
                    Auth_Prop en = new Auth_Prop();
                    en.UserName = Username.Text;
                    en.password = Password.Text;
                    en.UseRefreshToken = false;
                    en = await sv.Loginme(en);
                    
                    
                    if (string.IsNullOrEmpty(en.error))
                    {

                        //Organization Api call:
                        OrganizationWebService org = new OrganizationWebService();
                        organization_info obj =  await org.GetOrganizationdata();
                        //Order Counts Api call:
                        OrdersCountResponse count = new OrdersCountResponse();
                        OrdersCountResponse order = await org.GetOrderCounts(count);
                        //Alerts and News Api call:
                        NotificationWebService api = new NotificationWebService();
                        var response =await api.GetMessageListTaskAsync();
                        Application.Current.Properties["alerts and news"] = response;
                        //Order List Api call:
                        organization_info Org_Detail = (organization_info)Application.Current.Properties["organizationdetails"];
                       
                        //Redirection to Main page
                        Application.Current.MainPage = new CheckNet.MainPage();

                        //MasterDetailPage mdp = Application.Current.MainPage as MasterDetailPage;

                        //mdp.Detail = new NavigationPage(new View.Home())
                        //{

                        //    BarBackgroundColor = Color.FromHex("#d52b1e"),
                        //    BarTextColor = Color.FromHex("#ffffff"),
                        //};


                        //IsPresented = false;
                    }
                    else
                    {
                        await DisplayAlert("",en.error_description, MpV.Validation_Ok);
                        Debug.WriteLine(en.error);
                    }
                }
            }
            catch (Exception ex)
            {

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                Myindicator.IsRunning = false;
            }
        }

        async private void PassowordHint_Clicked(object sender, EventArgs e)
        {
            try
            {
                Myindicator.IsRunning = true;
                if (Username.Text == string.Empty)
                {
                    var answer = await DisplayAlert(MpV.Validation_Alert, MpV.Validation_User, MpV.Validation_Ok, MpV.Validation_Cancel);
                    Debug.WriteLine("Answer: " + answer);
                    //await DisplayAlert("Alert", "User Name Required", "OK");
                }
                else
                {
                    var sv = new CheckNetWebService();
                    Auth_Prop en = new Auth_Prop();
                    en.UserName = Username.Text;
                    string res = await sv.PasswordHint(en);
                    var answer = await DisplayAlert(MpV.Validation_Alert, MpV.Validation_PasswordHint, MpV.Validation_Ok, MpV.Validation_Cancel);
                }
            }
            catch (Exception ex)
            {

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                Myindicator.IsRunning = false;
            }
        }
        async void Handle_Clicked(object sender, System.EventArgs e)
        {

            await Navigation.PushAsync(new View.ForgotPass());
            BackgroundColor = Color.White;

        }
        //async void MenuToolbarClicked(object sender, EventArgs e)
        //{
        //    Menuselected();
        //    await Navigation.PushAsync(new MainPage());
        //}


        //    public  void Menuselected() { 
        //    List<MenuList> mylist = new List<MenuList>() {

        //    new MenuItem.MenuList() {

        //        Text =MpV.Menu_ContactUs ,MenuId="1",Icon="Contactus.png",ArrowIcon="Arrow1.png",
        //    },
        //    new MenuItem.MenuList() {
        //        Text =MpV.Menu_FAQ,MenuId="2",Icon="Faq.png",ArrowIcon="Arrow1.png",

        //    },
        //    new MenuItem.MenuList() {
        //        Text =MpV.Menu_TermsConditions,MenuId="3",Icon="Termscondi.png",ArrowIcon="Arrow1.png",
        //    },
        //    new MenuItem.MenuList() {
        //        Text =MpV.Menu_Policies,Icon="Policies.png",MenuId="4",ArrowIcon="Arrow1.png",
        //    },
        //    new MenuItem.MenuList() {
        //        Text =MpV.Menu_Help,Icon="Help.png",MenuId="5",ArrowIcon="Arrow1.png",
        //    },


        //};
        //}

    }
}


