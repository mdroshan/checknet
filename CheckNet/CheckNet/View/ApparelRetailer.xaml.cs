﻿using CheckNet.MenuItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ApparelRetailer : ContentPage
	{public List<MenuList> notificationdata;
		public ApparelRetailer ()
		{
			InitializeComponent ();
            Notification();
            //list.ItemsSource = notificationdata;
        }
        public void Notification()
        {
            notificationdata = new List<MenuList>
            {
                new MenuList()
                {
                    Icon="Home.png",Text="New Orders Received",MenuName="Notify if a new stage order is available"
                },
                new MenuList()
                {
                    Icon="Home.png",Text="Order Approval",MenuName="Notify if a newly placed order is waiting  for approval from you "
                },
                new MenuList()
                {
                    Icon="Home.png",Text="Approved/Declined Order",MenuName="Notify  whether an order you placed was apporved or declined"
                },
                new MenuList()
                {
                    Icon="Home.png",Text="Shipment Confirmation",MenuName="Notify if an order you placed was Shipped"
                },
                 new MenuList()
                {
                    Icon="Home.png",Text="Delivery Confirmation",MenuName="Notify if an order you placed was Delivered"
                },
                  new MenuList()
                {
                    Icon="Home.png",Text="Printshop Holiday",MenuName="Notify for any upcoming printshop closures due to holidays "
                },
                   new MenuList()
                {
                    Icon="Home.png",Text="Maintenance",MenuName="Notify for any upcoming printshop closures due to holidays "
                },
            };
        }

      


    }
}