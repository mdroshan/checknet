﻿using CheckNet.ViewModels;
using I18NPortable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Help : ContentPage
	{
		public Help ()
		{
            BindingContext = new MainPageViewModel
            {
                CurrentPage = this
            };
            InitializeComponent ();

            string Cur_lang = I18N.Current.Locale;
            string uri = string.Format("Help-Mobile-e-{0}.pdf",Cur_lang);
            CustomCntrl.Uri = uri;

        }
	}
}