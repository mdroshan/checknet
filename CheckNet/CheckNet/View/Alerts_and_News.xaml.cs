﻿using CheckNet.Models;
using CheckNet.Service;
using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Alerts_and_News : ContentPage
    {
        GetMessageListTaskAsync_Response alertnews = (GetMessageListTaskAsync_Response)Application.Current.Properties["alerts and news"];
        MainPageViewModel MpV = new MainPageViewModel();
        List<PartnerHolidayList> mylist = new List<PartnerHolidayList>();
        NotificationWebService api = new NotificationWebService();
        GetMessageListTaskAsync_Response alertdata;
        public Alerts_and_News()
        {
            InitializeComponent();
            var AlertsandNew= MpV.AlertsandNew;
            var Alerts_PrintshopHoliday = MpV.Alerts_PrintshopHoliday;
           GetAlert_News();
        }

        
        
        public async void GetAlert_News()
        {


            //CrossBadge.Current.SetBadge(10);

            for (int i = 0; i < alertnews.PartnerHolidayList.Count; i++)
            {
                mylist.Add(new PartnerHolidayList { Name = alertnews.PartnerHolidayList[i].Name, HolidayDescription = "("+alertnews.PartnerHolidayList[i].HolidayDescription+")"+"Closed on" + alertnews.PartnerHolidayList[i].StartDate.ToString("dd MMMM yyyy")+"-"+ alertnews.PartnerHolidayList[i].EndDate.ToString("dd MMMM yyyy") });
            }
            list.ItemsSource = mylist;


        }
    }
}


//var result = alertnews.PartnerHolidayList.GroupBy(x => x.Name).Select(x => x).ToList();

//var firstname = alertnews.PartnerHolidayList[0].Name;
//var description = "";

//            for (int i = 0; i<alertnews.PartnerHolidayList.Count; i++)
//            {
//                if (firstname == alertnews.PartnerHolidayList[i].Name)
//                {
//                     description += "(" + alertnews.PartnerHolidayList[i].HolidayDescription + ")" + "Closed on" + alertnews.PartnerHolidayList[i].StartDate.ToString("dd MMMM yyyy") + "-" + alertnews.PartnerHolidayList[i].EndDate.ToString("dd MMMM yyyy") + ',';
                   
//                  //  mylist.Add(new PartnerHolidayList { Name = alertnews.PartnerHolidayList[i].Name, HolidayDescription = "(" + description + ")" + "Closed on" + alertnews.PartnerHolidayList[i].StartDate.ToString("dd MMMM yyyy") + "-" + alertnews.PartnerHolidayList[i].EndDate.ToString("dd MMMM yyyy") });
//                }
//                else {

//                    mylist.Add(new PartnerHolidayList { Name = firstname, HolidayDescription = "(" + description + ")" + "Closed on" + alertnews.PartnerHolidayList[i].StartDate.ToString("dd MMMM yyyy") + "-" + alertnews.PartnerHolidayList[i].EndDate.ToString("dd MMMM yyyy") });

//                    description = "";
//                    description += "(" + alertnews.PartnerHolidayList[i].HolidayDescription + ")" + "Closed on" + alertnews.PartnerHolidayList[i].StartDate.ToString("dd MMMM yyyy") + "-" + alertnews.PartnerHolidayList[i].EndDate.ToString("dd MMMM yyyy") + ',';
//                    // mylist.Add(new PartnerHolidayList { Name = alertnews.PartnerHolidayList[i].Name, HolidayDescription = "(" + description + ")" + "Closed on" + alertnews.PartnerHolidayList[i].StartDate.ToString("dd MMMM yyyy") + "-" + alertnews.PartnerHolidayList[i].EndDate.ToString("dd MMMM yyyy") });
//                }
//                firstname = alertnews.PartnerHolidayList[i].Name;
//            }
//            list.ItemsSource = mylist;