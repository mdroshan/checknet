﻿using CheckNet.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Email : ContentPage
    {
        public Email()
        {
            InitializeComponent();
            if(IsCurrentlyLogin())
            {
                BeforeLogineMail.IsVisible =false;
                AfterLogineMail.IsVisible = true;
               
            }
            else
            {
                AfterLogineMail.IsVisible = false;
                BeforeLogineMail.IsVisible = true;
               
            }
        }

        public bool IsCurrentlyLogin()
        {
            try
            {
                if (Application.Current.Properties.ContainsKey("authorizationdata"))
                {
                    Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                    if (authprop.LoggedIn)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        async void SendEmail(object sender, System.EventArgs e)
        {
            if(IsCurrentlyLogin())
            {

            if (Name.Text == string.Empty)
            {
                var answer = await DisplayAlert("Alert", "Name Required", "Ok", "Cancel");

            }
            else if (email.Text == string.Empty)
            {
                var answer = await DisplayAlert("Alert", "Email Required", "Ok", "Cancel");

            }
            else if (Organization.Text == string.Empty)
            {
                var answer = await DisplayAlert("Alert", "Organization Required", "Ok", "Cancel");

            }
            else if (OrderNumber.Text == string.Empty)
            {
                var answer = await DisplayAlert("Alert", "Email Required", "Ok", "Cancel");
            }

            else
            {
                var sv = new CheckNetWebService();
                Write_Email en = new Write_Email();
                en.Name = Name.Text;
                en.WEmail = email.Text;
                en.Organization = Organization.Text;
                en.OrderNumber = OrderNumber.Text;
                en.Message = Message.Text;
                if (await sv.WriteEmail(en))
                {
                    var answer = await DisplayAlert("Success", "Email sent successfully", "Ok", "Cancel");
                }
                else
                {
                    var answer = await DisplayAlert("Error", "Email sending failed", "Ok", "Cancel");
                }
                
            }
            }
            else
            {

            }

        }

    }
}