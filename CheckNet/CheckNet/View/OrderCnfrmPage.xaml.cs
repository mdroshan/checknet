﻿using CheckNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderCnfrmPage : ContentPage
    {
        MainPageViewModel MpV = new MainPageViewModel();
        public OrderCnfrmPage()
        {

        }

        public string[] Splitwithstring(string Value, string Splitter)
        {

            return Value.Split(new string[] { Splitter }, StringSplitOptions.None);

        }

       
        public OrderCnfrmPage(string URL)
        {
            
            InitializeComponent();
            var OrderList_Orderconfirmation = MpV.OrderList_Orderconfirmation;
            if (URL != null)
            {
                string[] Firstset = Splitwithstring(URL,"<style");
                string[] Secondset = Splitwithstring(Firstset[1], "</style>");
                string myresult = Firstset[0] + Secondset[1];
                string mynewdata = myresult.Replace("\n", "");
                var html = new HtmlWebViewSource
                {
                    Html = myresult
                };
                
                //string removelastString = mynewdata.Remove(mynewdata.LastIndexOf(","), 1);
                //string result = removelastString.Remove(removelastString.IndexOf(","), 1);
                Browser.Source = html;

                //"<!DOCTYPE html><html><head><meta charset=\"utf-8\" /><title>Order Confirmation</title></head><body><div class=\"container fontStyle\"><table class=\"mainContainer widthSize\"><tbody><tr><td><img src=\"https://checknet.checkpt.com/images/consemail_CheckNet1.png\"></td><td><div class=\"notes\"><b>Order Confirmation</b></div><div class=\"notes2\">Sat Oct 07 2017 </div></td></tr><tr><td colspan=\"2\"><div class=\"graybar\"></div></td></tr><tr><td colspan=\"2\"></td></tr><tr><td colspan = \"2\">\n    <div id=\"confirmationHeader\">\n        <p class=\"confirmationHeader\">Your order has been submitted. Your shopping cart number is # <b><span id=\"shopCartNo\">1273112</span></b>.</p>\n\n        <div id=\"confirmationPage\">\n            <div id=\"headerData\">\n                <span id=\"orgName\" class=\"confirmationHtml\"><h4>ANKHUA EXPORTS PVT LTD</h4><b>Retailer Name :  </b> Arcadia</span>\n                <p class=\"confirmationHtml\"><b>Released by :</b> <span id=\"releaseBy\">ARCADIA7313&nbsp;&nbsp;<b> Email :</b> ARUN@ANKHUAEXPORTS.COM&nbsp;&nbsp;<b> Phone Number :</b>  0031880082300</span></p>\n            </div>\n            <div id=\"alertData\">\n        \n        \n\n\n    </div>\n\n\n            <div id=\"girdDataHtml\"><div class=\"confirmationHtml div-border\" id=\"gridHeader0\">\n\n    <table class=\"row-fluid confirmationHtml\">\n        <tbody><tr valign=\"top\" class=\"confirmationHtmlWidth\">\n\n              <td class=\"billTo\"> <b>Bill To Information</b>\n                <div id=\"billToAddress\"><p>ANKHUA EXPORTS PVT LTD<br>Mr Arun<br>H-45 SECTOR - 63 NOIDA<br>Delhi, Delhi, 201301, IN</p></div>\n              </td>\n\n              <td class=\"shipTo\"> <b>Ship To Information</b>\n                   <div id=\"shipToAddress\"><p>ANKHUA EXPORTS PVT LTD<br>Mr Arun<br>H-45 SECTOR - 63 NOIDA<br>Delhi, Delhi, 201301, IN</p></div>\n              </td> \n               <td>\n                    <b>Shopping Cart : </b> 1273112 <br>\n                    <b>Order Date : </b> 07/10/2017<br>\n                \n                    <b>Vendor Ref : </b><span class=\"vendorRefValue confirmationHtml\"> 7313</span><br>\n                \n              \n                \n              </td>\n              <td>                 \n               \n                               \n                    <span><b>Shipping Type :</b> REGULAR <br></span>\n            \n            \n            \n            \n            <span class=\"confirmationHtml hidden\" style=\"display: inline;\"><b>Payment Type : </b>  <span class=\"paymentType\">Invoice</span> <br></span>\n\n            \n                    <span><b>Courier :</b>  <br></span>\n                    <span><b>Account :</b>  <br></span>\n            \n\n            \n             </td>\n        </tr>\n    </tbody></table>\n<br><div id=\"gridgridHeader0\" data-role=\"grid\" class=\"k-grid k-widget table table-striped row-fluid\"><table role=\"grid\" class=\"confirmationHtmlWidth\"><colgroup><col style=\"width:170px\"><col style=\"width:150px\"><col style=\"width:170px\"><col style=\"width:160px\"><col style=\"width:250px\"><col style=\"width:70px\"></colgroup><thead class=\"k-grid-header\" role=\"rowgroup\"><tr role=\"row\"><th role=\"columnheader\" data-field=\"poNumber\" rowspan=\"1\" data-title=\"Order Number\" data-index=\"0\" id=\"0591984d-f426-4078-8f3e-8492f6417f88\" class=\"k-header\">Order Number</th><th role=\"columnheader\" data-field=\"salesOrderNo\" rowspan=\"1\" data-title=\"Sales Order\" data-index=\"1\" id=\"ef9a3258-614f-44b1-b465-46ab0ce9d502\" class=\"k-header\">Sales Order</th><th role=\"columnheader\" data-field=\"prodctionLocation\" rowspan=\"1\" data-title=\"Production Location\" data-index=\"2\" id=\"2bbfae04-5c37-49c6-8f5b-96bd50b909a3\" class=\"k-header\">Production Location</th><th role=\"columnheader\" data-field=\"itemCode\" rowspan=\"1\" data-title=\"Item Code\" data-index=\"3\" id=\"2c3d8d5f-f30e-41c0-9da9-a20331cfe187\" class=\"k-header\">Item Code</th><th role=\"columnheader\" data-field=\"itemDescription\" rowspan=\"1\" data-title=\"Item Description\" data-index=\"4\" id=\"55950898-8db6-41b2-ab04-efdabc1f59f5\" class=\"k-header\">Item Description</th><th role=\"columnheader\" data-field=\"quantity\" rowspan=\"1\" data-title=\"Quantity\" data-index=\"5\" id=\"83ef4947-06b4-4005-8dfc-03bfd57a2382\" class=\"k-header\">Quantity</th></tr></thead><tbody role=\"rowgroup\"><tr data-uid=\"a49b2186-e917-4cfb-b9db-d8a733100bbc\" role=\"row\"><td role=\"gridcell\"><span class=\"orderNumber\"> G13142302-13T05MBLK <span></span></span></td><td role=\"gridcell\">165101808</td><td role=\"gridcell\">NE Checkpoint India Offset</td><td role=\"gridcell\">NC-TS</td><td role=\"gridcell\">Swing Ticket</td><td role=\"gridcell\">150</td></tr></tbody></table></div><br><div id=\"gridFooter0\"><br></div></div><br></div>\n            <div id=\"confirmationGrandTotal\"><div id=\"mainFooter\" style=\"display: none;\"></div></div>\n\n            <div id=\"helpDesk\">\n                <br><b>Help Desk </b>:Checkpoint Systems UK Ltd <b> Phone: </b> +44 1635 567099 <b> E-Mail:</b> checkdirectuk@checkpt.com\n            </div>\n\n        </div>\n    </div>\n\n</td ></tr><br><tr><td colspan=\"2\"></td></tr><tr><td colspan=\"2\"><div class=\"contents\"><table class=\"contactus\" style=\"BACKGROUND-COLOR: rgb(207,207,207)\"><tbody><tr><td><span style=\"FONT-WEIGHT: 600\"><b>Europe</b></span><br><br>Phone: +31 (0)880082 400<br>Fax: +31 (0)315 341161<br>E-mail: cs@checkpt-als.com</td><td><span style=\"FONT-WEIGHT: 600\"><b>Hong Kong</b></span><br><br>Phone: +852 2995-8272<br>Fax: +852 2527-8408<br></td>\t<td><span style=\"FONT-WEIGHT: 600\"><b>United States</b></span><br><br>Phone: +1 800 - 775 - 1802 <br>Fax: +1 937 - 866 - 1909 <br>E-mail: checknet@checkpt.com </td></tr></tbody></table ></div></td ></tr> <tr><td colspan=\"2\"></td></tr><tr><td colspan=\"2\"><div style=\"HEIGHT: 40px\" class=\"text-center\">If you need further assistance with your order, please sign into Checknet.checkpt.com or call/email one of our offices listed above.<br>This mailbox is not monitored.Please do not reply to this email.<br></div></td ></tr> <tr><td colspan=\"2\"></td></tr><tr><td colspan=\"2\"><div class=\"text-center\"><img src=\"https://checknet.checkpt.com/images/consemail_checklogo.jpg\"></div></td></tr></tbody></table></div></body></html>" //"<html> <H4> By User1 </H4 > </html>"

            }


            //var htmlSource = new HtmlWebViewSource();
            //htmlSource.Html = "@'<!DOCTYPE html PUBLIC ' -//W3C//DTD HTML 4.01//EN'>< html >< head >  < title > My first styled page </ title > < style type = 'text/css' >     body {            color: purple;       background - color: #d8da3d } </ style ></ head >< body >< body>  <h1>Xamarin.Forms</h1>  <p>Welcome to WebView.</p>  </body></html>";
            //Browser.Source = htmlSource;
        }


    }
}