﻿using CheckNet.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CheckNet.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Approval : ContentPage
	{

        organization_info Org_Detail = (organization_info)Application.Current.Properties["organizationdetails"];
        OrganizationWebService org = new OrganizationWebService();
        List<OrderListResponse> mylist = new List<OrderListResponse>();
        ObservableCollection<OrderListResponse> orderlistresponse = (ObservableCollection<OrderListResponse>)Application.Current.Properties["OrderList"];
        public Approval ()
		{
			InitializeComponent ();
		}
        string Currentsearchtype;

        public Approval(string currentsearch)
        {
            InitializeComponent();
            Currentsearchtype = currentsearch;
            orderlistResponses(currentsearch);
        }

        public void orderlistResponses(string currentsearch)
        {
            if (currentsearch == "SalesOrderNumber")
            {

                SalesOrderNumber.Text = "Sales Order :" + orderlistresponse[0].SalesOrderNumber;
                OrganizationName.Text = orderlistresponse[0].OrganizationName;
                OrderedBy.Text = "Order By :" + orderlistresponse[0].OrderedBy;
                OrderDate.Text = "OrderDate :" + orderlistresponse[0].OrderDate.ToString();
            
            }
            else if (currentsearch == "ShoppingCartId")
            {

                SalesOrderNumber.Text = "Shopping Cart :" + orderlistresponse[0].ShoppingCartId;
                OrganizationName.Text = orderlistresponse[0].OrganizationName;
                OrderedBy.Text = "Order By :" + orderlistresponse[0].OrderedBy;
                OrderDate.Text = "OrderDate :" + orderlistresponse[0].OrderDate.ToString();
              
            }
            else if (currentsearch == "VendorRef")
            {

                SalesOrderNumber.Text = "VendorRef :" + orderlistresponse[0].VendorRef;
                OrganizationName.Text = orderlistresponse[0].OrganizationName;
                OrderedBy.Text = "Order By :" + orderlistresponse[0].OrderedBy;
                OrderDate.Text = "OrderDate :" + orderlistresponse[0].OrderDate.ToString();
                
            }
            
        }
    }
}