﻿using Newtonsoft.Json;
using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CheckNet.Models
{
    public class CheckNetWebService : ICheckNetWebService, IDisposable
    {
        HttpClient client = null;
        public CheckNetWebService()
        {
            client = new System.Net.Http.HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }
        public CheckNetWebService(bool IsAuthHeader)
        {
            string accesstoken = "";
            if (IsAuthHeader)
            {
                if (Application.Current.Properties["Access_token"] != null)
                {
                    accesstoken = Application.Current.Properties["Access_token"].ToString();
                }

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header   
            }

            Task<Auth_Prop> Prop = RefreshToken();

        }

        public void Dispose()
        {
            this.Dispose();
        }

        public async Task<Auth_Prop> Loginme(Auth_Prop en)
        {
            try
            {
                Auth_Prop rootobject = null;
                //Add param 
                var formContent = new FormUrlEncodedContent(new[]
                  {
              new KeyValuePair<string, string>("username", en.UserName),
              new KeyValuePair<string, string>("password", en.password),
              new KeyValuePair<string, string>("client_id", Constants.client_id),
              new KeyValuePair<string, string>("grant_type", Constants.grant_type),
             });
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "authorization/oauth/token"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header   
                String url = String.Format("{0}{1}", Constants.RestUrl, "authorization/oauth/token");
                var response = await client.PostAsync(url, formContent);
                var LoginDetailsJson = response.Content.ReadAsStringAsync().Result;
                rootobject = JsonConvert.DeserializeObject<Auth_Prop>(LoginDetailsJson);

               
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    //Storing info into Local system
                    rootobject.LoggedIn = true;

                    if (!en.UseRefreshToken)
                    {
                        en.RefreshToken = rootobject.RefreshToken;
                        en.UseRefreshToken = true;
                    }
                    //else
                    //{
                    //    en.RefreshToken = "";
                    //    en.UseRefreshToken = false;
                    //}

                    Application.Current.Properties["authorizationdata"] = rootobject;


                    //DeviceInfo devicedetails = (DeviceInfo)Application.Current.Properties["deviceinfo"];


                    if (rootobject.LoggedIn == true)
                    {
                        rootobject.isAuth = true;

                    }
                    //MainPage data = new MainPage();
                    //data.Menulanguage1();

                }
                else
                {
                    rootobject.LoggedIn = false;
                }

               
                return rootobject;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
            ;// rootobject.LoginDetails;


        }
        public async Task<string> PasswordHint(Auth_Prop en)
        {
            try
            {
                Auth_Prop rootobject = null;
                //Add param 

                //client.BaseAddress = new Uri(String.Format("{0}{1}{2}", Constants.RestUrl, "webapi/api/core/MobileApp/GetPasswordHint?username=", en.UserName));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header   
                String url = String.Format("{0}{1}{2}", Constants.RestUrl, "webapi/api/core/MobileApp/GetPasswordHint?username=", en.UserName);
                var response = await client.GetAsync(url);
                var LoginDetailsJson = response.Content.ReadAsStringAsync().Result;
                // rootobject = JsonConvert.DeserializeObject<Auth_Prop>(LoginDetailsJson);
                return LoginDetailsJson.ToString();
            }
            catch
            {

            }
            return null;
        }
        public async Task<bool> WriteEmail(Write_Email en)
        {
            try
            {
                //Add parameter                 
                var Json = JsonConvert.SerializeObject(en);
                var content = new StringContent(Json, Encoding.UTF8, "application/json");
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/ContactUsByEmail");
                var response = await client.PostAsync(url, content);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
            return false;
        }

        public bool Logout()
        {
            Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
            if (Application.Current.Properties["authorizationdata"] != null)
            {
                Application.Current.Properties["authorizationdata"] = null;
            }


            //if (Application.Current.Properties["userprofiledata"] != null)
            //{
            //    Application.Current.Properties["userprofiledata"] = null;
            //}
            return true;
        }

        public async Task<Auth_Prop> RefreshToken()
        {
            try
            {
                Auth_Prop rootobject = null;
                var client = new System.Net.Http.HttpClient();

                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];

                if (!authprop.UseRefreshToken)
                {
                    Application.Current.Properties["authorizationdata"] = null;

                    var formContent = new FormUrlEncodedContent(new[]
                    {
                      new KeyValuePair<string, string>("client_id", Constants.client_id),
                      new KeyValuePair<string, string>("grant_type", "refresh_token"),
                      new KeyValuePair<string, string>("refresh_token", authprop.RefreshToken),
                    });

                    client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "authorization/oauth/token"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header   
                    String url = String.Format("{0}{1}", Constants.RestUrl, "authorization/oauth/token");
                    var response = await client.PostAsync(url, formContent);
                    var LoginDetailsJson = response.Content.ReadAsStringAsync().Result;

                    rootobject = JsonConvert.DeserializeObject<Auth_Prop>(LoginDetailsJson);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Application.Current.Properties["authorizationdata"] = rootobject;
                    }
                }
                return rootobject;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            };
        }

        public async Task<Auth_Prop> ResetPassword(Auth_Prop en)
        {

            try
            {
                //   var formContent = new FormUrlEncodedContent(new[]
                //{
                // new KeyValuePair<string, string>("email", en.Email),

                //});
                var Detail = new Auth_Prop
                {
                    email = en.email,

                };
                var json = JsonConvert.SerializeObject(Detail.email);
                var email = new StringContent(json);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/Resetpassword"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header   
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/Resetpassword");
                var response = await client.PostAsync(url, email);
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;

                Auth_Prop rootobject = JsonConvert.DeserializeObject<Auth_Prop>(ResetDetailsJson);
                return rootobject;
            }

            catch (Exception er)
            {
                Debug.WriteLine(er.Message);
                return null;
            }
            //return null;
        }

        

    }
  
}

    interface ICheckNetWebService
    {
        //public async Task<Auth_Prop> ResetPassword(Auth_Prop en);
    }


