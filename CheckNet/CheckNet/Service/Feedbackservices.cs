﻿using CheckNet.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CheckNet.Service
{
   public class Feedbackservices
    {
        HttpClient client = null;
        organization_info data = (organization_info)Application.Current.Properties["organizationdetails"];
        public async Task<string> SendFeedback()
        {
         
            try
            {
                Feedback rootobject = null;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header   
                string FormatURL = string.Format("webapi/api/core/MobileApp/SendFeedback?webpage={0}&version={1}&comment={2}&userId={3}","","","", data.UserId);
                String url = String.Format("{0}{1}", Constants.RestUrl, FormatURL);
                var response = await client.GetAsync(url);
                var LoginDetailsJson = response.Content.ReadAsStringAsync().Result;
                // rootobject = JsonConvert.DeserializeObject<Auth_Prop>(LoginDetailsJson);
                return LoginDetailsJson.ToString();

            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return null;
        }
    }
    }
