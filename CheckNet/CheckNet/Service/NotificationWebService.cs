﻿using CheckNet.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CheckNet.Service
{
    public class NotificationWebService
    {


        public async Task<List<Notification_setting>> Notification_Setting()
        {
            try
            {
                string accesstoken = "";
                DeviceInfo devicedetails = (DeviceInfo)Application.Current.Properties["deviceinfo"];
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];                
                organization_info organizationdetail = (organization_info)Application.Current.Properties["organizationdetails"];
                var json = JsonConvert.SerializeObject(organizationdetail);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetUserNotificationSettings"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetUserNotificationSettings");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, new StringContent(json));
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
                List<Notification_setting> rootobject = JsonConvert.DeserializeObject<List<Notification_setting>>(ResetDetailsJson);

                return rootobject;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<GetMessagesResponse>> GetMessagesForInboxByUserId()
        {
            try
            {
                string accesstoken = "";
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                organization_info organizationdetail = (organization_info)Application.Current.Properties["organizationdetails"];
                var Messagedetail = new GetMessagesRequest
                {
                    UserId = organizationdetail.UserId,
                    PageSize = 20,
                    PageNumber = 1
                };
                

                var json = JsonConvert.SerializeObject(Messagedetail);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetMessagesForInboxByUserId"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetMessagesForInboxByUserId");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, new StringContent(json));
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
                List<GetMessagesResponse> rootobject = JsonConvert.DeserializeObject<List<GetMessagesResponse>>(ResetDetailsJson);

                return rootobject;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async void GetInboxUnReadMessageCount(NotificationsettingList data)
        {
            try
            {
                organization_info organizationdetail = (organization_info)Application.Current.Properties["organizationdetails"];
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header   
                String url = String.Format("{0}{1}{2}", Constants.RestUrl, "webapi/api/mobile/Notification/GetInboxUnReadMessageCount?userId=", organizationdetail.UserId);
                var response = await client.GetAsync(url);
                var LoginDetailsJson = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public async void UpdateNotification()
        {
            try
            {
                string accesstoken = "";
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                organization_info organizationdetail = (organization_info)Application.Current.Properties["organizationdetails"];
                var UpdateNotification = new UpdateNotificationRequest
                {
                    Status = "",
                    UserId = organizationdetail.UserId
                };
                var json = JsonConvert.SerializeObject(UpdateNotification);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/UpdateNotification"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/UpdateNotification");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, new StringContent(json));
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public async void UpdatePushNotificationMessageQueue()
        {
            try
            {
                string accesstoken = "";
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                organization_info organizationdetail = (organization_info)Application.Current.Properties["organizationdetails"];
                var UpdateNotification = new UpdateNotificationRequest
                {
                    Status = "",
                    UserId = organizationdetail.UserId
                };
                var json = JsonConvert.SerializeObject(UpdateNotification);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/UpdatePushNotificationMessageQueue"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/UpdatePushNotificationMessageQueue");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, new StringContent(json));
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

        }

        public async Task<string> UpdateMessageStatusWithMarkAll(NotificationsettingList data)
        {
            try
            {
                string accesstoken = "";
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                organization_info organizationdetail = (organization_info)Application.Current.Properties["organizationdetails"];
                
                var json = JsonConvert.SerializeObject(data);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/Notification/UpdateMessageStatusWithMarkAll"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/Notification/UpdateMessageStatusWithMarkAll");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, new StringContent(json));
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
                //var rootobject = JsonConvert.DeserializeObject <NotificationsettingList>(ResetDetailsJson);
                return null;
}
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }

        }

        public async Task<GetMessageListTaskAsync_Response> GetMessageListTaskAsync()
        {
            try
            {
                string accesstoken = "";
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                organization_info organizationdetail = (organization_info)Application.Current.Properties["organizationdetails"];
                var json = JsonConvert.SerializeObject(organizationdetail);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetMessageListTaskAsync"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetMessageListTaskAsync");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, new StringContent(json));
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
                GetMessageListTaskAsync_Response rootobject = JsonConvert.DeserializeObject<GetMessageListTaskAsync_Response>(ResetDetailsJson);

                return rootobject;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
