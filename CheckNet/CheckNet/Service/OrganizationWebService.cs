﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CheckNet.Models
{
    public class OrganizationWebService
    {
        public async Task<organization_info> GetOrganizationdata()
        {

            try
            {
                organization_info rootobject = null;
                if (!Application.Current.Properties.ContainsKey("organizationdetails"))
                {
                    string accesstoken = "";
                    DeviceInfo devicedetails = (DeviceInfo)Application.Current.Properties["deviceinfo"];
                    Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];

                    var organizationdetail = new OrganizationDetail_Info
                    {
                        UserName = authprop.UserName,
                        DeviceUUId = devicedetails.DeviceId,
                        DeviceModel = devicedetails.DeviceModel,
                        DevicePlatform = devicedetails.DevicePlatform,
                        DeviceVersion = devicedetails.AppVersion,
                        IsActive = true
                    };
                    var json = JsonConvert.SerializeObject(organizationdetail);

                    //var data = new StringContent(json);
                    var client = new HttpClient();
                    client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/OrganizationDetail"));
                    String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/OrganizationDetail");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    accesstoken = authprop.AccessToken;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                    HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                    //var response = await client.PostAsync(url, new StringContent(json));
                    var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
                    rootobject = JsonConvert.DeserializeObject<organization_info>(ResetDetailsJson);
                    Application.Current.Properties["organizationdetails"] = rootobject;
                    return rootobject;
                }
                else
                {
                    rootobject = (organization_info)Application.Current.Properties["organizationdetails"];
                    return rootobject;
                }
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;

            }



        }
        
        public async Task<ObservableCollection<OrderListResponse>> GetOrderList(OrderListRequest orderlistrequest)
        {
            try
            {
                
                string accesstoken = "";
                organization_info organizationdata = (organization_info)Application.Current.Properties["organizationdetails"];
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
               
                var json = JsonConvert.SerializeObject(orderlistrequest);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderList"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderList");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, new StringContent(json));
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
                ObservableCollection<OrderListResponse> rootobject = JsonConvert.DeserializeObject<ObservableCollection<OrderListResponse>>(ResetDetailsJson);
                Application.Current.Properties["OrderList"] = rootobject;
                return rootobject;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }

        }

        public async Task<OrdernumberPocResponse> GetOrderDetailByPOCtrlNo(OrdernumberPocRequest request)
        {
            try
            {
                string accesstoken = "";
                organization_info organizationdata = (organization_info)Application.Current.Properties["organizationdetails"];
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                
                var json = JsonConvert.SerializeObject(request);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderDetailByPOCtrlNo"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderDetailByPOCtrlNo");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, new StringContent(json));
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
                OrdernumberPocResponse rootobject = JsonConvert.DeserializeObject<OrdernumberPocResponse>(ResetDetailsJson);
                return rootobject;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        //public async void UpdateApproveOrderStatus()
        //{
        //    try
        //    {
        //        string accesstoken = null;
        //        Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
        //        organization_info organizationdata = (organization_info)Application.Current.Properties["organizationdetails"];
        //        var Updateorder = new updateorder_info
        //        {
        //            UpdateStatus = "success",
        //            OrganizationDetail = organizationdata,
        //            ApproveOrdersListData = 1,
        //        };
        //        var json = JsonConvert.SerializeObject(Updateorder);
        //        var client = new HttpClient();
        //        client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/UpdateApproveOrderStatus"));
        //        String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/UpdateApproveOrderStatus");
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
        //        accesstoken = authprop.AccessToken;
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
        //        HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
        //        //var response = await client.PostAsync(url, new StringContent(json));
        //        var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
        //        POCtrl_info rootobject = JsonConvert.DeserializeObject<POCtrl_info>(ResetDetailsJson);
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex);
        //    }
        //}

        public async Task<ObservableCollection<OrderSummaryResponse>> GetOrderSummary(OrderSummaryRequest orderlist)
        {
            try
            {
                string accesstoken = "";
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                              
                var json = JsonConvert.SerializeObject(orderlist);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderSummary"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderSummary");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));                
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;                
                ObservableCollection<OrderSummaryResponse> rootobject = JsonConvert.DeserializeObject<ObservableCollection<OrderSummaryResponse>>(ResetDetailsJson);
                return rootobject;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<OrderLineResponse> GetOrderLine(OrderLineRequest request)
        {

            try
            {
                string accesstoken = "";
                DeviceInfo devicedetails = (DeviceInfo)Application.Current.Properties["deviceinfo"];
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                organization_info organizationdata = (organization_info)Application.Current.Properties["organizationdetails"];
               
                var json = JsonConvert.SerializeObject(request);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderLine"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderLine");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, new StringContent(json));
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
                OrderLineResponse rootobject = JsonConvert.DeserializeObject<OrderLineResponse>(ResetDetailsJson);
                Application.Current.Properties["OrderLine"] = rootobject;
                return rootobject;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<string> GetConfirmationHtml(OrderconfirmationRequest orderconfirmationrequest)
        {
            try
            {
                string accesstoken = "";
                DeviceInfo devicedetails = (DeviceInfo)Application.Current.Properties["deviceinfo"];
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                organization_info organizationdata = (organization_info)Application.Current.Properties["organizationdetails"];
               
                var json = JsonConvert.SerializeObject(orderconfirmationrequest);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetConfirmationHtml"));
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetConfirmationHtml");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                accesstoken = authprop.AccessToken;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
               
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;
                string responsedata = ResetDetailsJson;
                //ConfirmationHtml_info rootobject = JsonConvert.DeserializeObject<ConfirmationHtml_info>(ResetDetailsJson);
                return responsedata;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }



        public async Task<OrdersCountResponse> GetOrderCounts(OrdersCountResponse obj)
        {
            try
            {

                string accesstoken = "";
                Auth_Prop authprop = (Auth_Prop)Application.Current.Properties["authorizationdata"];
                organization_info organizationdata = (organization_info)Application.Current.Properties["organizationdetails"];
                var OrganizationDetail = new organization_info();
                var Ordercountdetail = new OrdercountRequest
                {

                    OrderNumber = obj.OrderNumber,
                    ShoppingCartId = obj.ShoppingCartId,
                    SalesOrderNumber = obj.SalesOrderNumber,
                    VendorRef = obj.VendorRef,
                    OrganizationDetail = organizationdata,
                    SearchType = obj.SearchType
                };

                var json = JsonConvert.SerializeObject(Ordercountdetail);
                //var email = new StringContent(json);
                var client = new HttpClient();
                client.BaseAddress = new Uri(String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderCounts"));

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header   
                accesstoken = authprop.AccessToken;
                String url = String.Format("{0}{1}", Constants.RestUrl, "webapi/api/core/MobileApp/GetOrderCounts");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accesstoken); //ACCEPT header   
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                //var response = await client.PostAsync(url, email);
                var ResetDetailsJson = response.Content.ReadAsStringAsync().Result;

                OrdersCountResponse rootobject = JsonConvert.DeserializeObject<OrdersCountResponse>(ResetDetailsJson);
                return rootobject;
            }

            catch (Exception er)
            {
                Debug.WriteLine(er.Message);
                return null;
            }
            //return null;
        }
    }
}
