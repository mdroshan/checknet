﻿using CheckNet.ViewModels;
using I18NPortable;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CheckNet
{
    public class WebViewPageCS : ContentPage
    {
        public WebViewPageCS()
        {
           

            Padding = new Thickness(0, 20, 0, 0);
            Content = new StackLayout
            {
                Children = {
                    new CustomWebView {
                        
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand
                    }
                }
            };
        }
     
    }
}
